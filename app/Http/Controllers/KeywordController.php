<?php

namespace App\Http\Controllers;

use App\Models\Keyword;
use App\Models\Campaign;
use App\Models\RevenueReport;
use App\Models\Channel;
use App\Models\ChannelTracer;
use App\Models\ChannelTracerNew;
use Illuminate\Http\Request;
use App\Models\BidUser;
use DB;
use Auth;

class KeywordController extends Controller {
    
    public function clean($string) {
        $string = str_replace("Â", "", $string);
        $string = preg_replace('/\xc2\xa0/', '', $string);
        $string = urlencode(str_replace(" ", "", trim(strtolower($string))));
        return $string;
    }

    /* --------------------------------------------------------------------------- FUNCTION SEPARATOR --------------------------------------------------------------------------- */

    public function index(Request $request) {
    
    }

    /* --------------------------------------------------------------------------- FUNCTION SEPARATOR --------------------------------------------------------------------------- */

    public function normalize_match_type($match_type, $source) {
        if($source == "bing") {
            switch ($match_type) {
                case 'be':
                    $match_type = "Exact";
                    break;

                case 'bp':
                    $match_type = "Phrase";
                    break;
                
                case 'bb':
                    $match_type = "Broad";
                    break;

                default:
                    $match_type = $match_type;
                    break;
            }

        } else if($source == "google") {
            
            if($match_type == "e" || $match_type == "Exact match") {
                $match_type = "Exact";
            } else if($match_type == "p" || $match_type == "Phrase match") {
                $match_type = "Phrase";
            } else if($match_type == "b" || $match_type == "Broad match") {
                $match_type = "Broad";
            }

        }

        return $match_type;
    }

    /* --------------------------------------------------------------------------- FUNCTION SEPARATOR --------------------------------------------------------------------------- */

    public function new_aggregated(Request $request) {
        $keywords = null;
        $campaign = null;
        $consolidated_channels = array();
        $total_clickout = 0;
        $raw_total_revenues = 0;
        $total_channel_revenues = 0;

        $source = "bing";
        $keywords = null;
        $campaign = null;
        $consolidated_channels = array();

        if($request->input('source')) {
            $source = $request->input('source');
        }

        $fields_to_select = array("ID", "CHANNEL", "KEYWORD", "CAMPAIGN", "DATE", "KEYWORD", "MATCH_TYPE", "KEYWORD_IN", "COUNTRY");

        $start_date = date('Y-m-d',(strtotime ( '-1 day' , time() ) ));
        $end_date = date("Y-m-d");

        if($request->input('start') && $request->input('end')) {
            $start_date = $request->input('start');
            $end_date = $request->input('end');
        }

        
        /* @@@@@@@@@@@@@@@@@@@@@@@@@@@@@ BIDUSERS @@@@@@@@@@@@@@@@@@@@@@@@@@@@@ */

        $bid_users = BidUser::select(array("USERNAME", "SUBID"))->get();
        $formatted_bid_users = array();

        foreach($bid_users as $bid_user) {
            $formatted_bid_users[$bid_user->SUBID] = $bid_user->USERNAME;
        }

        /* @@@@@@@@@@@@@@@@@@@@@@@@@@@@@ BIDUSERS @@@@@@@@@@@@@@@@@@@@@@@@@@@@@ */



        /* ------------------------------------------------------------- REPORT VALIDATION SECTION ------------------------------------------------------------- */

        $keyword_reports = "SELECT EXTRACTION_DATE, SUM(SPEND) as spend, SUM(AVG_CPC) as avg_cpc, SUM(CLICKS) as clicks FROM KEYWORD_COST WHERE SOURCE = '$source' AND EXTRACTION_DATE >= '$start_date' AND EXTRACTION_DATE <= '$end_date' AND CAMPAIGN != '-' GROUP BY EXTRACTION_DATE order by EXTRACTION_DATE desc";

        if($source == "bing") {
            $revenue_reports = "SELECT DATE, SUM(CLICKS) as clicks, SUM(EARNINGS) as earnings FROM REVENUE_REPORT WHERE CUSTOM_CHANNEL_NAME <= 'T0000899' AND DATE >= '$start_date' AND DATE <= '$end_date' GROUP BY DATE order by DATE desc";
        } else {
            $revenue_reports = "SELECT DATE, SUM(CLICKS) as clicks, SUM(EARNINGS) as earnings FROM REVENUE_REPORT WHERE CUSTOM_CHANNEL_NAME > 'T0000899' AND DATE >= '$start_date' AND DATE <= '$end_date' GROUP BY DATE order by DATE desc";
        }

        $keyword_report_query = DB::select($keyword_reports);
        $revenue_report_query = DB::select($revenue_reports);

        $keyword_date_index = array();
        foreach($keyword_report_query as $keyword_report_instance) {
            $keyword_date_index[$keyword_report_instance->EXTRACTION_DATE] = $keyword_report_instance;
        }

        $report_data = array(
            "revenue_reports" => $revenue_report_query,
            "keyword_reports" => $keyword_date_index 
        );

        /* ------------------------------------------------------------- REPORT VALIDATION SECTION ------------------------------------------------------------- */

        
        $db_query_string = "SELECT id, STATUS, AD_GROUP, BID, LABELS, KEYWORD, MATCH_TYPE, DELIVERY, EXTRACTION_DATE, CAMPAIGN, SPEND, IMPR, CLICKS, USER FROM KEYWORD_COST where SOURCE = '$source' AND EXTRACTION_DATE >= '$start_date' AND EXTRACTION_DATE <= '$end_date' AND CAMPAIGN != '-' order by EXTRACTION_DATE ASC";
        $keywords = DB::select($db_query_string);

        $formatted_conversions = array();
        $formatted_conversions_by_channel = array();
        $conversions_by_keyword = array();

        $google_conversion_data = array();

        







        /* @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ WORK AROUND FOR GOOGLE TRACKING @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ */

        if($source != "bing") {
            $conversion_query_string = "SELECT DATE, SEARCHED_KEYWORD, KEYWORD ,CAMPAIGN, BIDMATCH, CHANNEL, USER FROM CONVERSIONS WHERE CHANNEL > 'T0000899' AND DATE >= '$start_date' AND DATE <= '$end_date'";
            $conversions = DB::select($conversion_query_string);

            foreach($conversions as $conversion) {

                $match_type = $this->normalize_match_type($conversion->BIDMATCH, $source);
                $index = $this->clean($conversion->SEARCHED_KEYWORD) ."-". $match_type;
                
                /* will be used for $total_tracked_conversions */

                $total_tracked_conversion_index = $conversion->CHANNEL;

                if(isset($formatted_conversions_by_channel[$total_tracked_conversion_index])) {
                    $formatted_conversions_by_channel[$total_tracked_conversion_index][] = $conversion;
                } else {
                    $formatted_conversions_by_channel[$total_tracked_conversion_index] = array();
                    $formatted_conversions_by_channel[$total_tracked_conversion_index][] = $conversion;
                }

                /* will be used for $total_tracked_conversions */


                /* will be used for keyword clickouts in frontend */

                if(isset($conversions_by_keyword[$this->clean($conversion->SEARCHED_KEYWORD)])) {
                    $conversions_by_keyword[$index][] = $conversion;
                } else {
                    $conversions_by_keyword[$this->clean($conversion->SEARCHED_KEYWORD)] = array();
                    $conversions_by_keyword[$this->clean($conversion->SEARCHED_KEYWORD)][] = $conversion;
                }

                /* will be used for keyword clickouts in frontend */


                /* will be used for keyword clickouts in frontend */

                if(isset($formatted_conversions[$index])) {
                    $formatted_conversions[$index][] = $conversion;
                } else {
                    $formatted_conversions[$index] = array();
                    $formatted_conversions[$index][] = $conversion;
                }

                /* will be used for keyword clickouts in frontend */

            }

            // return $formatted_conversions_by_channel;

            $google_conversion_data["conversions_by_keyword"] = $conversions_by_keyword;
            $google_conversion_data["clickouts"] = $formatted_conversions;
            $google_conversion_data["total_tracked_conversions"] = $formatted_conversions_by_channel;
        }

        // return $google_conversion_data["total_tracked_conversions"];

        // return $google_conversion_data;

        





        /* @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ WORK AROUND FOR GOOGLE TRACKING @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ */


        $campaigns_in_keywords = array();
        foreach($keywords as $keyword_instance) {
            $campaigns_in_keywords[] = $keyword_instance->CAMPAIGN;
        }

        $campaigns_in_keywords = array_unique($campaigns_in_keywords);
        $campaigns = Campaign::whereIn("CAMPAIGN", $campaigns_in_keywords)->where("EXTRACTION_DATE",">=", $start_date)->where("EXTRACTION_DATE","<=", $end_date)->where("SOURCE",$source)->orderBy("EXTRACTION_DATE", "ASC")->get();
        
        $campaigns_by_campaign_as_index = array();
        foreach($campaigns as $campaign_instance) {
            $campaigns_by_campaign_as_index[$this->clean($campaign_instance->CAMPAIGN)] = $campaign_instance;
        }

       




        /* ############################################################## REVENUE PROCESSING ############################################################## */

        if($source == "bing") {
            $revenues = RevenueReport::where("DATE",">=", $start_date)->where("DATE","<=", $end_date)->where("CUSTOM_CHANNEL_NAME","<=", "T0000899")->orderBy("DATE", "ASC")->get();
        } else {
            $revenues = RevenueReport::where("DATE",">=", $start_date)->where("DATE","<=", $end_date)->where("CUSTOM_CHANNEL_NAME",">=", "T0000900")->orderBy("DATE", "ASC")->get();
        }

        $revenue_channel_data = array();
        $revenue_raw = array();
        $revenue_with_channel_data_key = array();
        $collated_revenue_with_channel_key = array();

        foreach($revenues as $revenue) {
            
            if(!isset($revenue_with_channel_data_key[$revenue->CUSTOM_CHANNEL_NAME."-".$revenue->DATE])) {
                $revenue_with_channel_data_key[$revenue->CUSTOM_CHANNEL_NAME."-".$revenue->DATE] = array();
                $revenue_with_channel_data_key[$revenue->CUSTOM_CHANNEL_NAME."-".$revenue->DATE]['revenues'] = array();    
            }
            
            $revenue_with_channel_data_key[$revenue->CUSTOM_CHANNEL_NAME."-".$revenue->DATE]['revenues'][] = $revenue;
            
            if(!isset($collated_revenue_with_channel_key[$revenue->CUSTOM_CHANNEL_NAME])) {
                $collated_revenue_with_channel_key[$revenue->CUSTOM_CHANNEL_NAME] = array();
            }

            $collated_revenue_with_channel_key[$revenue->CUSTOM_CHANNEL_NAME][] = $revenue;

            $revenue_raw[] = $revenue['ID'];
            $raw_total_revenues += $revenue['EARNINGS']; 
        }


        /* ############################################################## REVENUE PROCESSING ############################################################## */


        /* ########################################### CHANNEL TABLE CONTAINS CURRENT DATE RECORDS AND CHANNEL TRACER CONTAINS HISTORIC RECORDS ###########################################  */

        if($end_date == date("Y-m-d")) {
            $fields_for_current_channel = $fields_to_select;
            unset($fields_for_current_channel[4]);
            unset($fields_for_current_channel[5]);

            $channel = Channel::select($fields_for_current_channel)->where("SOURCE",$source)->get();
            $consolidated_channels = $channel->toArray();
        }

        /* ########################################### CHANNEL TABLE CONTAINS CURRENT DATE RECORDS AND CHANNEL TRACER CONTAINS HISTORIC RECORDS ###########################################  */
        
        





        $historic_channel = ChannelTracerNew::select($fields_to_select)->where("DATE",">=", $start_date)->where("DATE","<=", $end_date)->where("SOURCE",$source)->orderBy("DATE", "ASC")->get();
        $channel_tracer = $historic_channel->toArray();

        $consolidated_channels = array_merge($consolidated_channels, $channel_tracer);

        





        /* ######################################## USING RELATIVE DATA TO MAKE IT ARRAY INDEX FOR COLLATED COMPUTATION ######################################## */

        $processed_channel_tracer = array();
        $processed_channel_tracer_by_id = array();
        $processed_channel_tracer_by_index = array();
        $revenue_used_in_channel = array();
        $channel_ids = array();
        $channel_indexes = array();
        $channels_with_revenue = array();
        $channels_without_revenue = array();
        $revenue_via_channel_key = array();

        $revenue_channel_keys = array_keys($revenue_with_channel_data_key);


        $channel_matched_to_revenues = array();
        foreach($consolidated_channels as $consolidated_channel) {

            $match_type = "";
            $channel_ids[] = $consolidated_channel['ID'];
            $channel_tracer_content_data = array();
            $index = null;

            $assoc_revenue = null;
            
            if(!isset($consolidated_channel['DATE'])) {
                $consolidated_channel['DATE'] = date("Y-m-d");
            }

            $match_type = $this->normalize_match_type($consolidated_channel['MATCH_TYPE'], $source);

            $matching_index_key = $consolidated_channel['CHANNEL']."-".$consolidated_channel['DATE'];
            $keyword_in_trimmed = $this->clean($consolidated_channel['KEYWORD_IN']);
            $campaign_trimmed = $this->clean($consolidated_channel['CAMPAIGN']);
            
            $index_key = $campaign_trimmed."-".$consolidated_channel['DATE']."-".$match_type."-".$keyword_in_trimmed;

            $channel_indexes[$index_key] = $consolidated_channel['ID'];

            $revenue_count = 0;
            if(!empty($assoc_revenue)) {
                $revenue_count = count($assoc_revenue['revenues']);
            }

            if(isset($revenue_with_channel_data_key[$matching_index_key])) {
                $channel_matched_to_revenues[$matching_index_key] = 1;
                $assoc_revenue = $revenue_with_channel_data_key[$matching_index_key];
                $assoc_revenue['indexKey'] = $index_key;
                $assoc_revenue['channelId'] = $consolidated_channel['ID'];
                $assoc_revenue['revenue_count'] = $revenue_count;
                $revenue_via_channel_key[$index_key] = $assoc_revenue;
            }

            // echo $match_type;
            // echo "<br />"

            $channel_tracer_content_data = array(
                'id'            => $consolidated_channel['ID'],
                'campaign'      => $consolidated_channel['CAMPAIGN'],
                'date'          => $consolidated_channel['DATE'],
                'match_type'    => $match_type,
                'country'       => $consolidated_channel['COUNTRY'],
                'keyword'       => $consolidated_channel['KEYWORD'],
                'channel'       => $consolidated_channel['CHANNEL'],
                'keyword_in'    => $consolidated_channel['KEYWORD_IN'],
                'revenue'       => $assoc_revenue,
                'revenue_count' => $revenue_count,
                'index_key'     => $index_key,
            );

            /* ------------------------- handling of channels that has multiple matching with revenues ------------------------- */
            
            /*if(isset($processed_channel_tracer[$index_key]['revenue']) && !empty($processed_channel_tracer[$index_key]['revenue'])) {
                
                if(isset($processed_channel_tracer[$index_key]['budget'])) {
                    $channel_tracer_content_data['budget'] += $processed_channel_tracer[$index_key]['budget'];
                }

                if($assoc_revenue != null) {
                    
                    // has existing revenue and just adding it up
                    // $channel_tracer_content_data['revenue']->EARNINGS += (float)$processed_channel_tracer[$index_key]['revenue']->EARNINGS;
                    $channel_tracer_content_data['revenue']['isRepeated'] = true;

                } else {

                    // if channel has more multiple instance and 
                    if(isset($processed_channel_tracer[$index_key]['revenue']) && !empty($processed_channel_tracer[$index_key]['revenue'])) {
                        $channel_tracer_content_data['revenue'] = $processed_channel_tracer[$index_key]['revenue'];
                        $channel_tracer_content_data['revenue']->isOverWritten = true;
                        $channel_tracer_content_data['revenue']->overWritingId = $consolidated_channel['ID'];
                    }

                }
            }*/

            /* ------------------------- handling of channels that has multiple matching with revenues ------------------------- */


            $processed_channel_tracer[$index_key] = $channel_tracer_content_data;
            $processed_channel_tracer_by_id[$consolidated_channel['ID']] = $channel_tracer_content_data;

            if(isset($assoc_revenue) && count($assoc_revenue['revenues']) > 0) {
                $channels_with_revenue[] = $consolidated_channel['ID'];
                
                foreach($assoc_revenue['revenues'] as $assoc_revenue_instance) {
                    $revenue_used_in_channel[] = $assoc_revenue_instance['ID'];
                }

            } else {
                $channels_without_revenue = $consolidated_channel['ID'];
            }

            // for reporting purposes
            // if(isset($channel_tracer_content_data['revenue']->EARNINGS)) {
            //     $total_channel_revenues += (float)$channel_tracer_content_data['revenue']->EARNINGS;
            // }
        }

        /* ######################################## USING RELATIVE DATA TO MAKE IT ARRAY INDEX FOR COLLATED COMPUTATION ######################################## */





        



        /* ########################################### WAY TO KNOW W/C REVENUE DOESNT HAVE A CHANNEL ASSOCIATED TO IT ########################################### */
        
        /*$matched_to_revenue = array_keys($channel_matched_to_revenues);
        $revenues_without_channel = array_diff($revenue_channel_keys, $matched_to_revenue);

        $missing_revenues = 0;
        foreach($revenues_without_channel as $revenue_in_channel) {
            if($revenue_with_channel_data_key[$revenue_in_channel]->EARNINGS > 0) {
                // echo $revenue_in_channel ."<strong> (".$revenue_with_channel_data_key[$revenue_in_channel]->EARNINGS.")</strong>";
                // echo "<br />";
                $missing_revenues += $revenue_with_channel_data_key[$revenue_in_channel]->EARNINGS;
            }
        }*/

        /* ########################################### WAY TO KNOW W/C REVENUE DOESNT HAVE A CHANNEL ASSOCIATED TO IT ########################################### */





        $revenues_in_channel = 0;
        foreach($processed_channel_tracer as $processed_channel_tracer_instance) {
            if($processed_channel_tracer_instance['revenue']) {
                if(isset($processed_channel_tracer_instance['revenue']->EARNINGS)) {
                    $revenues_in_channel += $processed_channel_tracer_instance['revenue']->EARNINGS;
                }

            }
        }

        $processed_channel_keys = array_keys($processed_channel_tracer);
        
        $revenues_used_by_keyword = array();
        $channels_used_in_keyword = array();
        $total_keyword_revenues = 0;

        $channels_used_frequency = array();
        $keyword_with_revenue_but_no_cost = array();

        $to_loop_keywords = array();

        // eliminating all keywords with zero cost and no revenue

        $keyword_index_keys = array();
        $rejectd_keyword_index_keys = array();
        $total_budget = 0;
        $keyword_to_revenue_assoc = array();
        $total_clickout_concat = 0;
        $total_clickout_processed = 0;
        $keyword_revenues = array();
        $keyword_channel_indexes = array();

        foreach($keywords as $keyword) {
            
            $hasTotal = false;
            $hasProcessed = false;

            /* --------------------- CLEANING OF INDEX KEY VALUES --------------------- */

                $keyword_in_trimmed = $this->clean($keyword->KEYWORD);
                $campaign_trimmed = $this->clean($keyword->CAMPAIGN);
                $match_type_trimmed = trim($this->normalize_match_type($keyword->MATCH_TYPE, $source));

                $index_key = $campaign_trimmed."-".$keyword->EXTRACTION_DATE."-".$match_type_trimmed."-".$keyword_in_trimmed;
                $keyword_index_key = $campaign_trimmed."-".$match_type_trimmed."-".$keyword_in_trimmed;

                if($source != "bing") {
                    $match_type_trimmed = trim($this->normalize_match_type($keyword->MATCH_TYPE, $source));
                    $keyword->MATCH_TYPE = $match_type_trimmed;
                }

            /* --------------------- CLEANING OF INDEX KEY VALUES --------------------- */
            
            $in_channels = in_array($index_key, $processed_channel_keys);
            $channel_reference_id = null;
            $budget = null;

            if($in_channels) {
                $channel_reference_id = $processed_channel_tracer[$index_key]['id'];
            }
            
            $keyword_index_keys[] = $index_key;

            $hasRevenue = false;
            $hasCost = false;

            if($keyword->SPEND > 0) {
                $hasCost = true;
            }

            $keyword->forced_keyword = null;
            if($in_channels) {
                $channels_used_in_keyword[] = $processed_channel_tracer[$index_key]['id'];
                $keyword->forced_keyword = $processed_channel_tracer[$index_key]['keyword'];
                $keyword_channel_indexes[$index_key] = $index_key;
                
                $keyword_revenue = null;
                if(isset($revenue_via_channel_key[$index_key]) && !empty($revenue_via_channel_key[$index_key])) {
                    $keyword_revenue = $revenue_via_channel_key[$index_key];
                    $keyword_revenue['keywordId'] = $keyword->id;
                    $hasRevenue = true;
                }
            }

            /* --------------------------------- HANDLING OF CAMPAIGN BUDGET --------------------------------- */

            if(isset($campaigns_by_campaign_as_index[$campaign_trimmed])) {
                $keyword->budget = $campaigns_by_campaign_as_index[$campaign_trimmed]->BUDGET;
            }   

            if(isset($keyword->budget) and isset($to_loop_keywords[$keyword_index_key])) {
                $keyword->budget += $to_loop_keywords[$keyword_index_key]->budget;
            }

            /* --------------------------------- HANDLING OF CAMPAIGN BUDGET --------------------------------- */

            $existing_revenue_value = null;
            if($hasCost ||  $hasRevenue) {

                if(isset($to_loop_keywords[$keyword_index_key])) {
                    (float)$keyword->IMPR += (float)$to_loop_keywords[$keyword_index_key]->IMPR;
                    (float)$keyword->SPEND += (float)$to_loop_keywords[$keyword_index_key]->SPEND;
                    (int)$keyword->CLICKS += (int)$to_loop_keywords[$keyword_index_key]->CLICKS;
                }
        
                
                /* -------------------------------- AGGREGATING THE REVENUE VALUES IN THE KEYWORD -------------------------------- */

                if($hasRevenue) {

                    if(!isset($keyword_revenues[$keyword_index_key])) {
                        $keyword_revenues[$keyword_index_key] = array();
                        $keyword_revenues[$keyword_index_key][] = $keyword_revenue;
                    } else {

                        /* ###################### MAKE SURE THAT THE REVENUE BEING ASSOCIATED TO THE KEYWORD INDEX IS NOT YET EXISTING ###################### */
                        
                        $isExisting = false;
                        foreach($keyword_revenues[$keyword_index_key] as $keyword_revenue_instance) {
                            if($keyword_revenue_instance['revenues'][0]['ID'] == $keyword_revenue['revenues'][0]['ID']) {
                                $isExisting = true;
                            }
                        }

                        if(!$isExisting) {
                            $keyword_revenues[$keyword_index_key][] = $keyword_revenue;
                        }

                        /* ###################### MAKE SURE THAT THE REVENUE BEING ASSOCIATED TO THE KEYWORD INDEX IS NOT YET EXISTING ###################### */
                    }

                }

                /* -------------------------------- AGGREGATING THE REVENUE VALUES IN THE KEYWORD -------------------------------- */

                
                $to_loop_keywords[$keyword_index_key] = $keyword;
            
            } else {

                $keyword_instance = Keyword::find($keyword->id);
                if($keyword_instance) {
                    $keyword_instance->delete($keyword->id);
                }

            }
        }

        $channel_indexes_keys = array_keys($channel_indexes);

        // raw_filtering of channels that wasnt used by keywords
        $unused_indexes = array_diff($channel_indexes_keys, $keyword_channel_indexes);
        $unused = array();

        foreach($unused_indexes as $unused_index) {
            array_push($unused, $channel_indexes[$unused_index]);
        }

        $unused_channels = array_diff($channel_ids, $channels_with_revenue);
        $difference = array_diff($revenue_raw, $revenue_used_in_channel);


        $data['formatted_bid_users'] = $formatted_bid_users;
        $data['google_conversion_data'] = $google_conversion_data;
        $data['revenues'] = $revenue_with_channel_data_key;
        $data['collated_revenue_with_channel_key'] = $collated_revenue_with_channel_key;
        $data['campaigns'] = $campaigns_by_campaign_as_index;
        $data['channel_tracer'] = $processed_channel_tracer;
        $data['processed_channel_tracer_by_id'] = $processed_channel_tracer_by_id;
        $data['keywords'] = $to_loop_keywords;
        $data['keyword_revenues'] = $keyword_revenues;
        $data['unused_channel_ids'] = $unused;
        $data['report_data'] = $report_data;

        return view('aggregated_keyword_performance', ["data"=>$data]);
    }

    /* --------------------------------------------------------------------------- FUNCTION SEPARATOR --------------------------------------------------------------------------- */

    public function aggregated(Request $request) {
        $keywords = null;
        $campaign = null;
        $consolidated_channels = array();
        $total_clickout = 0;
        $raw_total_revenues = 0;
        $total_channel_revenues = 0;

        $source = "bing";
        $keywords = null;
        $campaign = null;
        $consolidated_channels = array();

        if($request->input('source')) {
            $source = $request->input('source');
        }

        $fields_to_select = array("ID", "CHANNEL", "KEYWORD", "CAMPAIGN", "DATE", "KEYWORD", "MATCH_TYPE", "KEYWORD_IN", "COUNTRY");

        $start_date = date('Y-m-d',(strtotime ( '-1 day' , time() ) ));
        $end_date = date("Y-m-d");

        if($request->input('start') && $request->input('end')) {
            $start_date = $request->input('start');
            $end_date = $request->input('end');
        }

        
        /* @@@@@@@@@@@@@@@@@@@@@@@@@@@@@ BIDUSERS @@@@@@@@@@@@@@@@@@@@@@@@@@@@@ */

        $bid_users = BidUser::select(array("USERNAME", "SUBID"))->get();
        $formatted_bid_users = array();

        foreach($bid_users as $bid_user) {
            $formatted_bid_users[$bid_user->SUBID] = $bid_user->USERNAME;
        }

        /* @@@@@@@@@@@@@@@@@@@@@@@@@@@@@ BIDUSERS @@@@@@@@@@@@@@@@@@@@@@@@@@@@@ */



        /* ------------------------------------------------------------- REPORT VALIDATION SECTION ------------------------------------------------------------- */

        $keyword_reports = "SELECT EXTRACTION_DATE, SUM(SPEND) as spend, SUM(AVG_CPC) as avg_cpc, SUM(CLICKS) as clicks FROM KEYWORD_COST WHERE SOURCE = '$source' AND EXTRACTION_DATE >= '$start_date' AND EXTRACTION_DATE <= '$end_date' AND CAMPAIGN != '-' GROUP BY EXTRACTION_DATE order by EXTRACTION_DATE desc";

        if($source == "bing") {
            $revenue_reports = "SELECT DATE, SUM(CLICKS) as clicks, SUM(EARNINGS) as earnings FROM REVENUE_REPORT WHERE CUSTOM_CHANNEL_NAME <= 'T0000899' AND DATE >= '$start_date' AND DATE <= '$end_date' GROUP BY DATE order by DATE desc";
        } else {
            $revenue_reports = "SELECT DATE, SUM(CLICKS) as clicks, SUM(EARNINGS) as earnings FROM REVENUE_REPORT WHERE CUSTOM_CHANNEL_NAME > 'T0000899' AND DATE >= '$start_date' AND DATE <= '$end_date' GROUP BY DATE order by DATE desc";
        }

        $keyword_report_query = DB::select($keyword_reports);
        $revenue_report_query = DB::select($revenue_reports);

        $keyword_date_index = array();
        foreach($keyword_report_query as $keyword_report_instance) {
            $keyword_date_index[$keyword_report_instance->EXTRACTION_DATE] = $keyword_report_instance;
        }

        $report_data = array(
            "revenue_reports" => $revenue_report_query,
            "keyword_reports" => $keyword_date_index 
        );

        /* ------------------------------------------------------------- REPORT VALIDATION SECTION ------------------------------------------------------------- */

        
        $db_query_string = "SELECT id, STATUS, AD_GROUP, BID, LABELS, KEYWORD, MATCH_TYPE, DELIVERY, EXTRACTION_DATE, CAMPAIGN, SPEND, IMPR, CLICKS, USER FROM KEYWORD_COST where SOURCE = '$source' AND EXTRACTION_DATE >= '$start_date' AND EXTRACTION_DATE <= '$end_date' AND CAMPAIGN != '-' order by EXTRACTION_DATE ASC";
        $keywords = DB::select($db_query_string);

        $formatted_conversions = array();
        $formatted_conversions_by_channel = array();
        $conversions_by_keyword = array();

        $google_conversion_data = array();

        







        /* @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ WORK AROUND FOR GOOGLE TRACKING @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ */

        if($source != "bing") {
            $conversion_query_string = "SELECT DATE, SEARCHED_KEYWORD, KEYWORD ,CAMPAIGN, BIDMATCH, CHANNEL, USER FROM CONVERSIONS WHERE CHANNEL > 'T0000899' AND DATE >= '$start_date' AND DATE <= '$end_date'";
            $conversions = DB::select($conversion_query_string);

            foreach($conversions as $conversion) {

                $match_type = $this->normalize_match_type($conversion->BIDMATCH, $source);
                $index = $this->clean($conversion->SEARCHED_KEYWORD) ."-". $match_type;
                
                /* will be used for $total_tracked_conversions */

                $total_tracked_conversion_index = $conversion->CHANNEL;

                if(isset($formatted_conversions_by_channel[$total_tracked_conversion_index])) {
                    $formatted_conversions_by_channel[$total_tracked_conversion_index][] = $conversion;
                } else {
                    $formatted_conversions_by_channel[$total_tracked_conversion_index] = array();
                    $formatted_conversions_by_channel[$total_tracked_conversion_index][] = $conversion;
                }

                /* will be used for $total_tracked_conversions */


                /* will be used for keyword clickouts in frontend */

                if(isset($conversions_by_keyword[$this->clean($conversion->SEARCHED_KEYWORD)])) {
                    $conversions_by_keyword[$index][] = $conversion;
                } else {
                    $conversions_by_keyword[$this->clean($conversion->SEARCHED_KEYWORD)] = array();
                    $conversions_by_keyword[$this->clean($conversion->SEARCHED_KEYWORD)][] = $conversion;
                }

                /* will be used for keyword clickouts in frontend */


                /* will be used for keyword clickouts in frontend */

                if(isset($formatted_conversions[$index])) {
                    $formatted_conversions[$index][] = $conversion;
                } else {
                    $formatted_conversions[$index] = array();
                    $formatted_conversions[$index][] = $conversion;
                }

                /* will be used for keyword clickouts in frontend */

            }

            // return $formatted_conversions_by_channel;

            $google_conversion_data["conversions_by_keyword"] = $conversions_by_keyword;
            $google_conversion_data["clickouts"] = $formatted_conversions;
            $google_conversion_data["total_tracked_conversions"] = $formatted_conversions_by_channel;
        }

        // return $google_conversion_data["total_tracked_conversions"];

        // return $google_conversion_data;

        





        /* @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ WORK AROUND FOR GOOGLE TRACKING @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ */


        $campaigns_in_keywords = array();
        foreach($keywords as $keyword_instance) {
            $campaigns_in_keywords[] = $keyword_instance->CAMPAIGN;
        }

        $campaigns_in_keywords = array_unique($campaigns_in_keywords);
        $campaigns = Campaign::whereIn("CAMPAIGN", $campaigns_in_keywords)->where("EXTRACTION_DATE",">=", $start_date)->where("EXTRACTION_DATE","<=", $end_date)->where("SOURCE",$source)->orderBy("EXTRACTION_DATE", "ASC")->get();
        
        $campaigns_by_campaign_as_index = array();
        foreach($campaigns as $campaign_instance) {
            $campaigns_by_campaign_as_index[$this->clean($campaign_instance->CAMPAIGN)] = $campaign_instance;
        }

       




        /* ############################################################## REVENUE PROCESSING ############################################################## */

        if($source == "bing") {
            $revenues = RevenueReport::where("DATE",">=", $start_date)->where("DATE","<=", $end_date)->where("CUSTOM_CHANNEL_NAME","<=", "T0000899")->orderBy("DATE", "ASC")->get();
        } else {
            $revenues = RevenueReport::where("DATE",">=", $start_date)->where("DATE","<=", $end_date)->where("CUSTOM_CHANNEL_NAME",">=", "T0000900")->orderBy("DATE", "ASC")->get();
        }

        $revenue_channel_data = array();
        $revenue_raw = array();
        $revenue_with_channel_data_key = array();
        $collated_revenue_with_channel_key = array();

        foreach($revenues as $revenue) {
            
            if(!isset($revenue_with_channel_data_key[$revenue->CUSTOM_CHANNEL_NAME."-".$revenue->DATE])) {
                $revenue_with_channel_data_key[$revenue->CUSTOM_CHANNEL_NAME."-".$revenue->DATE] = array();
                $revenue_with_channel_data_key[$revenue->CUSTOM_CHANNEL_NAME."-".$revenue->DATE]['revenues'] = array();    
            }
            
            $revenue_with_channel_data_key[$revenue->CUSTOM_CHANNEL_NAME."-".$revenue->DATE]['revenues'][] = $revenue;
            
            if(!isset($collated_revenue_with_channel_key[$revenue->CUSTOM_CHANNEL_NAME])) {
                $collated_revenue_with_channel_key[$revenue->CUSTOM_CHANNEL_NAME] = array();
            }

            $collated_revenue_with_channel_key[$revenue->CUSTOM_CHANNEL_NAME][] = $revenue;

            $revenue_raw[] = $revenue['ID'];
            $raw_total_revenues += $revenue['EARNINGS']; 
        }


        /* ############################################################## REVENUE PROCESSING ############################################################## */


        /* ########################################### CHANNEL TABLE CONTAINS CURRENT DATE RECORDS AND CHANNEL TRACER CONTAINS HISTORIC RECORDS ###########################################  */

        if($end_date == date("Y-m-d")) {
            $fields_for_current_channel = $fields_to_select;
            unset($fields_for_current_channel[4]);
            unset($fields_for_current_channel[5]);

            $channel = Channel::select($fields_for_current_channel)->where("SOURCE",$source)->get();
            $consolidated_channels = $channel->toArray();
        }

        /* ########################################### CHANNEL TABLE CONTAINS CURRENT DATE RECORDS AND CHANNEL TRACER CONTAINS HISTORIC RECORDS ###########################################  */
        
        





        $historic_channel = ChannelTracer::select($fields_to_select)->where("DATE",">=", $start_date)->where("DATE","<=", $end_date)->where("SOURCE",$source)->orderBy("DATE", "ASC")->get();
        $channel_tracer = $historic_channel->toArray();

        $consolidated_channels = array_merge($consolidated_channels, $channel_tracer);

        





        /* ######################################## USING RELATIVE DATA TO MAKE IT ARRAY INDEX FOR COLLATED COMPUTATION ######################################## */

        $processed_channel_tracer = array();
        $processed_channel_tracer_by_id = array();
        $processed_channel_tracer_by_index = array();
        $revenue_used_in_channel = array();
        $channel_ids = array();
        $channel_indexes = array();
        $channels_with_revenue = array();
        $channels_without_revenue = array();
        $revenue_via_channel_key = array();

        $revenue_channel_keys = array_keys($revenue_with_channel_data_key);


        $channel_matched_to_revenues = array();
        foreach($consolidated_channels as $consolidated_channel) {

            $match_type = "";
            $channel_ids[] = $consolidated_channel['ID'];
            $channel_tracer_content_data = array();
            $index = null;

            $assoc_revenue = null;
            
            if(!isset($consolidated_channel['DATE'])) {
                $consolidated_channel['DATE'] = date("Y-m-d");
            }

            $match_type = $this->normalize_match_type($consolidated_channel['MATCH_TYPE'], $source);

            $matching_index_key = $consolidated_channel['CHANNEL']."-".$consolidated_channel['DATE'];
            $keyword_in_trimmed = $this->clean($consolidated_channel['KEYWORD_IN']);
            $campaign_trimmed = $this->clean($consolidated_channel['CAMPAIGN']);
            
            $index_key = $campaign_trimmed."-".$consolidated_channel['DATE']."-".$match_type."-".$keyword_in_trimmed;

            $channel_indexes[$index_key] = $consolidated_channel['ID'];

            $revenue_count = 0;
            if(!empty($assoc_revenue)) {
                $revenue_count = count($assoc_revenue['revenues']);
            }

            if(isset($revenue_with_channel_data_key[$matching_index_key])) {
                $channel_matched_to_revenues[$matching_index_key] = 1;
                $assoc_revenue = $revenue_with_channel_data_key[$matching_index_key];
                $assoc_revenue['indexKey'] = $index_key;
                $assoc_revenue['channelId'] = $consolidated_channel['ID'];
                $assoc_revenue['revenue_count'] = $revenue_count;
                $revenue_via_channel_key[$index_key] = $assoc_revenue;
            }

            // echo $match_type;
            // echo "<br />"

            $channel_tracer_content_data = array(
                'id'            => $consolidated_channel['ID'],
                'campaign'      => $consolidated_channel['CAMPAIGN'],
                'date'          => $consolidated_channel['DATE'],
                'match_type'    => $match_type,
                'country'       => $consolidated_channel['COUNTRY'],
                'keyword'       => $consolidated_channel['KEYWORD'],
                'channel'       => $consolidated_channel['CHANNEL'],
                'keyword_in'    => $consolidated_channel['KEYWORD_IN'],
                'revenue'       => $assoc_revenue,
                'revenue_count' => $revenue_count,
                'index_key'     => $index_key,
            );

            /* ------------------------- handling of channels that has multiple matching with revenues ------------------------- */
            
            /*if(isset($processed_channel_tracer[$index_key]['revenue']) && !empty($processed_channel_tracer[$index_key]['revenue'])) {
                
                if(isset($processed_channel_tracer[$index_key]['budget'])) {
                    $channel_tracer_content_data['budget'] += $processed_channel_tracer[$index_key]['budget'];
                }

                if($assoc_revenue != null) {
                    
                    // has existing revenue and just adding it up
                    // $channel_tracer_content_data['revenue']->EARNINGS += (float)$processed_channel_tracer[$index_key]['revenue']->EARNINGS;
                    $channel_tracer_content_data['revenue']['isRepeated'] = true;

                } else {

                    // if channel has more multiple instance and 
                    if(isset($processed_channel_tracer[$index_key]['revenue']) && !empty($processed_channel_tracer[$index_key]['revenue'])) {
                        $channel_tracer_content_data['revenue'] = $processed_channel_tracer[$index_key]['revenue'];
                        $channel_tracer_content_data['revenue']->isOverWritten = true;
                        $channel_tracer_content_data['revenue']->overWritingId = $consolidated_channel['ID'];
                    }

                }
            }*/

            /* ------------------------- handling of channels that has multiple matching with revenues ------------------------- */


            $processed_channel_tracer[$index_key] = $channel_tracer_content_data;
            $processed_channel_tracer_by_id[$consolidated_channel['ID']] = $channel_tracer_content_data;

            if(isset($assoc_revenue) && count($assoc_revenue['revenues']) > 0) {
                $channels_with_revenue[] = $consolidated_channel['ID'];
                
                foreach($assoc_revenue['revenues'] as $assoc_revenue_instance) {
                    $revenue_used_in_channel[] = $assoc_revenue_instance['ID'];
                }

            } else {
                $channels_without_revenue = $consolidated_channel['ID'];
            }

            // for reporting purposes
            // if(isset($channel_tracer_content_data['revenue']->EARNINGS)) {
            //     $total_channel_revenues += (float)$channel_tracer_content_data['revenue']->EARNINGS;
            // }
        }

        /* ######################################## USING RELATIVE DATA TO MAKE IT ARRAY INDEX FOR COLLATED COMPUTATION ######################################## */





        



        /* ########################################### WAY TO KNOW W/C REVENUE DOESNT HAVE A CHANNEL ASSOCIATED TO IT ########################################### */
        
        /*$matched_to_revenue = array_keys($channel_matched_to_revenues);
        $revenues_without_channel = array_diff($revenue_channel_keys, $matched_to_revenue);

        $missing_revenues = 0;
        foreach($revenues_without_channel as $revenue_in_channel) {
            if($revenue_with_channel_data_key[$revenue_in_channel]->EARNINGS > 0) {
                // echo $revenue_in_channel ."<strong> (".$revenue_with_channel_data_key[$revenue_in_channel]->EARNINGS.")</strong>";
                // echo "<br />";
                $missing_revenues += $revenue_with_channel_data_key[$revenue_in_channel]->EARNINGS;
            }
        }*/

        /* ########################################### WAY TO KNOW W/C REVENUE DOESNT HAVE A CHANNEL ASSOCIATED TO IT ########################################### */





        $revenues_in_channel = 0;
        foreach($processed_channel_tracer as $processed_channel_tracer_instance) {
            if($processed_channel_tracer_instance['revenue']) {
                if(isset($processed_channel_tracer_instance['revenue']->EARNINGS)) {
                    $revenues_in_channel += $processed_channel_tracer_instance['revenue']->EARNINGS;
                }

            }
        }

        $processed_channel_keys = array_keys($processed_channel_tracer);
        
        $revenues_used_by_keyword = array();
        $channels_used_in_keyword = array();
        $total_keyword_revenues = 0;

        $channels_used_frequency = array();
        $keyword_with_revenue_but_no_cost = array();

        $to_loop_keywords = array();

        // eliminating all keywords with zero cost and no revenue

        $keyword_index_keys = array();
        $rejectd_keyword_index_keys = array();
        $total_budget = 0;
        $keyword_to_revenue_assoc = array();
        $total_clickout_concat = 0;
        $total_clickout_processed = 0;
        $keyword_revenues = array();
        $keyword_channel_indexes = array();

        foreach($keywords as $keyword) {
            
            $hasTotal = false;
            $hasProcessed = false;

            /* --------------------- CLEANING OF INDEX KEY VALUES --------------------- */

                $keyword_in_trimmed = $this->clean($keyword->KEYWORD);
                $campaign_trimmed = $this->clean($keyword->CAMPAIGN);
                $match_type_trimmed = trim($this->normalize_match_type($keyword->MATCH_TYPE, $source));

                $index_key = $campaign_trimmed."-".$keyword->EXTRACTION_DATE."-".$match_type_trimmed."-".$keyword_in_trimmed;
                $keyword_index_key = $campaign_trimmed."-".$match_type_trimmed."-".$keyword_in_trimmed;

                if($source != "bing") {
                    $match_type_trimmed = trim($this->normalize_match_type($keyword->MATCH_TYPE, $source));
                    $keyword->MATCH_TYPE = $match_type_trimmed;
                }

            /* --------------------- CLEANING OF INDEX KEY VALUES --------------------- */
            
            $in_channels = in_array($index_key, $processed_channel_keys);
            $channel_reference_id = null;
            $budget = null;

            if($in_channels) {
                $channel_reference_id = $processed_channel_tracer[$index_key]['id'];
            }
            
            $keyword_index_keys[] = $index_key;

            $hasRevenue = false;
            $hasCost = false;

            if($keyword->SPEND > 0) {
                $hasCost = true;
            }

            $keyword->forced_keyword = null;
            if($in_channels) {
                $channels_used_in_keyword[] = $processed_channel_tracer[$index_key]['id'];
                $keyword->forced_keyword = $processed_channel_tracer[$index_key]['keyword'];
                $keyword_channel_indexes[$index_key] = $index_key;
                
                $keyword_revenue = null;
                if(isset($revenue_via_channel_key[$index_key]) && !empty($revenue_via_channel_key[$index_key])) {
                    $keyword_revenue = $revenue_via_channel_key[$index_key];
                    $keyword_revenue['keywordId'] = $keyword->id;
                    $hasRevenue = true;
                }
            }

            /* --------------------------------- HANDLING OF CAMPAIGN BUDGET --------------------------------- */

            if(isset($campaigns_by_campaign_as_index[$campaign_trimmed])) {
                $keyword->budget = $campaigns_by_campaign_as_index[$campaign_trimmed]->BUDGET;
            }   

            if(isset($keyword->budget) and isset($to_loop_keywords[$keyword_index_key])) {
                $keyword->budget += $to_loop_keywords[$keyword_index_key]->budget;
            }

            /* --------------------------------- HANDLING OF CAMPAIGN BUDGET --------------------------------- */

            $existing_revenue_value = null;
            if($hasCost ||  $hasRevenue) {

                if(isset($to_loop_keywords[$keyword_index_key])) {
                    (float)$keyword->IMPR += (float)$to_loop_keywords[$keyword_index_key]->IMPR;
                    (float)$keyword->SPEND += (float)$to_loop_keywords[$keyword_index_key]->SPEND;
                    (int)$keyword->CLICKS += (int)$to_loop_keywords[$keyword_index_key]->CLICKS;
                }
        
                
                /* -------------------------------- AGGREGATING THE REVENUE VALUES IN THE KEYWORD -------------------------------- */

                if($hasRevenue) {

                    if(!isset($keyword_revenues[$keyword_index_key])) {
                        $keyword_revenues[$keyword_index_key] = array();
                        $keyword_revenues[$keyword_index_key][] = $keyword_revenue;
                    } else {

                        /* ###################### MAKE SURE THAT THE REVENUE BEING ASSOCIATED TO THE KEYWORD INDEX IS NOT YET EXISTING ###################### */
                        
                        $isExisting = false;
                        foreach($keyword_revenues[$keyword_index_key] as $keyword_revenue_instance) {
                            if($keyword_revenue_instance['revenues'][0]['ID'] == $keyword_revenue['revenues'][0]['ID']) {
                                $isExisting = true;
                            }
                        }

                        if(!$isExisting) {
                            $keyword_revenues[$keyword_index_key][] = $keyword_revenue;
                        }

                        /* ###################### MAKE SURE THAT THE REVENUE BEING ASSOCIATED TO THE KEYWORD INDEX IS NOT YET EXISTING ###################### */
                    }

                }

                /* -------------------------------- AGGREGATING THE REVENUE VALUES IN THE KEYWORD -------------------------------- */

                
                $to_loop_keywords[$keyword_index_key] = $keyword;
            
            } else {

                $keyword_instance = Keyword::find($keyword->id);
                if($keyword_instance) {
                    $keyword_instance->delete($keyword->id);
                }

            }
        }

        $channel_indexes_keys = array_keys($channel_indexes);

        // raw_filtering of channels that wasnt used by keywords
        $unused_indexes = array_diff($channel_indexes_keys, $keyword_channel_indexes);
        $unused = array();

        foreach($unused_indexes as $unused_index) {
            array_push($unused, $channel_indexes[$unused_index]);
        }

        $unused_channels = array_diff($channel_ids, $channels_with_revenue);
        $difference = array_diff($revenue_raw, $revenue_used_in_channel);


        $data['formatted_bid_users'] = $formatted_bid_users;
        $data['google_conversion_data'] = $google_conversion_data;
        $data['revenues'] = $revenue_with_channel_data_key;
        $data['collated_revenue_with_channel_key'] = $collated_revenue_with_channel_key;
        $data['campaigns'] = $campaigns_by_campaign_as_index;
        $data['channel_tracer'] = $processed_channel_tracer;
        $data['processed_channel_tracer_by_id'] = $processed_channel_tracer_by_id;
        $data['keywords'] = $to_loop_keywords;
        $data['keyword_revenues'] = $keyword_revenues;
        $data['unused_channel_ids'] = $unused;
        $data['report_data'] = $report_data;

        return view('aggregated_keyword_performance', ["data"=>$data]);
    }

    /* --------------------------------------------------------------------------- FUNCTION SEPARATOR --------------------------------------------------------------------------- */

    public function create() {
        //
    }

    /* --------------------------------------------------------------------------- FUNCTION SEPARATOR --------------------------------------------------------------------------- */

    public function store(Request $request) {
        //
    }

    public function show(Keyword $keyword) {
        //
    }

    public function edit(Keyword $keyword) {
        //
    }

    /* --------------------------------------------------------------------------- FUNCTION SEPARATOR --------------------------------------------------------------------------- */

    public function update(Request $request, Keyword $keyword) {
        //
    }

    /* --------------------------------------------------------------------------- FUNCTION SEPARATOR --------------------------------------------------------------------------- */

    public function destroy(Keyword $keyword) {
        //
    }

    /* --------------------------------------------------------------------------- FUNCTION SEPARATOR --------------------------------------------------------------------------- */

    public function parse(Request $request) {

        // Get the UploadedFile object
        $file = $request->file('report');

        // You can store this but should validate it to avoid conflicts
        $original_name = $file->getClientOriginalName();

        // This would be used for the payload
        $csv_path = $file->getPathName();

        $isGoogle = false;
        $source = "Bing";
        if (strpos($original_name, 'google') !== false) {
            $source = "Google";
            $isGoogle = true;
        }

        $start = 0;
        $file_handle = fopen($csv_path, 'r');
        $created = 0;

        while (!feof($file_handle)) {

            $csv_data = fgetcsv($file_handle, 0, ",");
            
            if($isGoogle) {

                if($start == 1) {
                    $extraction_date = explode(" - ", $csv_data[0])[0];
                    $converted_time = strtotime($extraction_date);
                    $extraction_date = date("Y-m-d", $converted_time);
                }


                if($start > 3) {
                    if(isset($csv_data[14]) && !empty($csv_data[0])) {
                        $keyword = Keyword::create([
                            'STATUS'                => $csv_data[0], // 0 - Search keyword status
                            'KEYWORD'               => $csv_data[1], // 1 - Search keyword
                            'CAMPAIGN'              => $csv_data[2], // 2 - Campaign
                            'AD_GROUP'              => $csv_data[3], // 3 - Ad group
                            'MATCH_TYPE'            => $csv_data[4], // 4 - Search keyword match type
                            'BID_STRATEGY_TYPE'     => $csv_data[5], // 5 - Ad group bid strategy type
                            'DELIVERY'              => $csv_data[6], // 6 - Campaign bid strategy
                            'BID'                   => $csv_data[7], // 7 - Keyword max CPC
                            'AVG_POS'               => $csv_data[8], // 8 - Currency
                            'LABELS'                => $csv_data[9], // 9 - Labels on Keyword
                            'CLICKS'                => $csv_data[10], // 10 - Clicks
                            'IMPR'                  => str_replace(",", "", $csv_data[11]), // 11 - Impressions
                            'CTR'                   => $csv_data[12], // 12 - CTR
                            'AVG_CPC'               => $csv_data[13], // 13 - Avg. CPC
                            'SPEND'                 => $csv_data[14], // 14 - Cost
                            'EXTRACTION_DATE'       => $extraction_date,
                            'USER'                  => Auth::user()->campaign_user_id,
                            'SOURCE'                => $source
                        ]);

                        if($keyword) {
                            $created++;
                        }
                    }
                }

            } else {

                if($start == 0) {
                    $date_explode = explode(" ", $csv_data[1]);
                    $extraction_year = $date_explode[1];
                    $extraction_month = date("m", strtotime($date_explode[3]));
                    $extraction_day = $date_explode[4];
                    $extraction_date = $extraction_year."-".$extraction_month."-".$extraction_day;
                }

                if($start >= 4) {
                    if(isset($csv_data[14]) && !empty($csv_data[0])) {
                        // if($csv_data[9] > 0) {
                            $keyword = Keyword::create([
                                'STATUS'                => $csv_data[0],
                                'KEYWORD'               => $csv_data[1],
                                'CAMPAIGN'              => $csv_data[2],
                                'AD_GROUP'              => $csv_data[3],
                                'MATCH_TYPE'            => $csv_data[4],
                                'BID_STRATEGY_TYPE'     => $csv_data[5],
                                'DELIVERY'              => $csv_data[6],
                                'BID'                   => $csv_data[7],
                                'LABELS'                => $csv_data[8],
                                'CLICKS'                => $csv_data[9],
                                'IMPR'                  => $csv_data[10],
                                'CTR'                   => $csv_data[11],
                                'AVG_CPC'               => $csv_data[12],
                                'SPEND'                 => $csv_data[13],
                                'AVG_POS'               => $csv_data[14],
                                'EXTRACTION_DATE'       => $extraction_date,
                                'USER'                  => Auth::user()->campaign_user_id,
                                'SOURCE'                => $source
                            ]);

                            if($keyword) {
                                $created++;
                            }
                        // }
                    }
                }
            }

            $start++;

        }

        fclose($file_handle);
        echo $created . " records has been created";
        exit();
    }
}