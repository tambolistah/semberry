<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Campaign;
use App\Models\RevenueReport;
use App\Models\Channel;
use App\Models\ChannelTracer;
use App\Models\ChannelTracerNew;
use App\Models\BidUser;
USE DB;
use Auth;

use SoapVar;
use SoapFault;
use Exception;

// Specify the Microsoft\BingAds\V13\Reporting classes that will be used.
use Microsoft\BingAds\V13\Reporting\SubmitGenerateReportRequest;
use Microsoft\BingAds\V13\Reporting\PollGenerateReportRequest;
use Microsoft\BingAds\V13\Reporting\AccountPerformanceReportRequest;
use Microsoft\BingAds\V13\Reporting\AudiencePerformanceReportRequest;
use Microsoft\BingAds\V13\Reporting\KeywordPerformanceReportRequest;
use Microsoft\BingAds\V13\Reporting\ReportFormat;
use Microsoft\BingAds\V13\Reporting\ReportAggregation;
use Microsoft\BingAds\V13\Reporting\AccountThroughAdGroupReportScope;
use Microsoft\BingAds\V13\Reporting\CampaignReportScope;
use Microsoft\BingAds\V13\Reporting\AccountReportScope;
use Microsoft\BingAds\V13\Reporting\ReportTime;
use Microsoft\BingAds\V13\Reporting\ReportTimePeriod;
use Microsoft\BingAds\V13\Reporting\Date;
use Microsoft\BingAds\V13\Reporting\AccountPerformanceReportFilter;
use Microsoft\BingAds\V13\Reporting\KeywordPerformanceReportFilter;
use Microsoft\BingAds\V13\Reporting\DeviceTypeReportFilter;
use Microsoft\BingAds\V13\Reporting\AccountPerformanceReportColumn;
use Microsoft\BingAds\V13\Reporting\AudiencePerformanceReportColumn;
use Microsoft\BingAds\V13\Reporting\KeywordPerformanceReportColumn;
use Microsoft\BingAds\V13\Reporting\ReportRequestStatusType;



// Specify the Microsoft\BingAds\Auth classes that will be used.
use Microsoft\BingAds\Auth\ServiceClient;
use Microsoft\BingAds\Auth\ServiceClientType;

// Specify the Microsoft\BingAds\Samples classes that will be used.
use Microsoft\BingAds\Samples\V13\AuthHelper;
use Microsoft\BingAds\Samples\V13\ReportingExampleHelper;

// Specify the file to download the report to. Because the file is
// compressed use the .zip file extension.

class CampaignController extends Controller {
    
    public function index()
    {
        echo 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        exit();
    }

    /* ------------------------------------------------- CAMPAIGN AD GROUP FUNCTIONS ------------------------------------------------- */

    public function clean($string) {
        $string = str_replace("Â", "", $string);
        $string = preg_replace('/\xc2\xa0/', '', $string);
        $string = urlencode(str_replace(" ", "", trim(strtolower($string))));
        return $string;
    }

    /* ------------------------------------------------- CAMPAIGN AD GROUP FUNCTIONS ------------------------------------------------- */

    public function new_group_performance(Request $request) {
        
        $source = "bing";
        $data = array();
        $report_data = array();

        if($request->input('source')) {
            $source = $request->input('source');
        }

        $start_date = date('Y-m-d',(strtotime ( '-1 day' , time() ) ));
        $end_date = date("Y-m-d");

        $fields_to_select = array("CHANNEL", "KEYWORD", "CAMPAIGN", "DATE", "KEYWORD", "MATCH_TYPE", "COUNTRY");
        $consolidated_channels = array();

        if($request->input('start') && $request->input('end')) {
            $start_date = $request->input('start');
            $end_date = $request->input('end');
        }

        
        /* @@@@@@@@@@@@@@@@@@@@@@@@@@@@@ BIDUSERS @@@@@@@@@@@@@@@@@@@@@@@@@@@@@ */

        $bid_users = BidUser::select(array("USERNAME", "SUBID"))->get();
        $formatted_bid_users = array();

        foreach($bid_users as $bid_user) {
            $formatted_bid_users[$bid_user->SUBID] = $bid_user->USERNAME;
        }

        /* @@@@@@@@@@@@@@@@@@@@@@@@@@@@@ BIDUSERS @@@@@@@@@@@@@@@@@@@@@@@@@@@@@ */



        /* ------------------------------------------------------------- REPORT VALIDATION SECTION ------------------------------------------------------------- */

        $campaign_reports = "SELECT EXTRACTION_DATE, SUM(BUDGET) as budget, SUM(CLICKS) as campaign_clicks, SUM(SPEND) as cost FROM CAMPAIGN_COST WHERE IMPR <> 0 AND SOURCE = '$source' AND EXTRACTION_DATE >= '$start_date' AND EXTRACTION_DATE <= '$end_date' GROUP BY EXTRACTION_DATE order by EXTRACTION_DATE desc";

        if($source == "bing") {
            $revenue_reports = "SELECT DATE, SUM(CLICKS) as clicks, SUM(EARNINGS) as earnings FROM REVENUE_REPORT WHERE CUSTOM_CHANNEL_NAME <= 'T0000899' AND DATE >= '$start_date' AND DATE <= '$end_date' GROUP BY DATE order by DATE desc";
        } else {
            $revenue_reports = "SELECT DATE, SUM(CLICKS) as clicks, SUM(EARNINGS) as earnings FROM REVENUE_REPORT WHERE CUSTOM_CHANNEL_NAME > 'T0000899' AND DATE >= '$start_date' AND DATE <= '$end_date' GROUP BY DATE order by DATE desc";
        }

        $campaign_report_query = DB::select($campaign_reports);
        $revenue_report_query = DB::select($revenue_reports);

        $campaign_date_index = array();
        foreach($campaign_report_query as $campaign_report_instance) {
            $campaign_date_index[$campaign_report_instance->EXTRACTION_DATE] = $campaign_report_instance;
        }

        $report_data = array(
            "revenue_reports" => $revenue_report_query,
            "campaign_reports" => $campaign_date_index 
        );

        /* ------------------------------------------------------------- REPORT VALIDATION SECTION ------------------------------------------------------------- */

        


        /* ##################################################### KEYWORD DATE PROCESSING ##################################################### */

        $bid_cost_by_campaign = array();
        $bid_cost_by_campaign_date = array();

        $keyword_cost_data = DB::select("
            SELECT EXTRACTION_DATE, AD_GROUP, CAMPAIGN, MAX(BID) as BID 
            FROM KEYWORD_COST
            where SOURCE = '$source' AND EXTRACTION_DATE >= '$start_date' && EXTRACTION_DATE <= '$end_date'
            GROUP BY CAMPAIGN, EXTRACTION_DATE ORDER BY CAMPAIGN ASC;           
        ");

        // return $keyword_cost_data;


        $keyword_adgroup_by_campaign_date = array();
        foreach($keyword_cost_data as $keyword_cost_instance) {

            $keyword_campaign_index_key = $this->clean($keyword_cost_instance->CAMPAIGN);
            if(!isset($bid_cost_by_campaign[$keyword_campaign_index_key])) {
                $bid_cost_by_campaign[$keyword_campaign_index_key] = $keyword_cost_instance->BID;
            } else {
                if($bid_cost_by_campaign[$keyword_campaign_index_key] < $keyword_cost_instance->BID) {
                    $bid_cost_by_campaign[$keyword_campaign_index_key] = $keyword_cost_instance->BID;
                }
            }

            if(!isset($bid_cost_by_campaign_date[$keyword_campaign_index_key ."-". $keyword_cost_instance->EXTRACTION_DATE])) {
                $bid_cost_by_campaign_date[$keyword_campaign_index_key ."-". $keyword_cost_instance->EXTRACTION_DATE] = array();
                $bid_cost_by_campaign_date[$keyword_campaign_index_key ."-". $keyword_cost_instance->EXTRACTION_DATE] = $keyword_cost_instance->BID;
                $keyword_adgroup_by_campaign_date[$keyword_campaign_index_key ."-". $keyword_cost_instance->EXTRACTION_DATE] = $keyword_cost_instance->AD_GROUP;
            }

        }

        $processed_keyword_data = array('bid_cost_by_campaign' => $bid_cost_by_campaign, 'bid_cost_by_campaign_date' => $bid_cost_by_campaign_date);

        /* ##################################################### KEYWORD DATE PROCESSING ##################################################### */


        
        /* ########################################### CHANNEL TABLE CONTAINS CURRENT DATE RECORDS AND CHANNEL TRACER CONTAINS HISTORIC RECORDS ###########################################  */

        if($end_date == date("Y-m-d")) {
            $fields_for_current_channel = $fields_to_select;
            unset($fields_for_current_channel[4]);
            unset($fields_for_current_channel[5]);

            $channel = Channel::select($fields_for_current_channel)->where("CAMPAIGN","<>","")->where("SOURCE",$source)->get();
            $consolidated_channels = $channel->toArray();
        }


        /* ########################################### CHANNEL TABLE CONTAINS CURRENT DATE RECORDS AND CHANNEL TRACER CONTAINS HISTORIC RECORDS ###########################################  */

        
        $historic_channel = ChannelTracerNew::select($fields_to_select)->where("DATE",">=", $start_date)->where("DATE","<=", $end_date)->where("CAMPAIGN","<>","")->where("SOURCE",$source)->orderBy("DATE", "ASC")->get();
        $channel_tracer = $historic_channel->toArray();

        $campaigns = Campaign::where("CAMPAIGN", "<>", "UNKNOWN")->where("IMPR", "<>", "0")->where("EXTRACTION_DATE",">=", $start_date)->where("EXTRACTION_DATE","<=", $end_date)->where("SOURCE",$source)->orderBy("EXTRACTION_DATE", "ASC")->get();
        
        $raw_total_budget = 0;
        foreach($campaigns as $campaign_instance) {
            $raw_total_budget += (float)$campaign_instance->BUDGET;
        }

        // $revenues = RevenueReport::where("DATE",">=", $start_date)->where("DATE","<=", $end_date)->orderBy("DATE", "ASC")->get();
        if($source == "bing") {
            $revenues = RevenueReport::where("DATE",">=", $start_date)->where("DATE","<=", $end_date)->where("CUSTOM_CHANNEL_NAME","<=", "T0000899")->orderBy("DATE", "ASC")->get();
        } else {
            $revenues = RevenueReport::where("DATE",">=", $start_date)->where("DATE","<=", $end_date)->where("CUSTOM_CHANNEL_NAME",">=", "T0000900")->orderBy("DATE", "ASC")->get();
        }

        $consolidated_channels = array_merge($consolidated_channels, $channel_tracer);

        // return $consolidated_channels;


        /* ######################################## USING RELATIVE DATA TO MAKE IT ARRAY INDEX FOR COLLATED COMPUTATION ######################################## */

        $report_data['campaigns'] = $campaigns;
        $report_data['campaigns'] = json_decode(json_encode($report_data['campaigns']));
        

        /* ################################################### REVENUE COMPILATION WITH CHANNEL AND DATE INDEX KEYS ################################################### */

        $total_revenues = 0;    
        $revenue_with_channel_data_key = array();
        foreach($revenues as $revenue) {
            if(!isset($revenue_with_channel_data_key[$revenue->CUSTOM_CHANNEL_NAME."-".$revenue->DATE])) {
                $revenue_with_channel_data_key[$revenue->CUSTOM_CHANNEL_NAME."-".$revenue->DATE] = array();
                $revenue_with_channel_data_key[$revenue->CUSTOM_CHANNEL_NAME."-".$revenue->DATE][] = $revenue;
            } else {
                $revenue_with_channel_data_key[$revenue->CUSTOM_CHANNEL_NAME."-".$revenue->DATE][] = $revenue;
            }

            $total_revenues += $revenue->EARNINGS;
        }

        // return $revenue_with_channel_data_key;

        /* ################################################### REVENUE COMPILATION WITH CHANNEL AND DATE INDEX KEYS ################################################### */

        



        /* ################################################### ASSOCIATING REVENUES TO CHANNELS ################################################### */        

        $channel_with_campaign_key = array();
        $channel_with_campaign_and_date_key = array();
        $channel_with_revenues_total = 0;

        foreach($consolidated_channels as $consolidated_channel) {
            $channel_index_key = $consolidated_channel['CHANNEL']."-".$consolidated_channel['DATE'];

            // echo $channel_index_key;
            // echo "<br />";

            if(isset($revenue_with_channel_data_key[$channel_index_key])) {
                $consolidated_channel['REVENUE'] = $revenue_with_channel_data_key[$channel_index_key];
            }                


            $consolidated_channel['CAMPAIGN'] = str_replace("Â", "", $consolidated_channel['CAMPAIGN']);
            $channel_campaign_index_key = $this->clean($consolidated_channel['CAMPAIGN']);
            if(isset($channel_with_campaign_key[$channel_campaign_index_key])) {
                
                $channel_with_campaign_key[$channel_campaign_index_key][] = $consolidated_channel;
                $channel_with_campaign_and_date_key[$channel_campaign_index_key."-".$consolidated_channel['DATE']][] = $consolidated_channel;

            } else {
                $channel_with_campaign_key[$channel_campaign_index_key] = array();
                $channel_with_campaign_key[$channel_campaign_index_key][] = $consolidated_channel;

                $channel_with_campaign_and_date_key[$channel_campaign_index_key."-".$consolidated_channel['DATE']] = array();
                $channel_with_campaign_and_date_key[$channel_campaign_index_key."-".$consolidated_channel['DATE']][] = $consolidated_channel;
            }
        }

        $camapgin_channel_revenues = 0;
        foreach($channel_with_campaign_key as $channel_with_campaign_instance) {
            foreach($channel_with_campaign_instance as $campaign_channels) {

                if(isset($campaign_channels['REVENUE'])) {
                    foreach($campaign_channels['REVENUE'] as $revenue_instance) {
                        $camapgin_channel_revenues += (float)$revenue_instance->EARNINGS;
                    }
                }

            }
        }

        $report_data['channel_with_campaign_data_key'] = $channel_with_campaign_key;
        $report_data['channel_with_campaign_and_date_key'] = $channel_with_campaign_and_date_key;

        /* ######################################## USING RELATIVE DATA TO MAKE IT ARRAY INDEX FOR COLLATED COMPUTATION ######################################## */

        $collated_campaign_values = array();
        $campaign_clean_campaign_index_keys = array();
        foreach($campaigns as $campaign) {
            $campaign_clean_campaign_index_keys[$this->clean($campaign->CAMPAIGN)]  = $campaign;
            
            // ------------------- aggregated campaign value processing ------------------- //
            
            if(isset($collated_campaign_values[$this->clean($campaign->CAMPAIGN)])) {

                if($collated_campaign_values[$this->clean($campaign->CAMPAIGN)]->CAMPAIGN_ID != "UNKNOWN") {
                    (float)$collated_campaign_values[$this->clean($campaign->CAMPAIGN)]->BUDGET       += (float)$campaign->BUDGET;
                    (float)$collated_campaign_values[$this->clean($campaign->CAMPAIGN)]->CLICKS       += (float)$campaign->CLICKS;
                    (int)$collated_campaign_values[$this->clean($campaign->CAMPAIGN)]->IMPR           += (float)$campaign->IMPR;
                    
                    if(!empty($campaign->AVG_CPC) && is_numeric($campaign->AVG_CPC) && is_numeric($collated_campaign_values[$this->clean($campaign->CAMPAIGN)]->AVG_CPC)) {
                        (float)$collated_campaign_values[$this->clean($campaign->CAMPAIGN)]->AVG_CPC      += (float)$campaign->AVG_CPC;
                    } else {
                        $campaign->AVG_CPC = 0;
                    }

                    (float)$collated_campaign_values[$this->clean($campaign->CAMPAIGN)]->SPEND        += (float)$campaign->SPEND;
                    
                    if(is_int($campaign->CONV) || is_float($campaign->CONV)) {
                        (float)$collated_campaign_values[$this->clean($campaign->CAMPAIGN)]->CONV     += (float)$campaign->CONV;
                    }

                    (float)$collated_campaign_values[$this->clean($campaign->CAMPAIGN)]->AVG_POS      += (float)$campaign->AVG_POS;
                }

            } else {
                $collated_campaign_values[$this->clean($campaign->CAMPAIGN)] = $campaign;
            }

            // ------------------- aggregated campaign value processing ------------------- //
        }

        $channel_campaign_total_revenues = 0;
        $raw_campaign_keys = array_keys($campaign_clean_campaign_index_keys);
        $channels_with_no_matching_campaign = array();


        $further_revenue_test = 0;
        $channel_no_campaign_revenues = 0;

        foreach($channel_with_campaign_key as $channel_campaign_key => $channel_with_campaign_data_instance) {    
            
            foreach($channel_with_campaign_data_instance as $loop_key => $channel_with_campaign_instance) {
                if(isset($channel_with_campaign_instance['REVENUE']) && !empty($channel_with_campaign_instance['REVENUE'])) {
                    foreach($channel_with_campaign_instance['REVENUE'] as $revenue_instance) {
                        $further_revenue_test += (float)$revenue_instance->EARNINGS;
                    }
                    
                    if(in_array($channel_campaign_key, $raw_campaign_keys)) {
                        foreach($channel_with_campaign_instance['REVENUE'] as $revenue_instance) {
                            $channel_campaign_total_revenues += (float)$revenue_instance->EARNINGS;
                        }
                    } else {
                        $channels_with_no_matching_campaign[$channel_campaign_key][] = $channel_with_campaign_instance;
                        foreach($channel_with_campaign_instance['REVENUE'] as $revenue_instance) {
                            $channel_campaign_total_revenues += (float)$revenue_instance->EARNINGS;
                            
                        }
                    }
                }
            }
        }


        $cost_report = $collated_campaign_values;

        $grouped_by_campaign_key = array();
        foreach($cost_report as $cost_report_instance) {            

            $campaign                           = urlencode($cost_report_instance->CAMPAIGN);
            $campaign                           = str_replace("%C2%A0", "+", $campaign);
            $cost_report_instance->CAMPAIGN     = urldecode($campaign);

            $cost_report_instance->CAMPAIGN     = str_replace("Â", "", $cost_report_instance->CAMPAIGN);
            $casmpaign_index_key_cleaned        = $this->clean($cost_report_instance->CAMPAIGN);

            if(!isset($grouped_by_campaign_key[$casmpaign_index_key_cleaned])) {
                $grouped_by_campaign_key[$casmpaign_index_key_cleaned] = array();
                $grouped_by_campaign_key[$casmpaign_index_key_cleaned][$cost_report_instance->DATE] = $cost_report_instance;
            } else {
                $grouped_by_campaign_key[$casmpaign_index_key_cleaned][$cost_report_instance->DATE] = $cost_report_instance;
            }

        }

        $data['formatted_bid_users'] = $formatted_bid_users;
        $data['keyword_adgroup_by_campaign_date'] = $keyword_adgroup_by_campaign_date;
        $data['channels_with_no_matching_campaign'] = $channels_with_no_matching_campaign;
        $data['cost_report'] = $cost_report;
        $data['processed_report'] = $report_data;
        $data['processed_keyword_data'] = $processed_keyword_data;
        $data['grouped_by_campaign_key'] = $grouped_by_campaign_key;
        $data['report_data'] = $report_data;

        return view('group_performance', ["data"=>$data]);
    }

    /* #################################################### FUNCTION SEPARATOR #################################################### */

    public function group_performance(Request $request) {
        
        $source = "bing";
        $data = array();
        $report_data = array();

        if($request->input('source')) {
            $source = $request->input('source');
        }

        $start_date = date('Y-m-d',(strtotime ( '-1 day' , time() ) ));
        $end_date = date("Y-m-d");

        $fields_to_select = array("CHANNEL", "KEYWORD", "CAMPAIGN", "DATE", "KEYWORD", "MATCH_TYPE", "COUNTRY");
        $consolidated_channels = array();

        if($request->input('start') && $request->input('end')) {
            $start_date = $request->input('start');
            $end_date = $request->input('end');
        }

        
        /* @@@@@@@@@@@@@@@@@@@@@@@@@@@@@ BIDUSERS @@@@@@@@@@@@@@@@@@@@@@@@@@@@@ */

        $bid_users = BidUser::select(array("USERNAME", "SUBID"))->get();
        $formatted_bid_users = array();

        foreach($bid_users as $bid_user) {
            $formatted_bid_users[$bid_user->SUBID] = $bid_user->USERNAME;
        }

        /* @@@@@@@@@@@@@@@@@@@@@@@@@@@@@ BIDUSERS @@@@@@@@@@@@@@@@@@@@@@@@@@@@@ */



        /* ------------------------------------------------------------- REPORT VALIDATION SECTION ------------------------------------------------------------- */

        $campaign_reports = "SELECT EXTRACTION_DATE, SUM(BUDGET) as budget, SUM(CLICKS) as campaign_clicks, SUM(SPEND) as cost FROM CAMPAIGN_COST WHERE IMPR <> 0 AND SOURCE = '$source' AND EXTRACTION_DATE >= '$start_date' AND EXTRACTION_DATE <= '$end_date' GROUP BY EXTRACTION_DATE order by EXTRACTION_DATE desc";

        if($source == "bing") {
            $revenue_reports = "SELECT DATE, SUM(CLICKS) as clicks, SUM(EARNINGS) as earnings FROM REVENUE_REPORT WHERE CUSTOM_CHANNEL_NAME <= 'T0000899' AND DATE >= '$start_date' AND DATE <= '$end_date' GROUP BY DATE order by DATE desc";
        } else {
            $revenue_reports = "SELECT DATE, SUM(CLICKS) as clicks, SUM(EARNINGS) as earnings FROM REVENUE_REPORT WHERE CUSTOM_CHANNEL_NAME > 'T0000899' AND DATE >= '$start_date' AND DATE <= '$end_date' GROUP BY DATE order by DATE desc";
        }

        $campaign_report_query = DB::select($campaign_reports);
        $revenue_report_query = DB::select($revenue_reports);

        $campaign_date_index = array();
        foreach($campaign_report_query as $campaign_report_instance) {
            $campaign_date_index[$campaign_report_instance->EXTRACTION_DATE] = $campaign_report_instance;
        }

        $report_data = array(
            "revenue_reports" => $revenue_report_query,
            "campaign_reports" => $campaign_date_index 
        );

        /* ------------------------------------------------------------- REPORT VALIDATION SECTION ------------------------------------------------------------- */

        


        /* ##################################################### KEYWORD DATE PROCESSING ##################################################### */

        $bid_cost_by_campaign = array();
        $bid_cost_by_campaign_date = array();

        $keyword_cost_data = DB::select("
            SELECT EXTRACTION_DATE, AD_GROUP, CAMPAIGN, MAX(BID) as BID 
            FROM KEYWORD_COST
            where SOURCE = '$source' AND EXTRACTION_DATE >= '$start_date' && EXTRACTION_DATE <= '$end_date'
            GROUP BY CAMPAIGN, EXTRACTION_DATE ORDER BY CAMPAIGN ASC;           
        ");

        // return $keyword_cost_data;


        $keyword_adgroup_by_campaign_date = array();
        foreach($keyword_cost_data as $keyword_cost_instance) {

            $keyword_campaign_index_key = $this->clean($keyword_cost_instance->CAMPAIGN);
            if(!isset($bid_cost_by_campaign[$keyword_campaign_index_key])) {
                $bid_cost_by_campaign[$keyword_campaign_index_key] = $keyword_cost_instance->BID;
            } else {
                if($bid_cost_by_campaign[$keyword_campaign_index_key] < $keyword_cost_instance->BID) {
                    $bid_cost_by_campaign[$keyword_campaign_index_key] = $keyword_cost_instance->BID;
                }
            }

            if(!isset($bid_cost_by_campaign_date[$keyword_campaign_index_key ."-". $keyword_cost_instance->EXTRACTION_DATE])) {
                $bid_cost_by_campaign_date[$keyword_campaign_index_key ."-". $keyword_cost_instance->EXTRACTION_DATE] = array();
                $bid_cost_by_campaign_date[$keyword_campaign_index_key ."-". $keyword_cost_instance->EXTRACTION_DATE] = $keyword_cost_instance->BID;
                $keyword_adgroup_by_campaign_date[$keyword_campaign_index_key ."-". $keyword_cost_instance->EXTRACTION_DATE] = $keyword_cost_instance->AD_GROUP;
            }

        }

        $processed_keyword_data = array('bid_cost_by_campaign' => $bid_cost_by_campaign, 'bid_cost_by_campaign_date' => $bid_cost_by_campaign_date);

        /* ##################################################### KEYWORD DATE PROCESSING ##################################################### */


        
        /* ########################################### CHANNEL TABLE CONTAINS CURRENT DATE RECORDS AND CHANNEL TRACER CONTAINS HISTORIC RECORDS ###########################################  */

        if($end_date == date("Y-m-d")) {
            $fields_for_current_channel = $fields_to_select;
            unset($fields_for_current_channel[4]);
            unset($fields_for_current_channel[5]);

            $channel = Channel::select($fields_for_current_channel)->where("CAMPAIGN","<>","")->where("SOURCE",$source)->get();
            $consolidated_channels = $channel->toArray();
        }


        /* ########################################### CHANNEL TABLE CONTAINS CURRENT DATE RECORDS AND CHANNEL TRACER CONTAINS HISTORIC RECORDS ###########################################  */

        
        $historic_channel = ChannelTracer::select($fields_to_select)->where("DATE",">=", $start_date)->where("DATE","<=", $end_date)->where("CAMPAIGN","<>","")->where("SOURCE",$source)->orderBy("DATE", "ASC")->get();
        $channel_tracer = $historic_channel->toArray();

        $campaigns = Campaign::where("CAMPAIGN", "<>", "UNKNOWN")->where("IMPR", "<>", "0")->where("EXTRACTION_DATE",">=", $start_date)->where("EXTRACTION_DATE","<=", $end_date)->where("SOURCE",$source)->orderBy("EXTRACTION_DATE", "ASC")->get();
        
        $raw_total_budget = 0;
        foreach($campaigns as $campaign_instance) {
            $raw_total_budget += (float)$campaign_instance->BUDGET;
        }

        // $revenues = RevenueReport::where("DATE",">=", $start_date)->where("DATE","<=", $end_date)->orderBy("DATE", "ASC")->get();
        if($source == "bing") {
            $revenues = RevenueReport::where("DATE",">=", $start_date)->where("DATE","<=", $end_date)->where("CUSTOM_CHANNEL_NAME","<=", "T0000899")->orderBy("DATE", "ASC")->get();
        } else {
            $revenues = RevenueReport::where("DATE",">=", $start_date)->where("DATE","<=", $end_date)->where("CUSTOM_CHANNEL_NAME",">=", "T0000900")->orderBy("DATE", "ASC")->get();
        }

        $consolidated_channels = array_merge($consolidated_channels, $channel_tracer);

        // return $consolidated_channels;


        /* ######################################## USING RELATIVE DATA TO MAKE IT ARRAY INDEX FOR COLLATED COMPUTATION ######################################## */

        $report_data['campaigns'] = $campaigns;
        $report_data['campaigns'] = json_decode(json_encode($report_data['campaigns']));
        

        /* ################################################### REVENUE COMPILATION WITH CHANNEL AND DATE INDEX KEYS ################################################### */

        $total_revenues = 0;    
        $revenue_with_channel_data_key = array();
        foreach($revenues as $revenue) {
            if(!isset($revenue_with_channel_data_key[$revenue->CUSTOM_CHANNEL_NAME."-".$revenue->DATE])) {
                $revenue_with_channel_data_key[$revenue->CUSTOM_CHANNEL_NAME."-".$revenue->DATE] = array();
                $revenue_with_channel_data_key[$revenue->CUSTOM_CHANNEL_NAME."-".$revenue->DATE][] = $revenue;
            } else {
                $revenue_with_channel_data_key[$revenue->CUSTOM_CHANNEL_NAME."-".$revenue->DATE][] = $revenue;
            }

            $total_revenues += $revenue->EARNINGS;
        }

        // return $revenue_with_channel_data_key;

        /* ################################################### REVENUE COMPILATION WITH CHANNEL AND DATE INDEX KEYS ################################################### */

        



        /* ################################################### ASSOCIATING REVENUES TO CHANNELS ################################################### */        

        $channel_with_campaign_key = array();
        $channel_with_campaign_and_date_key = array();
        $channel_with_revenues_total = 0;

        foreach($consolidated_channels as $consolidated_channel) {
            $channel_index_key = $consolidated_channel['CHANNEL']."-".$consolidated_channel['DATE'];

            // echo $channel_index_key;
            // echo "<br />";

            if(isset($revenue_with_channel_data_key[$channel_index_key])) {
                $consolidated_channel['REVENUE'] = $revenue_with_channel_data_key[$channel_index_key];
            }                


            $consolidated_channel['CAMPAIGN'] = str_replace("Â", "", $consolidated_channel['CAMPAIGN']);
            $channel_campaign_index_key = $this->clean($consolidated_channel['CAMPAIGN']);
            if(isset($channel_with_campaign_key[$channel_campaign_index_key])) {
                
                $channel_with_campaign_key[$channel_campaign_index_key][] = $consolidated_channel;
                $channel_with_campaign_and_date_key[$channel_campaign_index_key."-".$consolidated_channel['DATE']][] = $consolidated_channel;

            } else {
                $channel_with_campaign_key[$channel_campaign_index_key] = array();
                $channel_with_campaign_key[$channel_campaign_index_key][] = $consolidated_channel;

                $channel_with_campaign_and_date_key[$channel_campaign_index_key."-".$consolidated_channel['DATE']] = array();
                $channel_with_campaign_and_date_key[$channel_campaign_index_key."-".$consolidated_channel['DATE']][] = $consolidated_channel;
            }
        }

        $camapgin_channel_revenues = 0;
        foreach($channel_with_campaign_key as $channel_with_campaign_instance) {
            foreach($channel_with_campaign_instance as $campaign_channels) {

                if(isset($campaign_channels['REVENUE'])) {
                    foreach($campaign_channels['REVENUE'] as $revenue_instance) {
                        $camapgin_channel_revenues += (float)$revenue_instance->EARNINGS;
                    }
                }

            }
        }

        $report_data['channel_with_campaign_data_key'] = $channel_with_campaign_key;
        $report_data['channel_with_campaign_and_date_key'] = $channel_with_campaign_and_date_key;

        /* ######################################## USING RELATIVE DATA TO MAKE IT ARRAY INDEX FOR COLLATED COMPUTATION ######################################## */

        $collated_campaign_values = array();
        $campaign_clean_campaign_index_keys = array();
        foreach($campaigns as $campaign) {
            $campaign_clean_campaign_index_keys[$this->clean($campaign->CAMPAIGN)]  = $campaign;
            
            // ------------------- aggregated campaign value processing ------------------- //
            
            if(isset($collated_campaign_values[$this->clean($campaign->CAMPAIGN)])) {

                if($collated_campaign_values[$this->clean($campaign->CAMPAIGN)]->CAMPAIGN_ID != "UNKNOWN") {
                    (float)$collated_campaign_values[$this->clean($campaign->CAMPAIGN)]->BUDGET       += (float)$campaign->BUDGET;
                    (float)$collated_campaign_values[$this->clean($campaign->CAMPAIGN)]->CLICKS       += (float)$campaign->CLICKS;
                    (int)$collated_campaign_values[$this->clean($campaign->CAMPAIGN)]->IMPR           += (float)$campaign->IMPR;
                    
                    if(!empty($campaign->AVG_CPC) && is_numeric($campaign->AVG_CPC) && is_numeric($collated_campaign_values[$this->clean($campaign->CAMPAIGN)]->AVG_CPC)) {
                        (float)$collated_campaign_values[$this->clean($campaign->CAMPAIGN)]->AVG_CPC      += (float)$campaign->AVG_CPC;
                    } else {
                        $campaign->AVG_CPC = 0;
                    }

                    (float)$collated_campaign_values[$this->clean($campaign->CAMPAIGN)]->SPEND        += (float)$campaign->SPEND;
                    
                    if(is_int($campaign->CONV) || is_float($campaign->CONV)) {
                        (float)$collated_campaign_values[$this->clean($campaign->CAMPAIGN)]->CONV     += (float)$campaign->CONV;
                    }

                    (float)$collated_campaign_values[$this->clean($campaign->CAMPAIGN)]->AVG_POS      += (float)$campaign->AVG_POS;
                }

            } else {
                $collated_campaign_values[$this->clean($campaign->CAMPAIGN)] = $campaign;
            }

            // ------------------- aggregated campaign value processing ------------------- //
        }

        $channel_campaign_total_revenues = 0;
        $raw_campaign_keys = array_keys($campaign_clean_campaign_index_keys);
        $channels_with_no_matching_campaign = array();


        $further_revenue_test = 0;
        $channel_no_campaign_revenues = 0;

        foreach($channel_with_campaign_key as $channel_campaign_key => $channel_with_campaign_data_instance) {    
            
            foreach($channel_with_campaign_data_instance as $loop_key => $channel_with_campaign_instance) {
                if(isset($channel_with_campaign_instance['REVENUE']) && !empty($channel_with_campaign_instance['REVENUE'])) {
                    foreach($channel_with_campaign_instance['REVENUE'] as $revenue_instance) {
                        $further_revenue_test += (float)$revenue_instance->EARNINGS;
                    }
                    
                    if(in_array($channel_campaign_key, $raw_campaign_keys)) {
                        foreach($channel_with_campaign_instance['REVENUE'] as $revenue_instance) {
                            $channel_campaign_total_revenues += (float)$revenue_instance->EARNINGS;
                        }
                    } else {
                        $channels_with_no_matching_campaign[$channel_campaign_key][] = $channel_with_campaign_instance;
                        foreach($channel_with_campaign_instance['REVENUE'] as $revenue_instance) {
                            $channel_campaign_total_revenues += (float)$revenue_instance->EARNINGS;
                            
                        }
                    }
                }
            }
        }


        $cost_report = $collated_campaign_values;

        $grouped_by_campaign_key = array();
        foreach($cost_report as $cost_report_instance) {            

            $campaign                           = urlencode($cost_report_instance->CAMPAIGN);
            $campaign                           = str_replace("%C2%A0", "+", $campaign);
            $cost_report_instance->CAMPAIGN     = urldecode($campaign);

            $cost_report_instance->CAMPAIGN     = str_replace("Â", "", $cost_report_instance->CAMPAIGN);
            $casmpaign_index_key_cleaned        = $this->clean($cost_report_instance->CAMPAIGN);

            if(!isset($grouped_by_campaign_key[$casmpaign_index_key_cleaned])) {
                $grouped_by_campaign_key[$casmpaign_index_key_cleaned] = array();
                $grouped_by_campaign_key[$casmpaign_index_key_cleaned][$cost_report_instance->DATE] = $cost_report_instance;
            } else {
                $grouped_by_campaign_key[$casmpaign_index_key_cleaned][$cost_report_instance->DATE] = $cost_report_instance;
            }

        }

        $data['formatted_bid_users'] = $formatted_bid_users;
        $data['keyword_adgroup_by_campaign_date'] = $keyword_adgroup_by_campaign_date;
        $data['channels_with_no_matching_campaign'] = $channels_with_no_matching_campaign;
        $data['cost_report'] = $cost_report;
        $data['processed_report'] = $report_data;
        $data['processed_keyword_data'] = $processed_keyword_data;
        $data['grouped_by_campaign_key'] = $grouped_by_campaign_key;
        $data['report_data'] = $report_data;

        return view('group_performance', ["data"=>$data]);
    }

    /* #################################################### FUNCTION SEPARATOR #################################################### */

    public function kwd_performance() {
        $data = array();
        return view('kwd_performance', ["data"=>$data]);
    }

    /* ------------------------------------------------- CAMPAIGN AD GROUP FUNCTIONS ------------------------------------------------- */

    public function create() {
        //
    }

    /* --------------------------------------------------------------------------- FUNCTION SEPARATOR --------------------------------------------------------------------------- */

    public function store(Request $request) {
        //
    }

    /* --------------------------------------------------------------------------- FUNCTION SEPARATOR --------------------------------------------------------------------------- */

    public function show($id) {
        //
    }

    /* --------------------------------------------------------------------------- FUNCTION SEPARATOR --------------------------------------------------------------------------- */

    public function edit($id) {
        //
    }

    /* --------------------------------------------------------------------------- FUNCTION SEPARATOR --------------------------------------------------------------------------- */

    public function update(Request $request, $id) {
        //
    }

    /* --------------------------------------------------------------------------- FUNCTION SEPARATOR --------------------------------------------------------------------------- */

    public function destroy($id) {
        //
    }

    /* --------------------------------------------------------------------------- FUNCTION SEPARATOR --------------------------------------------------------------------------- */

    public function parse(Request $request) {
        
        // Get the UploadedFile object
        $file = $request->file('report');

        // You can store this but should validate it to avoid conflicts
        $original_name = $file->getClientOriginalName();

        // This would be used for the payload
        $csv_path = $file->getPathName();

        $isGoogle = false;
        $source = "Bing";
        if (strpos($original_name, 'google') !== false) {
            $source = "Google";
            $isGoogle = true;
        }

        $start = 0;
        $file_handle = fopen($csv_path, 'r');
        $extraction_date = "";
        $created = 0;

        $total_spend = 0;
        $overall_spend = 0;

        while (!feof($file_handle)) {
            
            $csv_data = fgetcsv($file_handle, 0, ",");
            if($isGoogle) {

                if($start == 1) {
                    $extraction_date = explode(" - ", $csv_data[0])[0];
                    $converted_time = strtotime($extraction_date);
                    $extraction_date = date("Y-m-d", $converted_time);
                }

                if($start > 3) {
                    if(!empty($csv_data[0])) {
                        $campaign = Campaign::create([
                            'CAMPAIGN_ID'               => $csv_data[0], // 0 - Campaign ID
                            'STATUS'                    => $csv_data[1], // 1 - Campaign status
                            'CAMPAIGN'                  => $csv_data[2], // 2 - Campaign
                            'BUDGET'                    => $csv_data[3], // 3 - Budget
                            'BUDGET_TYPE'               => $csv_data[4], // 4 - Currency
                            'DELIVERY'                  => $csv_data[5], // 5 - Ad group status
                            'BID_STRATEGY_TYPE'         => $csv_data[6], // 6 - Campaign bid strategy type
                            'LABELS'                    => $csv_data[7], // 7 - Labels on Campaign
                            'CLICKS'                    => $csv_data[9], // 9 - Clicks
                            'IMPR'                      => str_replace(",","", $csv_data[10]), // 10 - Impressions
                            'CTR'                       => $csv_data[11], // 11 - CTR
                            'AVG_CPC'                   => $csv_data[12], // 12 - Avg. CPC
                            'SPEND'                     => $csv_data[13], // 13 - Cost
                            'AVG_POS'                   => $csv_data[14], // 14 - Avg. CPM
                            'REVENUE'                   => $csv_data[16], // 16 - All conv. value
                            'CONV'                      => $csv_data[15], // 15 - Conversions
                            'EXTRACTION_DATE'           => $extraction_date,
                            'USER'                      => Auth::user()->campaign_user_id,
                            'SOURCE'                    => $source
                        ]);

                        if($campaign) {
                            $created++;
                        }
                    }
                }

            } else {

                /* ----------------------------------------- HANDLING OF BING ----------------------------------------- */
                
                if($start == 0) {
                    $date_explode = explode(" ", $csv_data[1]);
                    $extraction_year = $date_explode[1];
                    $extraction_month = date("m", strtotime($date_explode[3]));
                    $extraction_day = $date_explode[4];
                    $extraction_date = $extraction_year."-".$extraction_month."-".$extraction_day;
                }

                if($start > 3) {
                    
                    if(!empty($csv_data[0])) {
                        if($csv_data[1] != "-" && $csv_data[2] != "-" && $csv_data[3] != "-" && $csv_data[4] != "-" && $csv_data[5] != "-" && $csv_data[6] != "-" && $csv_data[7] != "-") {
                            $campaign = Campaign::create([
                                'CAMPAIGN_ID'               => substr($csv_data[0], 1, -1),
                                'STATUS'                    => $csv_data[1],
                                'CAMPAIGN'                  => $csv_data[2],
                                'BUDGET'                    => $csv_data[3],
                                'BUDGET_TYPE'               => $csv_data[4],
                                'DELIVERY'                  => $csv_data[5],
                                'BID_STRATEGY_TYPE'         => $csv_data[6],
                                'LABELS'                    => $csv_data[7],
                                'CLICKS'                    => $csv_data[8],
                                'IMPR'                      => str_replace(",", "", $csv_data[9]),
                                'CTR'                       => $csv_data[10],
                                'AVG_CPC'                   => $csv_data[11],
                                'SPEND'                     => $csv_data[12],
                                'CONV'                      => $csv_data[13],
                                'AVG_POS'                   => $csv_data[14],
                                'REVENUE'                   => $csv_data[15],
                                'EXTRACTION_DATE'           => $extraction_date,
                                'USER'                      => Auth::user()->campaign_user_id,
                                'SOURCE'                    => $source
                            ]);

                            if($campaign) {
                                $total_spend += $csv_data[12];
                                $created++;
                            }
                        } else {
                            if($csv_data[0] == "Overall total") {
                                $overall_spend = $csv_data[12];
                            }
                        }
                    }
                }

                /* ----------------------------------------- HANDLING OF BING ----------------------------------------- */
            }

            $start++;
        }

        if(!$isGoogle) {
            if($total_spend < $overall_spend) {
                $campaign = Campaign::create([
                    'CAMPAIGN_ID'               => "UNKNOWN",
                    'STATUS'                    => "UNKNOWN",
                    'CAMPAIGN'                  => "UNKNOWN",
                    'BUDGET'                    => "UNKNOWN",
                    'BUDGET_TYPE'               => "UNKNOWN",
                    'DELIVERY'                  => "UNKNOWN",
                    'BID_STRATEGY_TYPE'         => "UNKNOWN",
                    'LABELS'                    => "UNKNOWN",
                    'CLICKS'                    => "UNKNOWN",
                    'IMPR'                      => "UNKNOWN",
                    'CTR'                       => "UNKNOWN",
                    'AVG_CPC'                   => "UNKNOWN",
                    'SPEND'                     => $overall_spend - $total_spend,
                    'CONV'                      => "UNKNOWN",
                    'AVG_POS'                   => "UNKNOWN",
                    'REVENUE'                   => "UNKNOWN",
                    'EXTRACTION_DATE'           => $extraction_date,
                    'USER'                      => Auth::user()->campaign_user_id,
                    'SOURCE'                    => $source
                ]);
            }
        }

        fclose($file_handle);
        echo $created . " records has been created";
        exit();
    }

    /* --------------------------------------------------------------------------- FUNCTION SEPARATOR --------------------------------------------------------------------------- */

    public function bingcampaign(Request $request) {

        $DownloadPath = "/var/www/html/";

        // Confirm that the download folder exist; otherwise, exit.

        $length = strrpos($DownloadPath, '\\');
        $folder = $DownloadPath;

        // echo $folder;
        // exit();

        if (!is_dir($folder)) {
            printf("The output folder, %s, does not exist.\nEnsure that the " .
                "folder exists and try again.", $folder);
            return;
        }

        try {
            // Authenticate user credentials and set the account ID for the sample.  
            \Microsoft\BingAds\Samples\V13\AuthHelper::Authenticate();

            // You can submit one of the example reports, or build your own.
            $report = GetAccountPerformanceReportRequest(
                $GLOBALS['AuthorizationData']->AccountId
            );
                
            // SubmitGenerateReport helper method calls the corresponding Bing Ads service operation
            // to request the report identifier. The identifier is used to check report generation status
            // before downloading the report.
            
            print("-----\r\nSubmitGenerateReport:\r\n");
            $reportRequestId = ReportingExampleHelper::SubmitGenerateReport(
                $report
            )->ReportRequestId;

            printf("Report Request ID: %s\r\n", $reportRequestId);
            
            $waitTime = 10 * 1; 
            $requestStatus = null;
            $resultFileUrl = null;
            
            // This sample polls every 10 seconds up to 5 minutes.
            // In production you may poll the status every 1 to 2 minutes for up to one hour.
            // If the call succeeds, stop polling. If the call or 
            // download fails, the call throws a fault.
            
            for ($i = 0; $i < 30; $i++) {
                printf("-----\r\nsleep(%s seconds)\r\n", $waitTime);
                sleep($waitTime);
                
                // Get the download request status.
                print("-----\r\nPollGenerateReport:\r\n");
                $pollGenerateReportResponse = ReportingExampleHelper::PollGenerateReport(
                    $reportRequestId
                );

                $requestStatus = $pollGenerateReportResponse->ReportRequestStatus->Status;
                $resultFileUrl = $pollGenerateReportResponse->ReportRequestStatus->ReportDownloadUrl;
                printf("RequestStatus: %s\r\n", $requestStatus);
                printf("ReportDownloadUrl: %s\r\n", $resultFileUrl);
            
                if ($requestStatus == ReportRequestStatusType::Success || $requestStatus == ReportRequestStatusType::Error) {
                    break;
                }
            }

            if ($requestStatus != null) {
                if ($requestStatus == ReportRequestStatusType::Success) {            
                    if($resultFileUrl == null) {
                        print "No report data for the submitted request.\r\n";
                    } else {
                        printf("-----\r\nDownloading from %s.\r\n", $resultFileUrl);
                        $this->DownloadFile($resultFileUrl, $DownloadPath);
                        printf("The report was written to %s.\r\n", $DownloadPath);
                    }
                    
                } else if ($requestStatus == ReportRequestStatusType::Error) {
                    printf("The request failed. Try requesting the report later.\r\n" .
                        "If the request continues to fail, contact support.\r\n"
                    );
                } else { // Pending 
                    printf("The request is taking longer than expected.\r\n " .
                        "Save the report ID (%s) and try again later.\r\n",
                        $reportRequestId
                    );
                }
            }
        }

        catch (SoapFault $e) {
            printf("-----\r\nFault Code: %s\r\nFault String: %s\r\nFault Detail: \r\n", $e->faultcode, $e->faultstring);
            var_dump($e->detail);
            print "-----\r\nLast SOAP request/response:\r\n";
            print $GLOBALS['Proxy']->GetWsdl() . "\r\n";
            print $GLOBALS['Proxy']->GetService()->__getLastRequest()."\r\n";
            print $GLOBALS['Proxy']->GetService()->__getLastResponse()."\r\n";
        }

        catch (Exception $e) {
            // Ignore fault exceptions that we already caught.
            if ($e->getPrevious()) { ;
            } else {
                print $e->getCode()." ".$e->getMessage()."\r\n";
                print $e->getTraceAsString()."\r\n";
            }
        }
    }

    // Using the URL that the PollGenerateReport operation returned,
    // send an HTTP request to get the report and write it to the specified
    // ZIP file.

    /* --------------------------------------------------------------------------- FUNCTION SEPARATOR --------------------------------------------------------------------------- */

    public function DownloadFile($resultFileUrl, $downloadPath) {
        if (!$reader = fopen($resultFileUrl, 'rb')) {
            throw new Exception("Failed to open URL " . $resultFileUrl . ".");
        }

        if (!$writer = fopen($downloadPath, 'wb')) {
            fclose($reader);
            throw new Exception("Failed to create ZIP file " . $downloadPath . ".");
        }

        $bufferSize = 100 * 1024;

        while (!feof($reader)) {
            if (false === ($buffer = fread($reader, $bufferSize))) {
                 fclose($reader);
                 fclose($writer);
                 throw new Exception("Read operation from URL failed.");
            }

            if (fwrite($writer, $buffer) === false) {
                 fclose($reader);
                 fclose($writer);
                 $exception = new Exception("Write operation to ZIP file failed.");
            }
        }

        fclose($reader);
        fflush($writer);
        fclose($writer);
    }

    /* --------------------------------------------------------------------------- FUNCTION SEPARATOR --------------------------------------------------------------------------- */

    public function GetKeywordPerformanceReportRequest($accountId) {
        $report = new KeywordPerformanceReportRequest();
        
        $report->Format = ReportFormat::Tsv;
        $report->ReportName = 'My Keyword Performance Report';
        $report->ReturnOnlyCompleteData = false;
        $report->Aggregation = ReportAggregation::Weekly;
        
        $report->Scope = new AccountThroughAdGroupReportScope();
        $report->Scope->AccountIds = array();
        $report->Scope->AccountIds[] = $accountId;
        $report->Scope->AdGroups = null;
        $report->Scope->Campaigns = null;
        
        $report->Time = new ReportTime();
        $report->Time->PredefinedTime = ReportTimePeriod::Yesterday;
                
        $report->Columns = array (
                KeywordPerformanceReportColumn::TimePeriod,
                KeywordPerformanceReportColumn::AccountId,
                KeywordPerformanceReportColumn::CampaignId,
                KeywordPerformanceReportColumn::Keyword,
                KeywordPerformanceReportColumn::KeywordId,
                KeywordPerformanceReportColumn::DeviceType,
                KeywordPerformanceReportColumn::BidMatchType,
                KeywordPerformanceReportColumn::Clicks,
                KeywordPerformanceReportColumn::Impressions,
                KeywordPerformanceReportColumn::Ctr,
                KeywordPerformanceReportColumn::AverageCpc,
                KeywordPerformanceReportColumn::Spend,
                KeywordPerformanceReportColumn::QualityScore
        );

        $encodedReport = new SoapVar(
            $report, 
            SOAP_ENC_OBJECT, 
            'KeywordPerformanceReportRequest', 
            $GLOBALS['ReportingProxy']->GetNamespace()
        );
        
        return $encodedReport;
    }

    /* --------------------------------------------------------------------------- FUNCTION SEPARATOR --------------------------------------------------------------------------- */

    public function GetAccountPerformanceReportRequest($accountId) {
        $report = new AccountPerformanceReportRequest();
        
        $report->Format = ReportFormat::Tsv;
        $report->ReportName = 'My Account Performance Report';
        $report->ReturnOnlyCompleteData = false;
        $report->Aggregation = ReportAggregation::Weekly;
        
        $report->Scope = new AccountReportScope();
        $report->Scope->AccountIds = array();
        $report->Scope->AccountIds[] = $accountId;
            
        $report->Time = new ReportTime();
        $report->Time->PredefinedTime = ReportTimePeriod::Yesterday;

        $report->Columns = array (
                AccountPerformanceReportColumn::TimePeriod,
                AccountPerformanceReportColumn::AccountId,
                AccountPerformanceReportColumn::AccountName,
                AccountPerformanceReportColumn::Clicks,
                AccountPerformanceReportColumn::Impressions,
                AccountPerformanceReportColumn::Ctr,
                AccountPerformanceReportColumn::AverageCpc,
                AccountPerformanceReportColumn::Spend,
                AccountPerformanceReportColumn::DeviceOS
        );

        $encodedReport = new SoapVar(
            $report, 
            SOAP_ENC_OBJECT, 
            'AccountPerformanceReportRequest', 
            $GLOBALS['ReportingProxy']->GetNamespace()
        );
        
        return $encodedReport;
    }

    /* --------------------------------------------------------------------------- FUNCTION SEPARATOR --------------------------------------------------------------------------- */

    public function GetAudiencePerformanceReportRequest($accountId) {
        $report = new AudiencePerformanceReportRequest();
        
        $report->Format = ReportFormat::Tsv;
        $report->ReportName = 'My Audience Performance Report';
        $report->ReturnOnlyCompleteData = false;
        $report->Aggregation = ReportAggregation::Daily;
        
        $report->Scope = new AccountThroughAdGroupReportScope();
        $report->Scope->AccountIds = array();
        $report->Scope->AccountIds[] = $accountId;
        $report->Scope->AdGroups = null;
        $report->Scope->Campaigns = null;
        
        $report->Time = new ReportTime();
        $report->Time->PredefinedTime = ReportTimePeriod::Yesterday;
        
        $report->Columns = array (
                AudiencePerformanceReportColumn::TimePeriod,
                AudiencePerformanceReportColumn::AccountId,
                AudiencePerformanceReportColumn::CampaignId,
                AudiencePerformanceReportColumn::AudienceId,
                AudiencePerformanceReportColumn::Clicks,
                AudiencePerformanceReportColumn::Impressions,
                AudiencePerformanceReportColumn::Ctr,
                AudiencePerformanceReportColumn::AverageCpc,
                AudiencePerformanceReportColumn::Spend,
        );
        
        $encodedReport = new SoapVar(
            $report, 
            SOAP_ENC_OBJECT, 
            'AudiencePerformanceReportRequest', 
            $GLOBALS['ReportingProxy']->GetNamespace()
        );
        
        return $encodedReport;
    }
}