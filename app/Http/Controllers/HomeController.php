<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Conversion;
use App\Models\ClickCounter;
use App\Models\Channel;
use App\Models\ChannelTracer;

use App\Models\Keyword;
use App\Models\Campaign;
use App\Models\RevenueReport;

use DB;

class HomeController extends Controller {
    
    public function __construct() {
        $this->middleware('auth');
    }

    /* -------------------------------------------------------------- FUNCTION SEPARATOR -------------------------------------------------------------- */

    public function index() {
        $date = date('Y-m-d',(strtotime ( '-208 hours' , time() ) ));
        $real_time_data = DB::select("
                SELECT CONVERSIONS.CHANNEL, CONVERSIONS.SOURCE as conversion_source, CONVERSIONS.DATE, CONVERSIONS.DATE, COUNT(CONVERSIONS.ID) as clickout, CONVERSIONS.KEYWORD, CONVERSIONS.SUBID2, CONVERSIONS.SUBID3, CONVERSIONS.CAMPAIGN, CONVERSIONS.SEARCHED_KEYWORD,

                CLICK_COUNTER_TEST.ID as id,
                CLICK_COUNTER_TEST.CLICKS as visits,
                CLICK_COUNTER_TEST.SOURCE as source,
                CLICK_COUNTER_TEST.TODAY as click_date,

                CHANNELS_TEST.COUNTRY as channel_country,
                CHANNELS_TEST.MATCH_TYPE

                FROM CONVERSIONS
                LEFT JOIN CLICK_COUNTER_TEST on CONVERSIONS.DATE = CLICK_COUNTER_TEST.TODAY AND CONVERSIONS.CHANNEL = CLICK_COUNTER_TEST.CHANNEL
                LEFT JOIN CHANNELS_TEST on CONVERSIONS.CHANNEL = CHANNELS_TEST.CHANNEL

                WHERE CONVERSIONS.DATE >= '$date' AND CHANNELS_TEST.MATCH_TYPE <> '' AND CLICK_COUNTER_TEST.CLICKS >= 10

                group by CONVERSIONS.DATE, CONVERSIONS.KEYWORD, CONVERSIONS.SUBID2, CONVERSIONS.SUBID3, CONVERSIONS.CAMPAIGN                 
            ");
        
        return view('home', ["real_time_data"=>$real_time_data]);
    }

    /* -------------------------------------------------------------- FUNCTION SEPARATOR -------------------------------------------------------------- */

    public function keyword_out() {
        $date = date('Y-m-d',(strtotime ( '-8 hours' , time() ) ));
        $start_date = $date;
        $end_date = $date;

        $real_time_data = DB::select("
                SELECT CONVERSIONS.CHANNEL, CONVERSIONS.DATE, CONVERSIONS.DATE, COUNT(CONVERSIONS.ID) as clickout, CONVERSIONS.KEYWORD, CONVERSIONS.SUBID2, CONVERSIONS.SUBID3, CONVERSIONS.CAMPAIGN, CONVERSIONS.SEARCHED_KEYWORD,

                CLICK_COUNTER_TEST.CLICKS as visits,
                CLICK_COUNTER_TEST.SOURCE as source,
                CLICK_COUNTER_TEST.TODAY as click_date,

                CHANNELS_TEST.COUNTRY as channel_country,
                CHANNELS_TEST.MATCH_TYPE

                FROM CONVERSIONS
                LEFT JOIN CLICK_COUNTER_TEST on CONVERSIONS.DATE = CLICK_COUNTER_TEST.TODAY AND CONVERSIONS.CHANNEL = CLICK_COUNTER_TEST.CHANNEL
                LEFT JOIN CHANNELS_TEST on CONVERSIONS.CHANNEL = CHANNELS_TEST.CHANNEL

                WHERE CONVERSIONS.DATE >= '$date' AND CHANNELS_TEST.MATCH_TYPE <> '' AND CLICK_COUNTER_TEST.CLICKS >= 10

                group by CONVERSIONS.DATE, CONVERSIONS.KEYWORD, CONVERSIONS.SUBID2, CONVERSIONS.SUBID3, CONVERSIONS.CAMPAIGN                 
            ");


        /* ############################################################## REVENUE PROCESSING ############################################################## */

        $revenues = RevenueReport::where("DATE",">=", $start_date)->where("DATE","<=", $end_date)->orderBy("DATE", "ASC")->get();

        $raw_total_revenues = 0;
        $total_channel_revenues = 0;

        $revenue_channel_data = array();
        $revenue_raw = array();
        $revenue_with_channel_data_key = array();
        foreach($revenues as $revenue) {
            $revenue_with_channel_data_key[$revenue->CUSTOM_CHANNEL_NAME."-".$revenue->DATE] = $revenue;

            // $revenue_channel_data[$revenue['CUSTOM_CHANNEL_NAME']] = $revenue['ID'];
            $revenue_raw[] = $revenue['ID'];

            $raw_total_revenues += $revenue['EARNINGS']; 
        }

        /* ############################################################## REVENUE PROCESSING ############################################################## */



        $consolidated_channels = array();
        $fields_to_select = array("ID", "CHANNEL", "KEYWORD", "CAMPAIGN", "DATE", "KEYWORD", "MATCH_TYPE", "KEYWORD_IN");

        /* ########################################### CHANNEL TABLE CONTAINS CURRENT DATE RECORDS AND CHANNEL TRACER CONTAINS HISTORIC RECORDS ###########################################  */

        if($end_date == date("Y-m-d")) {
            $fields_for_current_channel = $fields_to_select;
            unset($fields_for_current_channel[4]);
            unset($fields_for_current_channel[5]);

            $channel = Channel::select($fields_for_current_channel)->where("KEYWORD","<>","")->get();
            $consolidated_channels = $channel->toArray();
        }

        /* ########################################### CHANNEL TABLE CONTAINS CURRENT DATE RECORDS AND CHANNEL TRACER CONTAINS HISTORIC RECORDS ###########################################  */
        
        $historic_channel = ChannelTracer::select($fields_to_select)->where("DATE",">=", $start_date)->where("DATE","<=", $end_date)->where("KEYWORD","<>","")->orderBy("DATE", "ASC")->get();
        $channel_tracer = $historic_channel->toArray();

        $consolidated_channels = array_merge($consolidated_channels, $channel_tracer);

        /* ######################################## USING RELATIVE DATA TO MAKE IT ARRAY INDEX FOR COLLATED COMPUTATION ######################################## */

        $processed_channel_tracer = array();
        $processed_channel_tracer_by_id = array();
        $revenue_used_in_channel = array();
        $channel_ids = array();

        foreach($consolidated_channels as $consolidated_channel) {

            $match_type = "";
            $channel_ids[] = $consolidated_channel['ID'];

            $assoc_revenue = null;
            
            if(!isset($consolidated_channel['DATE'])) {
                $consolidated_channel['DATE'] = date("Y-m-d");
            }

            if(isset($revenue_with_channel_data_key[$consolidated_channel['CHANNEL']."-".$consolidated_channel['DATE']])) {
                $assoc_revenue = $revenue_with_channel_data_key[$consolidated_channel['CHANNEL']."-".$consolidated_channel['DATE']];
                
                if(isset($assoc_revenue['ID'])) {
                    $revenue_used_in_channel[] = $assoc_revenue['ID'];
                }

                $total_channel_revenues += $assoc_revenue->EARNINGS;
            }

            $match_type = $consolidated_channel['MATCH_TYPE'];

            $channel_tracer_content_data = array(
                'id'            => $consolidated_channel['ID'],
                'campaign'      => $consolidated_channel['CAMPAIGN'],
                'date'          => $consolidated_channel['DATE'],
                'match_type'    => $match_type,
                'keyword'       => $consolidated_channel['KEYWORD'],
                'channel'       => $consolidated_channel['CHANNEL'],
                'keyword_in'    => $consolidated_channel['KEYWORD_IN'],
                'revenue'       => $assoc_revenue
            );

            $processed_channel_tracer[$consolidated_channel['CAMPAIGN']."-".$consolidated_channel['KEYWORD_IN']."-".$consolidated_channel['DATE']."-".$match_type] = $channel_tracer_content_data;
        }

        return view('keyword_out', ["real_time_data"=>$real_time_data, "processed_channel_tracer" => $processed_channel_tracer]);
    }

    /* -------------------------------------------------------------- FUNCTION SEPARATOR -------------------------------------------------------------- */

    public function keyword_download(Request $request) {
        if($request->input('start') && $request->input('end')) {
            $start_date = $request->input('start');
            $end_date = $request->input('end');
        }

        $consolidated_channels = array();
        $fields_to_select = array("ID", "CHANNEL", "KEYWORD", "CAMPAIGN", "DATE", "KEYWORD", "MATCH_TYPE", "KEYWORD_IN");

        $db_query_string = "SELECT * FROM KEYWORD_COST where EXTRACTION_DATE >= '$start_date' AND EXTRACTION_DATE <= '$end_date' AND CAMPAIGN != '-'";
        // $keywords = Keyword::where("CLICKS", "<>", "0")->where("IMPR", "<>", "0")->where("EXTRACTION_DATE",">=", $start_date)->where("EXTRACTION_DATE","<=", $end_date)->where("CAMPAIGN","<>", "-")->orderBy("EXTRACTION_DATE", "DESC")->get();
        $keywords = DB::select($db_query_string);

        /* ########################################### CHANNEL TABLE CONTAINS CURRENT DATE RECORDS AND CHANNEL TRACER CONTAINS HISTORIC RECORDS ###########################################  */

        if($end_date == date("Y-m-d")) {
            $fields_for_current_channel = $fields_to_select;
            unset($fields_for_current_channel[4]);
            unset($fields_for_current_channel[5]);

            $channel = Channel::select($fields_for_current_channel)->where("KEYWORD","<>","")->get();
            $consolidated_channels = $channel->toArray();
        }

        /* ########################################### CHANNEL TABLE CONTAINS CURRENT DATE RECORDS AND CHANNEL TRACER CONTAINS HISTORIC RECORDS ###########################################  */
        
        $historic_channel = ChannelTracer::select($fields_to_select)->where("DATE",">=", $start_date)->where("DATE","<=", $end_date)->where("KEYWORD","<>","")->orderBy("DATE", "ASC")->get();
        $channel_tracer = $historic_channel->toArray();

        $consolidated_channels = array_merge($consolidated_channels, $channel_tracer);

        /* ######################################## USING RELATIVE DATA TO MAKE IT ARRAY INDEX FOR COLLATED COMPUTATION ######################################## */



        $processed_channel_tracer = array();
        $processed_channel_tracer_by_id = array();
        $revenue_used_in_channel = array();
        $channel_ids = array();

        foreach($consolidated_channels as $consolidated_channel) {

            $match_type = "";
            $channel_ids[] = $consolidated_channel['ID'];

            $assoc_revenue = null;
            
            if(!isset($consolidated_channel['DATE'])) {
                $consolidated_channel['DATE'] = date("Y-m-d");
            }

            if(isset($revenue_with_channel_data_key[$consolidated_channel['CHANNEL']."-".$consolidated_channel['DATE']])) {
                $assoc_revenue = $revenue_with_channel_data_key[$consolidated_channel['CHANNEL']."-".$consolidated_channel['DATE']];
                
                if(isset($assoc_revenue['ID'])) {
                    $revenue_used_in_channel[] = $assoc_revenue['ID'];
                }

                $total_channel_revenues += $assoc_revenue->EARNINGS;
            }

            $match_type = "";
            switch ($consolidated_channel['MATCH_TYPE']) {
                case 'be':
                    $match_type = "Exact";
                    break;

                case 'bp':
                    $match_type = "Phrase";
                    break;
                
                default:
                    $match_type = "Broad";
                    break;
            }

            $channel_tracer_content_data = array(
                'id'            => $consolidated_channel['ID'],
                'campaign'      => $consolidated_channel['CAMPAIGN'],
                'date'          => $consolidated_channel['DATE'],
                'match_type'    => $match_type,
                'keyword'       => $consolidated_channel['KEYWORD'],
                'channel'       => $consolidated_channel['CHANNEL'],
                'keyword_in'    => $consolidated_channel['KEYWORD_IN'],
                'revenue'       => $assoc_revenue
            );

            $processed_channel_tracer[$consolidated_channel['CAMPAIGN']."-".$consolidated_channel['KEYWORD_IN']."-".$consolidated_channel['DATE']."-".$match_type] = $channel_tracer_content_data;
            $processed_channel_tracer_by_id[$consolidated_channel['ID']] = $channel_tracer_content_data;
        }


        // return $keywords;
        $revenues_used_by_keyword = array();
        $channels_used_in_keyword = array();
        $total_keyword_revenues = 0;

        $channels_used_frequency = array();
        $keyword_with_revenue_but_no_cost = array();

        $to_loop_keywords = array();

        // eliminating all keywords with zero cost and no revenue
        foreach($keywords as $keyword) {
            if(isset($processed_channel_tracer[$keyword->CAMPAIGN."-".$keyword->KEYWORD."-".$keyword->EXTRACTION_DATE."-".$keyword->MATCH_TYPE])) {
                
                $channel_instace = $processed_channel_tracer[$keyword->CAMPAIGN."-".$keyword->KEYWORD."-".$keyword->EXTRACTION_DATE."-".$keyword->MATCH_TYPE];
                $channels_used_in_keyword[] = $channel_instace['id'];

                if(isset($channels_used_frequency[$channel_instace['id']])) {
                    $channels_used_frequency[$channel_instace['id']] += 1;
                } else {
                    $channels_used_frequency[$channel_instace['id']] = 1;
                }

                if(isset($processed_channel_tracer[$keyword->CAMPAIGN."-".$keyword->KEYWORD."-".$keyword->EXTRACTION_DATE."-".$keyword->MATCH_TYPE]['revenue'])) {
                    $total_keyword_revenues += $processed_channel_tracer[$keyword->CAMPAIGN."-".$keyword->KEYWORD."-".$keyword->EXTRACTION_DATE."-".$keyword->MATCH_TYPE]['revenue']->EARNINGS;

                    if($keyword->SPEND == 0 && (int)$processed_channel_tracer[$keyword->CAMPAIGN."-".$keyword->KEYWORD."-".$keyword->EXTRACTION_DATE."-".$keyword->MATCH_TYPE]['revenue']->EARNINGS > 0) {
                        $keyword_with_revenue_but_no_cost[] = array(
                            "keyword" => $keyword,
                            "channel_with_revenue" => $processed_channel_tracer[$keyword->CAMPAIGN."-".$keyword->KEYWORD."-".$keyword->EXTRACTION_DATE."-".$keyword->MATCH_TYPE],
                        );
                    }

                    $to_loop_keywords[] = $keyword;

                } else {
                    if($keyword->SPEND > 0) {
                        $to_loop_keywords[] = $keyword;
                    }
                }
            } else {
                if($keyword->SPEND > 0) {
                    $to_loop_keywords[] = $keyword;
                }
            }
        }

        $csv_string = "ID,Status,Keyword,Campaign,Ad Group,Match Type,Bid Strategy Type,Delivery,Bid,Labels,Clicks,Impr,Ctr,Avg Cpc,Spend,Avg Pos,Date\n";
        foreach($to_loop_keywords as $keyword) {
            $csv_string .= "\"$keyword->id\"," . "\"$keyword->STATUS\"," . "\"$keyword->KEYWORD\"," . "\"$keyword->CAMPAIGN\"," . "\"$keyword->AD_GROUP\"," . "\"$keyword->MATCH_TYPE\"," . "\"$keyword->BID_STRATEGY_TYPE\"," . "\"$keyword->DELIVERY\"," . "\"$keyword->BID\"," . "\"str_replace(',', '|', $keyword->LABELS)\"," . "\"$keyword->CLICKS\"," . "\"$keyword->IMPR\"," . "\"$keyword->CTR\"," . "\"$keyword->AVG_CPC\"," . "\"$keyword->SPEND\"," . "\"$keyword->AVG_POS\"," . "\"$keyword->EXTRACTION_DATE\"," . "\n";
        }

        $date = time();
        $FileName = "keyword-report-$date.csv";

        header("Content-type: text/csv");
        header('Content-Disposition: attachment; filename="' . $FileName . '"'); 
        echo $csv_string;
        exit();

    }

    /* -------------------------------------------------------------- FUNCTION SEPARATOR -------------------------------------------------------------- */

    public function campaign_download(Request $request) {
        if($request->input('start') && $request->input('end')) {
            $start_date = $request->input('start');
            $end_date = $request->input('end');
        }
        
        $csv_string = "ID,Status,Campaign,Budget,Budget Type,Delivery,Bid Stratedy Type,Labels,Clicks,IMPR,CTR,Avg CPC,Spend,Conv,Avg Pos,Revenue,Date\n";
        $campaigns = Campaign::where("EXTRACTION_DATE",">=", $start_date)->where("EXTRACTION_DATE","<=", $end_date)->orderBy("EXTRACTION_DATE", "ASC")->get();
        foreach($campaigns as $campaign) {
            $campaign_labels = str_replace(";", "|", $campaign->LABELS);
            $csv_string .= "\"$campaign->id\"," . "\"$campaign->STATUS\"," . "\"$campaign->CAMPAIGN\"," . "\"$campaign->BUDGET\"," . "\"$campaign->BUDGET_TYPE\"," . "\"$campaign->DELIVERY\"," . "\"$campaign->BID_STRATEGY_TYPE\"," . "\"$campaign_labels\"," . "\"$campaign->CLICKS\"," . "\"$campaign->IMPR\"," . "\"$campaign->CTR\"," . "\"$campaign->AVG_CPC\"," . "\"$campaign->SPEND\"," . "\"$campaign->CONV\",". "\"$campaign->AVG_POS\"," . "\"$campaign->REVENUE\"," . "\"$campaign->EXTRACTION_DATE\""  . "\n";
        }

        $date = time();
        $FileName = "campaign-report-$date.csv";

        header("Content-type: text/csv");
        header('Content-Disposition: attachment; filename="' . $FileName . '"'); 
        echo $csv_string;
        exit();
    }

    /* -------------------------------------------------------------- FUNCTION SEPARATOR -------------------------------------------------------------- */

    public function revenue_download(Request $request) {
        if($request->input('start') && $request->input('end')) {
            $start_date = $request->input('start');
            $end_date = $request->input('end');
        }
        
        $csv_string = "ID,Country,Date,Ad Client Id,Platform Type Name,Custom Channel Name,Ad Requests,Clicks,Earnings,Page Views,Individual Ad Impressions,Page Views RPM,Individual Ad Impressions RPM,Matched Ad Requests,Updated\n";
        
        $revenues = RevenueReport::where("DATE",">=", $start_date)->where("DATE","<=", $end_date)->orderBy("DATE", "ASC")->get();
        foreach($revenues as $revenue) {
            $csv_string .= "\"$revenue->ID\"," . "\"$revenue->COUNTRY\"," . "\"$revenue->DATE\"," . "\"$revenue->AD_CLIENT_ID\"," . "\"$revenue->PLATFORM_TYPE_NAME\"," . "\"$revenue->CUSTOM_CHANNEL_NAME\"," . "\"$revenue->AD_REQUESTS\"," . "\"$revenue->CLICKS\"," . "\"$revenue->EARNINGS\"," . "\"$revenue->PAGE_VIEWS\"," . "\"$revenue->INDIVIDUAL_AD_IMPRESSIONS\"," . "\"$revenue->PAGE_VIEWS_RPM\"," . "\"$revenue->INDIVIDUAL_AD_IMPRESSIONS_RPM\"," . "\"$revenue->MATCHED_AD_REQUESTS\"," . "\"$revenue->UPDATED\"" . "\n";
        }

        $date = time();
        $FileName = "revenue-report-$date.csv";

        header("Content-type: text/csv");
        header('Content-Disposition: attachment; filename="' . $FileName . '"'); 
        echo $csv_string;
        exit();
    }

    /* -------------------------------------------------------------- FUNCTION SEPARATOR -------------------------------------------------------------- */

    public function conversion_download(Request $request) {
        if($request->input('start') && $request->input('end')) {
            $start_date = $request->input('start');
            $end_date = $request->input('end');
        }
        
        $csv_string = "ID,Time,Keyword,Sub_ID_1,Sub_ID_2,Sub_ID_3,Campaign,Source,Searched Keyword,Bidmatch,Channel,User,Date\n";
        $conversions = Conversion::where("DATE",">=", $start_date)->where("DATE","<=", $end_date)->orderBy("DATE", "ASC")->get();
        foreach($conversions as $conversion) {
            $csv_string .= "\"$conversion->ID\"," . "\"$conversion->TIME\"," . "\"$conversion->KEYWORD\"," . "\"$conversion->SUBID1\"," . "\"$conversion->SUBID2\"," . "\"$conversion->SUBID3\"," . "\"$conversion->CAMPAIGN\"," . "\"$conversion->SOURCE\"," . "\"$conversion->SEARCHED_KEYWORD\"," . "\"$conversion->BIDMATCH\"," . "\"$conversion->CHANNEL\"," . "\"$conversion->USER\"," . "\"$conversion->DATE\"," . "\n";
        }

        $date = time();
        $FileName = "conversion-report-$date.csv";

        header("Content-type: text/csv");
        header('Content-Disposition: attachment; filename="' . $FileName . '"'); 
        echo $csv_string;
        exit();
    }

    /* -------------------------------------------------------------- FUNCTION SEPARATOR -------------------------------------------------------------- */

    public function csv_exporter(Request $request) {
        $export_name = $request->input("export_name");

        header("Content-type: application/octet-stream");
        header("Content-Disposition: attachment; filename=\"$export_name\"");

        $csv_string = $request->input("csv_string");
        $exploded_csv = explode("\\n", $csv_string);

        echo implode("\n", $exploded_csv);
    }
}
