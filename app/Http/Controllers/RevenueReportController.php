<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Campaign;
use App\Models\RevenueReport;
use App\Models\Channel;
use App\Models\ChannelTracer;
use App\Models\Keyword;
use DB;

class RevenueReportController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        
        $data = array();
        $source = "bing";

        if($request->input('source')) {
            $source = $request->input('source');
        }
        
        $start_date = date('Y-m-d',(strtotime ( '-2 day' , time() ) ));
        $end_date = date("Y-m-d");

        if($request->input('start') && $request->input('end')) {
            $start_date = $request->input('start');
            $end_date = $request->input('end');
        }
        
        
        $keywords = "SELECT EXTRACTION_DATE, SPEND as spend, AVG_CPC as avg_cpc, CLICKS as clicks FROM KEYWORD_COST WHERE SOURCE = '$source' AND EXTRACTION_DATE >= '$start_date' AND EXTRACTION_DATE <= '$end_date'";
        $campaigns = "SELECT EXTRACTION_DATE, SUM(CLICKS) as clicks, SUM(SPEND) as spend, AVG_CPC as avg_cpc FROM CAMPAIGN_COST WHERE SOURCE = '$source' AND EXTRACTION_DATE >= '$start_date' AND EXTRACTION_DATE <= '$end_date' GROUP BY EXTRACTION_DATE order by EXTRACTION_DATE desc";

        if($source == "bing") {
            $revenues = "SELECT DATE, SUM(CLICKS) as clicks, SUM(EARNINGS) as earnings FROM REVENUE_REPORT WHERE CUSTOM_CHANNEL_NAME <= 'T0000899' AND DATE >= '$start_date' AND DATE <= '$end_date' GROUP BY DATE order by DATE desc";
        } else {
            $revenues = "SELECT DATE, SUM(CLICKS) as clicks, SUM(EARNINGS) as earnings FROM REVENUE_REPORT WHERE CUSTOM_CHANNEL_NAME > 'T0000899' AND DATE >= '$start_date' AND DATE <= '$end_date' GROUP BY DATE order by DATE desc";
        }

        $campaign_query = DB::select($campaigns);
        $keyword_query = DB::select($keywords);
        $revenue_query = DB::select($revenues);

        $to_loop_campaigns = array();

        foreach($campaign_query as $campaign_instance) {

            $to_loop_campaigns[$campaign_instance->EXTRACTION_DATE]['spend'] = $campaign_instance->spend;
            $to_loop_campaigns[$campaign_instance->EXTRACTION_DATE]['avg_cpc'] = $campaign_instance->avg_cpc;
            $to_loop_campaigns[$campaign_instance->EXTRACTION_DATE]['clicks'] = $campaign_instance->clicks;

        }

        $to_loop_keywords = array();

        foreach($keyword_query as $keyword_instance) {

            $to_loop_keywords[$keyword_instance->EXTRACTION_DATE]['spend'] = $keyword_instance->spend;
            $to_loop_keywords[$keyword_instance->EXTRACTION_DATE]['avg_cpc'] = $keyword_instance->avg_cpc;
            $to_loop_keywords[$keyword_instance->EXTRACTION_DATE]['clicks'] = $keyword_instance->clicks;

        }

        $to_loop_revenues = array();

        foreach($revenue_query as $revenue_instance) {

            $to_loop_revenues[$revenue_instance->DATE]['clickout'] = $revenue_instance->clicks;
            $to_loop_revenues[$revenue_instance->DATE]['earnings'] = $revenue_instance->earnings;

        }

        $data['to_loop_campaigns'] = $to_loop_campaigns;
        $data['to_loop_keywords'] = $to_loop_keywords;
        $data['to_loop_revenues'] = $to_loop_revenues;

        // Keyword
            // Cost
            // Avg CPC
        
        // Computed
            // Clickouts
            // CR
            // Avg RPC
            // GM
            // NEt ROI 
        
        //Revenue   
            // Clicks
            // REV

        return view('general_report', ["data"=>$data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\RevenueReport  $revenueReport
     * @return \Illuminate\Http\Response
     */
    public function show(RevenueReport $revenueReport) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\RevenueReport  $revenueReport
     * @return \Illuminate\Http\Response
     */
    public function edit(RevenueReport $revenueReport)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\RevenueReport  $revenueReport
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RevenueReport $revenueReport)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\RevenueReport  $revenueReport
     * @return \Illuminate\Http\Response
     */
    public function destroy(RevenueReport $revenueReport)
    {
        //
    }


    /* --------------------------------------------------------------------------- FUNCTION SEPARATOR --------------------------------------------------------------------------- */

    public function parse(RevenueReport $revenueReport) {
        $csv_path = public_path() . "/revenues.csv";

        $start = 0;
        $file_handle = fopen($csv_path, 'r');
        while (!feof($file_handle)) {
            
            $csv_data = fgetcsv($file_handle, 0, ",");

            if($start != 0) {
                $RevenueReport = RevenueReport::create([
                    "DATE"                                  => $csv_data[0],
                    "COUNTRY"                               => $csv_data[1],
                    "AD_CLIENT_ID"                          => $csv_data[2],
                    "PLATFORM_TYPE_NAME"                    => $csv_data[3],
                    "CUSTOM_CHANNEL_NAME"                   => $csv_data[4],
                    "AD_REQUESTS"                           => $csv_data[5],
                    "CLICKS"                                => $csv_data[6],
                    "EARNINGS"                              => $csv_data[7],
                    "PAGE_VIEWS"                            => $csv_data[8],
                    "INDIVIDUAL_AD_IMPRESSIONS"             => $csv_data[9],
                    "PAGE_VIEWS_RPM"                        => $csv_data[10],
                    "INDIVIDUAL_AD_IMPRESSIONS_RPM"         => $csv_data[11],
                    "MATCHED_AD_REQUESTS"                   => $csv_data[12]
                ]);
            }

            $start++;
        }

        fclose($file_handle);
        exit();
    }
}
