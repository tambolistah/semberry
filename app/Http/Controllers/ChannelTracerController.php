<?php

namespace App\Http\Controllers;

use App\Models\ChannelTracer;
use Illuminate\Http\Request;

class ChannelTracerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ChannelTracer  $channelTracer
     * @return \Illuminate\Http\Response
     */
    public function show(ChannelTracer $channelTracer) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ChannelTracer  $channelTracer
     * @return \Illuminate\Http\Response
     */
    public function edit(ChannelTracer $channelTracer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ChannelTracer  $channelTracer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ChannelTracer $channelTracer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ChannelTracer  $channelTracer
     * @return \Illuminate\Http\Response
     */
    public function destroy(ChannelTracer $channelTracer)
    {
        //
    }
}
