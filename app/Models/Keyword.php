<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Keyword extends Model
{
    use HasFactory;
    protected $table = 'KEYWORD_COST';

    public $fillable = [
		'STATUS',
		'KEYWORD',
		'CAMPAIGN',
		'AD_GROUP',
		'MATCH_TYPE',
		'BID_STRATEGY_TYPE',
		'DELIVERY',
		'BID',
		'LABELS',
		'CLICKS',
		'IMPR',
		'CTR',
		'AVG_CPC',
		'SPEND',
		'AVG_POS',
        'EXTRACTION_DATE',
        'USER',
        'SOURCE'
	];

}
