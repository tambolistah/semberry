<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ChannelTracer extends Model
{
    use HasFactory;
    protected $table = 'CHANNELS_TRACER';
}
