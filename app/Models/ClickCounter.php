<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClickCounter extends Model
{
    use HasFactory;
    protected $table = 'CLICK_COUNTER_TEST';
}
