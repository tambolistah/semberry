<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RevenueReport extends Model
{
    use HasFactory;
    protected $table = 'REVENUE_REPORT';

    public $fillable = [
    	"DATE",
        "COUNTRY",
        "AD_CLIENT_ID",
        "PLATFORM_TYPE_NAME",
        "CUSTOM_CHANNEL_NAME",
        "AD_REQUESTS",
        "CLICKS",
        "EARNINGS",
        "PAGE_VIEWS",
        "INDIVIDUAL_AD_IMPRESSIONS",
        "PAGE_VIEWS_RPM",
        "INDIVIDUAL_AD_IMPRESSIONS_RPM",
        "MATCHED_AD_REQUESTS"
    ];
}
