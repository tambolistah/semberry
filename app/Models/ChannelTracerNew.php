<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ChannelTracerNew extends Model {
    use HasFactory;
    protected $table = 'CHANNELS_TRACER_NEW';
}