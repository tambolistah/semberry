<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BidUser extends Model {
    use HasFactory;

    protected $table = 'USERS';
}
