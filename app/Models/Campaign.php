<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{
    use HasFactory;

    protected $table = 'CAMPAIGN_COST';
    public $fillable = [
		'CAMPAIGN_ID',
        'STATUS',
        'CAMPAIGN',
        'BUDGET',
        'BUDGET_TYPE',
        'DELIVERY',
        'BID_STRATEGY_TYPE',
        'LABELS',
        'CLICKS',
        'IMPR',
        'CTR',
        'AVG_CPC',
        'SPEND',
        'CONV',
        'AVG_POS',
        'REVENUE',
        'EXTRACTION_DATE',
        'USER',
        'SOURCE'
	];
}
