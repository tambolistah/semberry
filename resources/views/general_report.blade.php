@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1 class="m-0 text-dark">General Report</h1>
@stop

@section('content')
    
    <?php
        function clean($string) {
            $string = preg_replace('/\xc2\xa0/', '', $string);
            $string = urlencode(str_replace(" ", "", trim(strtolower($string))));
            return $string;
        }

        function to_ita_values($string) {
            $string = str_replace('.', ',', $string);
            return $string;
        }

        $start = date('Y-m-d',(strtotime ( '-2 day' , time() ) ));
        $end = date("Y-m-d");

        if(isset($_GET['start']) && isset($_GET['end'])) {
            $start = $_GET['start'];
            $end = $_GET['end'];           
        }

        $search = "";
        if(isset($_GET['search'])) {
            $search = $_GET['search'];
        }

        $source = "bing";
        if(isset($_GET['source'])) {
            $source = $_GET['source'];
        }
    ?>
    
    <form method="get">
        <div class="row">
            <div class="col-md-6 col-12">
                <div class="row">

                    <?php if(isset($_GET['source'])) { ?>
                        <input name="source" type="hidden" value="<?php echo $_GET['source']; ?>" />
                    <?php } ?>
                    
                    <div class="col-md-5 col-12">
                        <div class="form-group">
                            <input name="start" value="<?php echo $start; ?>" placeholder="Start Date" class="datepicker form-control" data-date-format="yyyy-mm-dd">
                        </div>
                    </div>
                    <div class="col-md-5 col-12">
                        <div class="form-group">
                            <input name="end" value="<?php echo $end; ?>" placeholder="End Date" class="datepicker form-control" data-date-format="yyyy-mm-dd">
                        </div>
                    </div>
                    <div class="col-md-2 col-12">
                        <input type="submit" class="btn btn-success" value="Filter" />
                    </div>
                </div>
            </div>
        </div>
    </form>

    <ul class="nav nav-tabs" id="myTab" role="tablist">
      <li class="nav-item">
        <a class="nav-link <?php if(!isset($_GET['source']) || $_GET['source'] == "bing") { echo "active"; } ?>" id="home-tab" data-toggle="external-link" href="?source=bing" role="tab" aria-controls="home" aria-selected="true">Bing</a>
      </li>
      <li class="nav-item">
        <a class="nav-link <?php if(isset($_GET['source']) && $_GET['source'] == "google") { echo "active"; } ?>" id="profile-tab" data-toggle="external-link" href="?source=google" role="tab" aria-controls="profile" aria-selected="false">Google</a>
      </li>
    </ul>

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <table id="myTable">              
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>Cost</th>
                                <th>Clicks</th>
                                <th>Avg CPC</th>
                                <th>Clickouts</th>
                                <th>CR</th>
                                <th>Avg RPC</th>
                                <th>REV</th>
                                <th>GM</th>
                                <th>Net ROI</th> 
                          </tr>
                        </thead>

                        <?php
                            $total_spend = 0;
                            $total_clicks = 0;
                            $total_avg_cpc = 0;
                            $total_clickout = 0;
                            $total_cr = 0;
                            $total_avg_rpc = 0;
                            $total_rev = 0;
                            $total_gm = 0;
                            $total_net_roi = 0;
                        ?>

                        <tbody>
                            <?php foreach($data['to_loop_campaigns'] as $key => $to_loop_campaigns) { ?>
                                <tr>
                                    <td>{{ $key }}</td>
                                    <td>
                                        <?php
                                            $spend = $to_loop_campaigns['spend'];
                                            $total_spend += $spend;
                                            echo $spend;
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                            $clicks = (float)$to_loop_campaigns['clicks'];
                                            $total_clicks += $clicks;
                                            echo $clicks;
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                            $avg_cpc = 0;
                                            if($clicks > 0) {
                                                $avg_cpc = round(($spend / $clicks), 2);
                                                $total_avg_cpc += $avg_cpc;
                                            }
                                            echo $avg_cpc;
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                            $clickout = 0;
                                            if(isset($data['to_loop_revenues'][$key])) {
                                                $clickout = $data['to_loop_revenues'][$key]['clickout'];
                                                $total_clickout += $clickout;
                                            }
                                            
                                            echo $clickout;
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                            $cr = 0;

                                            if($to_loop_campaigns['clicks'] > 0) {
                                                $cr = round($data['to_loop_revenues'][$key]['clickout'] / $to_loop_campaigns['clicks'], 2);
                                            }

                                            $total_cr += $cr;
                                            echo $cr;
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                            $avg_rpc = 0;

                                            if(isset($data['to_loop_revenues'][$key])) {
                                                if($data['to_loop_revenues'][$key]['clickout'] > 0) {
                                                    $avg_rpc = round(($data['to_loop_revenues'][$key]['earnings'] * env('REVENUE_MULTIPLIER')) / $data['to_loop_revenues'][$key]['clickout'] ,2);
                                                }
                                            }

                                            $total_avg_rpc += $avg_rpc;
                                            echo $avg_rpc;
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                            $rev = 0;
                                            if(isset($data['to_loop_revenues'][$key])) {
                                                $rev = round(($data['to_loop_revenues'][$key]['earnings'] * env('REVENUE_MULTIPLIER')),2);
                                            }

                                            $total_rev += $rev;
                                            echo $rev;
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                            $gm = 0;
                                            if(isset($data['to_loop_revenues'][$key])) {
                                                $gm = round(($data['to_loop_revenues'][$key]['earnings'] * env('REVENUE_MULTIPLIER')) - $to_loop_campaigns['spend'], 2);
                                            }
                                            $total_gm += $gm;
                                            echo $gm;
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                            $net_roi = 0;
                                            
                                            if($to_loop_campaigns['spend'] > 0) {
                                                $net_roi = round((($data['to_loop_revenues'][$key]['earnings'] * env('REVENUE_MULTIPLIER')) - $to_loop_campaigns['spend']) / $to_loop_campaigns['spend'], 2);
                                            }

                                            $total_net_roi += $net_roi;
                                            echo $net_roi;
                                        ?>
                                    </td>
                              </tr>
                            <?php } ?>

                        </tbody>
                    </table>

                    <hr />

                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>Cost</th>
                                <th>Clicks</th>
                                <th>Avg CPC</th>
                                <th>Clickouts</th>
                                <th>CR</th>
                                <th>Avg RPC</th>
                                <th>REV</th>
                                <th>GM</th>
                                <th>Net ROI</th> 
                          </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th>Total</th>
                                <th>{{ $total_spend }}</th>
                                <th>{{ $total_clicks }}</th>
                                <th>
                                    <?php
                                        if($total_clicks > 0) {
                                            echo round(($total_spend / $total_clicks), 2);
                                        } else {
                                            echo 0;
                                        }
                                    ?>
                                </th>
                                <th>{{ $total_clickout }}</th>
                                <th>
                                    <?php
                                        if($total_clicks > 0) {
                                            echo round(($total_clickout / $total_clicks), 2);
                                        } else {
                                            echo 0;
                                        }
                                    ?>
                                </th>
                                <th>
                                    <?php
                                        if($total_clickout > 0) {
                                            echo round(($total_rev / $total_clickout),2);
                                        } else {
                                            echo 0;
                                        }
                                    ?>        
                                </th>
                                <th>{{ $total_rev }}</th>
                                <th>{{ $total_gm }}</th>
                                <th>{{ $total_net_roi }}</th>
                            </tr>
                        </tbody>
                    </table>
                    
                </div>
            </div>

        </div>
    </div>
@stop


@section('adminlte_js')
    @parent

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
    
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">

    <script type="text/javascript">
        $(document).ready( function () {
            $('.datepicker').datepicker();
            $('#myTable').DataTable();
        } );
    </script>
@stop