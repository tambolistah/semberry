@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1 class="m-0 text-dark">Keyword Report Uploader</h1>
@stop

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <div class="row">
                        <div class="col-md-6 col-12">
                            <form method="post" action="keyword/parse" enctype="multipart/form-data">
                              <input type="hidden" name="_token" value={{ csrf_token() }}>
                              <div class="form-group">
                                <label for="exampleFormControlFile1">Select keyword report to upload</label>
                                <input type="file" name="report">
                              </div>

                              <hr />
                              <input type="submit" class="btn btn-success" />
                            </form>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
@stop