@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1 class="m-0 text-dark">Dashboard - Realtime</h1>
@stop

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">


                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="active nav-link" id="keyword-tab" data-toggle="tab" href="#keyword" role="tab" aria-controls="keyword" aria-selected="false">By Keywords</a>
                        </li>
                    </ul>

                    <?php
                        $aggregated_keywords = array();
                    ?>

                    <?php foreach($real_time_data as $real_time_instance) {
                        
                            $keyword_in = explode("xvvx", $real_time_instance->SUBID2)[0];
                            $source = "";
                            if ($real_time_instance->source == "tab")          { $source="Taboola";}
                            if ($real_time_instance->source == "fb")           { $source="Facebook";} 
                            if ($real_time_instance->source == "goo")          { $source="Google";} 
                            if ($real_time_instance->source == "org_search")   { $source="Organic";} 
                            if ($real_time_instance->source == "bing")         { $source="Bing";} 

                            /* ############################################################# AGGREGATED KEYWORD HANDLING ############################################################# */

                            if(!isset($aggregated_keywords[$keyword_in])) {
                                $aggregated_keywords[$keyword_in] = array();

                                $aggregated_keywords[$keyword_in]['date'] = $real_time_instance->DATE;
                                $aggregated_keywords[$keyword_in]['bidmatch'] = $real_time_instance->MATCH_TYPE;
                                $aggregated_keywords[$keyword_in]['source'] = $source;
                                $aggregated_keywords[$keyword_in]['country'] = $real_time_instance->channel_country;
                                $aggregated_keywords[$keyword_in]['campaign'] = $real_time_instance->CAMPAIGN;
                                $aggregated_keywords[$keyword_in]['keyword_in'] = $keyword_in;
                                $aggregated_keywords[$keyword_in]['forced_keyword'] = $real_time_instance->KEYWORD;
                                $aggregated_keywords[$keyword_in]['visits'] = $real_time_instance->visits;
                                $aggregated_keywords[$keyword_in]['clickout'] = $real_time_instance->clickout;
                            
                            } else {

                                $aggregated_keywords[$keyword_in]['date'] = $real_time_instance->DATE;
                                
                                if(empty($aggregated_keywords[$keyword_in]['bidmatch'])) {
                                    $aggregated_keywords[$keyword_in]['bidmatch'] = $real_time_instance->MATCH_TYPE;
                                }

                                $aggregated_keywords[$keyword_in]['source'] = $source;
                                
                                if(empty($aggregated_keywords[$keyword_in]['country'])) {
                                    $aggregated_keywords[$keyword_in]['country'] = $real_time_instance->channel_country;
                                }
                                
                                $aggregated_keywords[$keyword_in]['campaign'] = $real_time_instance->CAMPAIGN;
                                $aggregated_keywords[$keyword_in]['keyword_in'] = $keyword_in;
                                $aggregated_keywords[$keyword_in]['forced_keyword'] = $real_time_instance->KEYWORD;
                                $aggregated_keywords[$keyword_in]['visits'] += $real_time_instance->visits;
                                $aggregated_keywords[$keyword_in]['clickout'] += $real_time_instance->clickout;

                            }

                            /* ############################################################# AGGREGATED KEYWORD HANDLING ############################################################# */
                        ?>


                    <?php } ?>

                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane show active" id="keyword" role="tabpanel" aria-labelledby="keyword-tab">
                            
                            <p>&nbsp;</p>
                            <h4>Aggregated by Keyword</h4>
                            <hr />

                            <table id="aggregatedKeyword">
                                <thead>
                                    <tr>
                                        <th>DATE</td>
                                        <th>BIDMATCH</th>
                                        <th>SOURCE</th>
                                        <th>COUNTRY</th>
                                        <th>CAMPAIGN</th>
                                        <th>KEYWORD IN</th>
                                        <th>FORCED KEYWORD</th>
                                        <th>VISITS</th>
                                        <th>CLICKOUT</th>
                                        <th>RPC</th>
                                        <th>CR</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <?php foreach($aggregated_keywords as $aggregated_keyword) { ?>
                                        <tr>
                                            <td>
                                                {{ $aggregated_keyword['date'] }}
                                            </td>
                                            
                                            <td class="uppercase">
                                                {{ $aggregated_keyword['bidmatch'] }}
                                            </td>
                                            
                                            <td>
                                                {{ $aggregated_keyword['source'] }}
                                            </td>
                                            
                                            <td>
                                                {{ $aggregated_keyword['country'] }}
                                            </td>
                                            
                                            <td>
                                                {{ $aggregated_keyword['campaign'] }}
                                            </td>
                                            
                                            <td>
                                                {{ $aggregated_keyword['keyword_in'] }}
                                            </td>

                                            <td>
                                                {{ $aggregated_keyword['forced_keyword'] }}
                                            </td>

                                            <td>
                                                {{ $aggregated_keyword['visits'] }}
                                            </td>
                                            
                                            <td>
                                                {{ $aggregated_keyword['clickout'] }}
                                            </td>


                                            <td>
                                                <?php
                                                    if(isset($processed_channel_tracer[$aggregated_keyword['campaign']."-".$aggregated_keyword['keyword_in']."-".$aggregated_keyword['date']."-".$aggregated_keyword['bidmatch']])) {
                                                        $channel_data = $processed_channel_tracer[$aggregated_keyword['campaign']."-".$aggregated_keyword['keyword_in']."-".$aggregated_keyword['date']."-".$aggregated_keyword['bidmatch']];
                                                        if(isset($channel_data) && !empty($channel_data['revenue'])) {
                                                            if($aggregated_keyword['clickout'] > 0) {
                                                                echo round($channel_data['revenue']->EARNINGS / $aggregated_keyword['clickout'], 2);
                                                            } else {
                                                                echo $channel_data['revenue']->EARNINGS;
                                                            }
                                                        } else {
                                                            echo "0";
                                                        }
                                                    } else {
                                                        echo "0";
                                                    }
                                                ?>
                                            </td>

                                            <td>
                                                <?php
                                                    $ctr = 0;
                                                    if($aggregated_keyword['visits'] > 0) {
                                                        $ctr = ($aggregated_keyword['clickout'] / $aggregated_keyword['visits']) * 100;
                                                    }
                                                    
                                                    echo round($ctr) . "%";
                                                ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@stop


@section('adminlte_js')
    @parent

    <script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready( function () {
            $('#myTable').DataTable();
            $('#aggregatedCampaign').DataTable();
            $('#aggregatedKeyword').DataTable();
        } );
    </script>
@stop