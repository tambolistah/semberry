@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1 class="m-0 text-dark">Keyword CSV Download</h1>
@stop

@section('content')
  
    <?php
        $start = date('Y-m-d',(strtotime ( '-2 day' , time() ) ));
        $end = date("Y-m-d");
    ?>

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form method="POST">
                      <input type="hidden" name="_token" value={{ csrf_token() }}>
                      <div class="row">
                          <div class="col-md-6 col-12">
                              <div class="row">
                                  <div class="col-md-5 col-12">
                                      <div class="form-group">
                                          <input name="start" value="<?php echo $start; ?>" placeholder="Start Date" class="datepicker form-control" data-date-format="yyyy-mm-dd">
                                      </div>
                                  </div>
                                  <div class="col-md-5 col-12">
                                      <div class="form-group">
                                          <input name="end" value="<?php echo $end; ?>" placeholder="End Date" class="datepicker form-control" data-date-format="yyyy-mm-dd">
                                      </div>
                                  </div>
                                  <div class="col-md-2 col-12">
                                      <input type="submit" class="btn btn-success" value="Download" />
                                  </div>
                              </div>
                          </div>
                      </div>
                  </form>

                </div>
            </div>

        </div>
    </div>
@stop

@section('adminlte_js')
    @parent
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">

    <script type="text/javascript">
        $(document).ready( function () {
            $('.datepicker').datepicker();
        });
    </script>
@stop