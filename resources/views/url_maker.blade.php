@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1 class="m-0 text-dark">URL Maker</h1>
@stop

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <div class="row">
                        <div class="col-md-6 col-12">
                            <label class="sr-only" for="inlineFormInputGroupUsername2">Sub 3 Option</label>
                            <div class="input-group mb-2 mr-sm-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">Sub 3 Option</div>
                                </div>
                                <select class="form-control" id="sub3_selector">
                                    <option value="Bing">Bing</option>
                                    <option value="Google">Google</option>
                                </select>
                            </div>

                            <hr />

                            <h4>Your generated url</h4>

                            <div class="alert alert-info" role="alert">
                                Your campaign names must contain _{{ Auth::user()->campaign_user_id }}_. 
                            </div>

                            <div class="alert alert-info" role="info">
                                Values between [ ] must be replaced manually.
                            </div>

                            <div class="form-group">
                                <textarea id="generated_url" class="form-control">https://web-index.online/cb.sr/g.php?cn=US&sub1={msclkid}&sub2={keyword}|{BidMatchType}&sub3=Bing&c=[CAMPAIGN_NAME]&q=[KEYWORD]&u={{ Auth::user()->campaign_user_id }}</textarea>
                            </div>

                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
@stop


@section('adminlte_js')
    @parent

    <script type="text/javascript">
        $(document).ready( function () {
            $('#sub3_selector').change( function () {

                var bing_url = "https://web-index.online/cb.sr/g.php?cn=US&sub1={msclkid}&sub2={keyword}|{BidMatchType}&sub3=Bing&c=[CAMPAIGN_NAME]&q=[KEYWORD]&u={{ Auth::user()->campaign_user_id }}";
                var google_url = "https://web-index.online/cb.sr/g.php?cn=US&ch={channel}&sub1={msclkid}&sub2={keyword}|{BidMatchType}&sub3=Google&c=[CAMPAIGN_NAME]&q=[KEYWORD]&u={{ Auth::user()->campaign_user_id }}";

                console.log($(this).val());

                if($(this).val() == "Bing") {
                    $('textarea#generated_url').val(bing_url);
                } else {
                    $('textarea#generated_url').val(google_url);
                }

            });
        } );
    </script>
@stop