@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1 class="m-0 text-dark">Dashboard - Realtime</h1>
@stop

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">


                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">All</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="campaign-tab" data-toggle="tab" href="#campaign" role="tab" aria-controls="campaign" aria-selected="false">By Campaign</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="keyword-tab" data-toggle="tab" href="#keyword" role="tab" aria-controls="keyword" aria-selected="false">By Keywords</a>
                        </li>
                    </ul>

                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                            
                            <p>&nbsp;</p>
                            <h4>All</h4>
                            <hr />

                            <table id="myTable">              
                                <thead>
                                    <tr>
                                        <th>DATE</td>
                                        <th>BIDMATCH</th>
                                        <th>SOURCE</th>
                                        <th>COUNTRY</th>
                                        <th>CAMPAIGN</th>
                                        <th>KEYWORD IN</th>
                                        <th>FORCED KEYWORD</th>
                                        <th>VISITS</th>
                                        <th>CLICKOUT</th>
                                        <th>CR</th>
                                  </tr>
                                </thead>

                                <tbody>

                                    <?php
                                        $aggregated_campaigns = array();
                                        $aggregated_keywords = array();
                                    ?>

                                    <?php foreach($real_time_data as $real_time_instance) { ?>
                                        
                                        <?php
                                            $keyword_in = explode("xvvx", $real_time_instance->SUBID2)[0];
                                            $keyword_in = str_replace("q77q","+",$keyword_in);
                                        ?>

                                        <tr>
                                            <td>
                                                {{ $real_time_instance->DATE }}
                                            </td>
                                            
                                            <td class="uppercase">
                                                <?php
                                                    $match_type = "";

                                                    if($real_time_instance->MATCH_TYPE == "b" || $real_time_instance->MATCH_TYPE == "bb") {
                                                        $match_type = "Broad";                                                        
                                                    }else if($real_time_instance->MATCH_TYPE == "e" || $real_time_instance->MATCH_TYPE == "be") {
                                                        $match_type = "Exact";                                                        
                                                    }else if($real_time_instance->MATCH_TYPE == "p" || $real_time_instance->MATCH_TYPE == "bp") {
                                                        $match_type = "Phrase";                                                        
                                                    } else {
                                                        $match_type = $real_time_instance->MATCH_TYPE;                                                        
                                                    }

                                                    echo $match_type;
                                                ?>
                                            </td>
                                            
                                            <?php

                                                $source = "";
                                                if ($real_time_instance->conversion_source == "tab")          { $source="Taboola";}
                                                if ($real_time_instance->conversion_source == "fb")           { $source="Facebook";} 
                                                if ($real_time_instance->conversion_source == "google")       { $source="Google";} 
                                                if ($real_time_instance->conversion_source == "org_search")   { $source="Organic";} 
                                                if ($real_time_instance->conversion_source == "bing")         { $source="Bing";} 
                                            ?>
                                            
                                            <td>
                                                {{ $source }}
                                            </td>
                                            
                                            <td>
                                                {{ $real_time_instance->channel_country }}
                                            </td>
                                            
                                            <td>
                                                {{ $real_time_instance->CAMPAIGN }}
                                            </td>
                                            
                                            <td>
                                                <?php echo $keyword_in; ?>
                                            </td>

                                            <td>
                                                {{ $real_time_instance->KEYWORD }}
                                            </td>

                                            <td>
                                                {{ $real_time_instance->visits }}
                                            </td>
                                            
                                            <td>{{ $real_time_instance->clickout }}</td>

                                            <td>
                                                <?php
                                                    $ctr = 0;
                                                    if($real_time_instance->visits > 0) {
                                                        $ctr = ($real_time_instance->clickout / $real_time_instance->visits) * 100;
                                                    }
                                                    
                                                    echo round($ctr) . "%";
                                                ?>
                                            </td>
                                        </tr>

                                        <?php
                                            
                                            /* ############################################################# AGGREGATED CAMPAIGN HANDLING ############################################################# */
                                            
                                            if(!isset($aggregated_campaigns[$real_time_instance->CAMPAIGN])) {
                                                $aggregated_campaigns[$real_time_instance->CAMPAIGN] = array();
                                                
                                                $aggregated_campaigns[$real_time_instance->CAMPAIGN]['date'] = $real_time_instance->DATE;
                                                $aggregated_campaigns[$real_time_instance->CAMPAIGN]['bidmatch'] = $real_time_instance->MATCH_TYPE;
                                                $aggregated_campaigns[$real_time_instance->CAMPAIGN]['source'] = $source;
                                                $aggregated_campaigns[$real_time_instance->CAMPAIGN]['country'] = $real_time_instance->channel_country;
                                                $aggregated_campaigns[$real_time_instance->CAMPAIGN]['campaign'] = $real_time_instance->CAMPAIGN;
                                                $aggregated_campaigns[$real_time_instance->CAMPAIGN]['keyword_in'] = $keyword_in;
                                                $aggregated_campaigns[$real_time_instance->CAMPAIGN]['forced_keyword'] = $real_time_instance->KEYWORD;
                                                $aggregated_campaigns[$real_time_instance->CAMPAIGN]['visits'] = $real_time_instance->visits;
                                                $aggregated_campaigns[$real_time_instance->CAMPAIGN]['clickout'] = $real_time_instance->clickout;
                                            
                                            } else {

                                                $aggregated_campaigns[$real_time_instance->CAMPAIGN]['date'] = $real_time_instance->DATE;
                                                
                                                if($aggregated_campaigns[$real_time_instance->CAMPAIGN]['bidmatch']) {
                                                    $aggregated_campaigns[$real_time_instance->CAMPAIGN]['bidmatch'] = $real_time_instance->MATCH_TYPE;
                                                }
                                                
                                                $aggregated_campaigns[$real_time_instance->CAMPAIGN]['source'] = $source;
                                                
                                                if(empty($aggregated_campaigns[$real_time_instance->CAMPAIGN]['country'])) {
                                                    $aggregated_campaigns[$real_time_instance->CAMPAIGN]['country'] = $real_time_instance->channel_country;
                                                }
                                                
                                                $aggregated_campaigns[$real_time_instance->CAMPAIGN]['campaign'] = $real_time_instance->CAMPAIGN;
                                                $aggregated_campaigns[$real_time_instance->CAMPAIGN]['keyword_in'] = $keyword_in;
                                                $aggregated_campaigns[$real_time_instance->CAMPAIGN]['forced_keyword'] = $real_time_instance->KEYWORD;
                                                $aggregated_campaigns[$real_time_instance->CAMPAIGN]['visits'] += $real_time_instance->visits;
                                                $aggregated_campaigns[$real_time_instance->CAMPAIGN]['clickout'] += $real_time_instance->clickout;
                                            }

                                            /* ############################################################# AGGREGATED CAMPAIGN HANDLING ############################################################# */

                                            



                                            /* ############################################################# AGGREGATED KEYWORD HANDLING ############################################################# */

                                            if(!isset($aggregated_keywords[$keyword_in])) {
                                                $aggregated_keywords[$keyword_in] = array();

                                                $aggregated_keywords[$keyword_in]['date'] = $real_time_instance->DATE;
                                                $aggregated_keywords[$keyword_in]['bidmatch'] = $real_time_instance->MATCH_TYPE;
                                                $aggregated_keywords[$keyword_in]['source'] = $source;
                                                $aggregated_keywords[$keyword_in]['country'] = $real_time_instance->channel_country;
                                                $aggregated_keywords[$keyword_in]['campaign'] = $real_time_instance->CAMPAIGN;
                                                $aggregated_keywords[$keyword_in]['keyword_in'] = $keyword_in;
                                                $aggregated_keywords[$keyword_in]['forced_keyword'] = $real_time_instance->KEYWORD;
                                                $aggregated_keywords[$keyword_in]['visits'] = $real_time_instance->visits;
                                                $aggregated_keywords[$keyword_in]['clickout'] = $real_time_instance->clickout;
                                            
                                            } else {

                                                $aggregated_keywords[$keyword_in]['date'] = $real_time_instance->DATE;
                                                
                                                if(empty($aggregated_keywords[$keyword_in]['bidmatch'])) {
                                                    $aggregated_keywords[$keyword_in]['bidmatch'] = $real_time_instance->MATCH_TYPE;
                                                }

                                                $aggregated_keywords[$keyword_in]['source'] = $source;
                                                
                                                if(empty($aggregated_keywords[$keyword_in]['country'])) {
                                                    $aggregated_keywords[$keyword_in]['country'] = $real_time_instance->channel_country;
                                                }
                                                
                                                $aggregated_keywords[$keyword_in]['campaign'] = $real_time_instance->CAMPAIGN;
                                                $aggregated_keywords[$keyword_in]['keyword_in'] = $keyword_in;
                                                $aggregated_keywords[$keyword_in]['forced_keyword'] = $real_time_instance->KEYWORD;
                                                $aggregated_keywords[$keyword_in]['visits'] += $real_time_instance->visits;
                                                $aggregated_keywords[$keyword_in]['clickout'] += $real_time_instance->clickout;

                                            }

                                            /* ############################################################# AGGREGATED KEYWORD HANDLING ############################################################# */
                                        ?>


                                    <?php } ?>
                                </tbody>
                            </table>

                        </div>
                        <div class="tab-pane fade" id="campaign" role="tabpanel" aria-labelledby="campaign-tab">
                            
                            <p>&nbsp;</p>
                            <h4>Aggregated by Campaign</h4>
                            <hr />

                            <table id="aggregatedCampaign">
                                <thead>
                                    <tr>
                                        <th>DATE</td>
                                        <th>BIDMATCH</th>
                                        <th>SOURCE</th>
                                        <th>COUNTRY</th>
                                        <th>CAMPAIGN</th>
                                        <!-- <th>KEYWORD IN</th> -->
                                        <th>FORCED KEYWORD</th>
                                        <th>VISITS</th>
                                        <th>CLICKOUT</th>
                                        <th>CR</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <?php foreach($aggregated_campaigns as $aggregated_campaign) { ?>
                                        <tr>
                                            <td>
                                                {{ $aggregated_campaign['date'] }}
                                            </td>
                                            
                                            <td class="uppercase">

                                                <?php
                                                    $match_type = "";

                                                    if($aggregated_campaign['bidmatch'] == "b" || $aggregated_campaign['bidmatch'] == "bb") {
                                                        $match_type = "Broad";                                                        
                                                    }else if($aggregated_campaign['bidmatch'] == "e" || $aggregated_campaign['bidmatch'] == "be") {
                                                        $match_type = "Exact";                                                        
                                                    }else if($aggregated_campaign['bidmatch'] == "p" || $aggregated_campaign['bidmatch'] == "bp") {
                                                        $match_type = "Phrase";                                                        
                                                    } else {
                                                        $match_type = $aggregated_campaign['bidmatch'];                                                        
                                                    }

                                                    echo $match_type;
                                                ?>

                                            </td>
                                            
                                            <td>
                                                {{ $aggregated_campaign['source'] }}
                                            </td>
                                            
                                            <td>
                                                {{ $aggregated_campaign['country'] }}
                                            </td>
                                            
                                            <td>
                                                {{ $aggregated_campaign['campaign'] }}
                                            </td>
                                            
                                            <!-- <td>
                                                {{ $aggregated_campaign['keyword_in'] }}
                                            </td> -->

                                            <td>
                                                {{ $aggregated_campaign['forced_keyword'] }}
                                            </td>

                                            <td>
                                                {{ $aggregated_campaign['visits'] }}
                                            </td>
                                            
                                            <td>
                                                {{ $aggregated_campaign['clickout'] }}
                                            </td>

                                            <td>
                                                <?php
                                                    $ctr = 0;
                                                    if($aggregated_campaign['visits'] > 0) {
                                                        $ctr = ($aggregated_campaign['clickout'] / $aggregated_campaign['visits']) * 100;
                                                    }
                                                    
                                                    echo round($ctr) . "%";
                                                ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            
                            </table>
                        </div>
                        <div class="tab-pane fade" id="keyword" role="tabpanel" aria-labelledby="keyword-tab">
                            
                            <p>&nbsp;</p>
                            <h4>Aggregated by Keyword</h4>
                            <hr />

                            <table id="aggregatedKeyword">
                                <thead>
                                    <tr>
                                        <th>DATE</td>
                                        <th>BIDMATCH</th>
                                        <th>SOURCE</th>
                                        <th>COUNTRY</th>
                                        <th>CAMPAIGN</th>
                                        <th>KEYWORD IN</th>
                                        <th>FORCED KEYWORD</th>
                                        <th>VISITS</th>
                                        <th>CLICKOUT</th>
                                        <th>CR</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <?php foreach($aggregated_keywords as $aggregated_keyword) { ?>
                                        <tr>
                                            <td>
                                                {{ $aggregated_keyword['date'] }}
                                            </td>
                                            
                                            <td class="uppercase">

                                                <?php
                                                    $match_type = "";

                                                    if($aggregated_keyword['bidmatch'] == "b" || $aggregated_keyword['bidmatch'] == "bb") {
                                                        $match_type = "Broad";                                                        
                                                    }else if($aggregated_keyword['bidmatch'] == "e" || $aggregated_keyword['bidmatch'] == "be") {
                                                        $match_type = "Exact";                                                        
                                                    }else if($aggregated_keyword['bidmatch'] == "p" || $aggregated_keyword['bidmatch'] == "bp") {
                                                        $match_type = "Phrase";                                                        
                                                    } else {
                                                        $match_type = $aggregated_keyword['bidmatch'];                                                        
                                                    }

                                                    echo $match_type;
                                                ?>

                                            </td>
                                            
                                            <td>
                                                {{ $aggregated_keyword['source'] }}
                                            </td>
                                            
                                            <td>
                                                {{ $aggregated_keyword['country'] }}
                                            </td>
                                            
                                            <td>
                                                {{ $aggregated_keyword['campaign'] }}
                                            </td>
                                            
                                            <td>
                                                {{ $aggregated_keyword['keyword_in'] }}
                                            </td>

                                            <td>
                                                {{ $aggregated_keyword['forced_keyword'] }}
                                            </td>

                                            <td>
                                                {{ $aggregated_keyword['visits'] }}
                                            </td>
                                            
                                            <td>
                                                {{ $aggregated_keyword['clickout'] }}
                                            </td>

                                            <td>
                                                <?php
                                                    $ctr = 0;
                                                    if($aggregated_keyword['visits'] > 0) {
                                                        $ctr = ($aggregated_keyword['clickout'] / $aggregated_keyword['visits']) * 100;
                                                    }
                                                    
                                                    echo round($ctr) . "%";
                                                ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@stop


@section('adminlte_js')
    @parent

    <script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready( function () {
            $('#myTable').DataTable();
            $('#aggregatedCampaign').DataTable();
            $('#aggregatedKeyword').DataTable();
        } );
    </script>
@stop