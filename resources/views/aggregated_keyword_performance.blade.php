@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')

    <?php
        $rss = simplexml_load_file('https://trackingberry.com/valuta.xml');
        $euro_value = (float)$rss->eur;
        $dollar_to_eur_value = 1 / $euro_value;

        $clients = array("yahoo", "sedo");

        $query_string = $_SERVER['QUERY_STRING'];
        if($query_string != "") {
            $exploded_query_string = explode("&", $query_string);
            
            if(isset($_GET['client'])) {
                unset($exploded_query_string[count($exploded_query_string) - 1]);
            }

            $query_string = implode("&", $exploded_query_string);
        }

        $client = "yahoo";
        if(isset($_GET['client'])) {
            $client = $_GET['client'];
        }

        function clean($string) {
            $string = str_replace("Â", "", $string);
            $string = preg_replace('/\xc2\xa0/', '', $string);
            $string = urlencode(str_replace(" ", "", trim(strtolower($string))));
            return $string;
        }

        function to_ita_values($string) {
            $string = str_replace('.', ',', $string);
            return $string;
        }

        function special_character_parse($string) {
            $string = str_replace("Â", "", $string);
            $string = preg_replace('/\xc2\xa0/', ' ', $string);
            return $string;
        }

        $start = date('Y-m-d',(strtotime ( '-1 day' , time() ) ));
        $end = $start;

        if(isset($_GET['start']) && isset($_GET['end'])) {
            $start = $_GET['start'];
            $end = $_GET['end'];           
        }

        $search = "";
        if(isset($_GET['search'])) {
            $search = $_GET['search'];
        }

        $source = "bing";
        if(isset($_GET['source'])) {
            $source = $_GET['source'];
        }        
    ?>

    <div class="row">
        <div class="col-md-6">
            <h1 class="m-0 text-dark">
                Keyword Performance
                <a id="ing_aggregated_csv_download" class="btn inline-block btn-success">Export to CSV (ENG)</a>
                <a id="ita_aggregated_csv_download" class="btn inline-block btn-info">Export to CSV (ITA)</a>
            </h1>
        </div>
    </div>

    <hr />

    <form method="get">
        <div class="row">
            <div class="col-md-6 col-12">
                <div class="row">

                    <input name="source" type="hidden" value="<?php echo $source; ?>" />
    

                    <div class="col-md-5 col-12">
                        <div class="form-group">
                            <input name="start" value="<?php echo $start; ?>" placeholder="Start Date" class="datepicker form-control" data-date-format="yyyy-mm-dd">
                        </div>
                    </div>
                    <div class="col-md-5 col-12">
                        <div class="form-group">
                            <input name="end" value="<?php echo $end; ?>" placeholder="End Date" class="datepicker form-control" data-date-format="yyyy-mm-dd">
                        </div>
                    </div>
                    <div class="col-md-2 col-12">
                        <input type="submit" class="btn btn-success" value="Filter" />
                    </div>
                </div>
            </div>
        </div>
    </form>
@stop

@section('content')
    
    <?php
        $aggregated_first_line = "Campaign,Status,Budget,Ad Group,Keyword Label,Campaign Label,Keyword In,Forced KWD,Match Type,KWD Delivery,Bid,COST($),Impressions,Clicks,CTR,Avg_Cpc,Clickout,CR,RPC,REV($),GM($),Net ROI,CPC BE,User,Country\\n";
        $aggregated_keyword_csv_string_ita = $aggregated_first_line;
        $aggregated_keyword_csv_string_ing = $aggregated_first_line;


        $grand_budget_total = 0;
        $grand_bid_total = 0;
        $grand_cost_total = 0;
        $grand_impression_total = 0;
        $grand_click_total = 0;
        $grand_ctr_total = 0;
        $grand_avg_cpc_total = 0;
        $grand_clickout_total = 0;
        $grand_cr_total = 0;
        $grand_rpc_total = 0;
        $grand_rev_total = 0;
        $grand_gm_total = 0;
        $grand_net_roi = 0;
        $grand_cpc_be = 0;
    ?>

    <ul class="nav nav-tabs" id="myTab" role="tablist">
      <li class="nav-item">
        <a class="nav-link <?php if(!isset($_GET['source']) || $_GET['source'] == "bing") { echo "active"; } ?>" id="home-tab" data-toggle="external-link" href="?source=bing" role="tab" aria-controls="home" aria-selected="true">Bing</a>
      </li>
      <li class="nav-item">
        <a class="nav-link <?php if(isset($_GET['source']) && $_GET['source'] == "google") { echo "active"; } ?>" id="profile-tab" data-toggle="external-link" href="?source=google" role="tab" aria-controls="profile" aria-selected="false">Google</a>
      </li>
    </ul>
    
    <div id="group_performance">
        <div class="row">
            <div class="col-12">
                <div class="card">
                   <div class="card-body">

                        <br />

                        <table id="myTableSplit" class="table bordered striped">
                            <thead>
                                <tr>
                                    <th>Campaign</th>
                                    <th>Status</th>
                                    <th>Budget</th>
                                    <th>Ad group</th>
                                    <th>Keyword Labels</th>
                                    <th>Campaign Label</th>
                                    <th>Keyword in</th>
                                    <th>Forced KWD</th>
                                    <th>Match Type</th>
                                    <th>KWD Delivery</th>
                                    <th>Bid</th>
                                    <th>COST(&euro;)</th>
                                    <th>Impressions</th>
                                    <th>Clicks</th>
                                    <th>CTR</th>
                                    <th>Avg. Cpc</th>
                                    <th>Clickout</th>
                                    <th>CR</th>
                                    <th>RPC</th>
                                    <th>REV(&euro;)</th>
                                    <th>GM($)</th>
                                    <th>Net ROI</th>
                                    <th>CPC BE</th>
                                    <th>User</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                                <?php foreach($data['keywords'] as $keyword) { ?>                                    
                                    <?php
                                        $keyword->AD_GROUP = special_character_parse($keyword->AD_GROUP);
                                        $keyword->CAMPAIGN = special_character_parse($keyword->CAMPAIGN);
                                        $keyword->LABELS = special_character_parse($keyword->LABELS);
                                        $keyword->AD_GROUP = special_character_parse($keyword->AD_GROUP);
                                        
                                        $keyword_in_trimmed = clean($keyword->KEYWORD);
                                        $campaign_trimmed = clean($keyword->CAMPAIGN);
                                        $channel_index_key = $campaign_trimmed."-".$keyword->EXTRACTION_DATE."-".$keyword->MATCH_TYPE."-".$keyword_in_trimmed;
                                        $keyword_index_key = $campaign_trimmed."-".$keyword->MATCH_TYPE."-".$keyword_in_trimmed;
                                    ?>

                                    <?php if(isset($data['campaigns'][$campaign_trimmed])) { ?>

                                        <tr>
                                            <td>{{ $keyword->CAMPAIGN }}</th>
                                            <td>{{ $keyword->STATUS }}</td>
                                            <td>
                                                <?php
                                                    $budget = 0;

                                                    if(isset($keyword->budget)) {
                                                        $budget = $keyword->budget;
                                                    }

                                                    echo $budget;
                                                    $grand_budget_total += $budget;
                                                ?>
                                            </td>
                                            <td>{{ $keyword->AD_GROUP }}</td>
                                            <td>{{ $keyword->LABELS }}</td>
                                            <td>
                                                <?php
                                                    $campaign_label = "";

                                                    if(isset($data['campaigns'][$campaign_trimmed])) {
                                                        $campaign_label = special_character_parse($data['campaigns'][$campaign_trimmed]->LABELS);
                                                    }
                                                    
                                                    echo $campaign_label;
                                                ?>
                                            </td>
                                            <td>{{ $keyword->KEYWORD }}</td>
                                            <td>{{ $keyword->forced_keyword }}</td>
                                            <td>{{ $keyword->MATCH_TYPE }}</td>
                                            <td>{{ $keyword->DELIVERY }}</td>
                                            <td>{{ $keyword->BID }}</td>
                                            <td>
                                                <?php
                                                    $keyword->SPEND = round($keyword->SPEND * $dollar_to_eur_value,2);
                                                    $cost = $keyword->SPEND;
                                                    $grand_cost_total += $cost;   
                                                    echo $cost;
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                    $impr = (float)$keyword->IMPR;
                                                    $grand_impression_total += $impr;
                                                    echo $impr;
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                    $clicks = (int)$keyword->CLICKS;
                                                    $grand_click_total +=  $clicks;
                                                    echo $clicks;
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                    $ctr = "0";
                                                    if((int)$keyword->CLICKS > 0 && (int)$keyword->IMPR > 0) {
                                                        $ctr = round((int)$keyword->CLICKS / (int)$keyword->IMPR, 2);
                                                    }
                                                    
                                                    $grand_ctr_total += $ctr;
                                                    echo $ctr;
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                    $avg_cpc = "0";
                                                    if($keyword->CLICKS > 0) {
                                                        $avg_cpc = round($keyword->SPEND / $keyword->CLICKS, 2);
                                                    }

                                                    $grand_avg_cpc_total += $avg_cpc;
                                                    echo $avg_cpc;
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                    $campaign_clickout = 0;
                                                    $campaign_revenue_revenue = 0;
                                                    $total_tracked_conversions = 0;
                                                    

                                                    if($source == "bing") {

                                                        if(isset($data['keyword_revenues'][$keyword_index_key])) {
                                                            // foreach($data['keyword_revenues'][$keyword_index_key]['revenues'] as $keyword_revenue) {
                                                            //     $campaign_clickout += $keyword_revenue->CLICKS;
                                                            //     $campaign_revenue_revenue += round($keyword_revenue->EARNINGS * env('REVENUE_MULTIPLIER'), 2);
                                                            // }


                                                            // foreach($keyword_revenues as $keyword_by_channel) {
                                                                // foreach($data['keyword_revenues'][$keyword_index_key] as $key => $keyword_revenue_instance) {
                                                                    foreach($data['keyword_revenues'][$keyword_index_key] as $keyword_revenue_instance) {
                                                                        foreach($keyword_revenue_instance['revenues'] as $revenue_instance) {
                                                                            // foreach($revenue_instance as $inner_revenue_instance) {
                                                                                
                                                                                // echo $revenue_instance->EARNINGS;
                                                                                // echo "<br />";

                                                                                $campaign_clickout += $revenue_instance->CLICKS;
                                                                                $campaign_revenue_revenue += (float)$revenue_instance->EARNINGS;                                                                        
                                                                            // }
                                                                        }
                                                                    }

                                                                    

                                                                // }
                                                            // }

                                                            $computed_revenue = ($campaign_revenue_revenue * env('REVENUE_MULTIPLIER')) * $dollar_to_eur_value;
                                                            $campaign_revenue_revenue = round($computed_revenue, 2);
                                                        }
                                                    }


                                                    if($source == "google") {

                                                        $channel = null;

                                                        if(isset($data['google_conversion_data']['conversions_by_keyword'][$keyword_in_trimmed])) {
                                                            $channel = $data['google_conversion_data']['conversions_by_keyword'][$keyword_in_trimmed][0]->CHANNEL;
                                                            
                                                            if(isset($data['collated_revenue_with_channel_key'][strtoupper($channel)])) {
                                                                
                                                                foreach($data['collated_revenue_with_channel_key'][strtoupper($channel)] as $channel_revenue_instance) {
                                                                    $campaign_revenue_revenue += $channel_revenue_instance->EARNINGS;
                                                                }

                                                                $computed_revenue = ($campaign_revenue_revenue * env('REVENUE_MULTIPLIER')) * $dollar_to_eur_value;
                                                                $campaign_revenue_revenue = round($computed_revenue, 2);

                                                                // $campaign_revenue_revenue = $data['revenue_channel_key'][strtoupper($channel)]->EARNINGS;

                                                                if(isset($data['google_conversion_data']['total_tracked_conversions'][$channel])) {
                                                                    $total_tracked_conversions = count($data['google_conversion_data']['total_tracked_conversions'][$channel]);
                                                                }
                                                            }
                                                        }

                                                        if(isset($data['google_conversion_data']['clickouts'][$keyword_in_trimmed ."-". $keyword->MATCH_TYPE])) {
                                                            $campaign_clickout = count($data['google_conversion_data']['clickouts'][$keyword_in_trimmed ."-". $keyword->MATCH_TYPE]);
                                                        }
                                                    }

                                                    $grand_clickout_total += $campaign_clickout;
                                                    echo $campaign_clickout;
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                    $click_rate = "0";
                                                    if($keyword->CLICKS > 0) {
                                                        $click_rate = round($campaign_clickout / $keyword->CLICKS, 2);   
                                                    }

                                                    $grand_cr_total += $click_rate;
                                                    echo $click_rate;
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                    $raw_revenue_per_campaign = 0;
                                                    $rpc = "0";
                                                    if($campaign_clickout > 0) {
                                                        $rpc = round($campaign_revenue_revenue / $campaign_clickout, 2);
                                                    }

                                                    if($source == "google") {
                                                        if($total_tracked_conversions > 0) {
                                                            
                                                            $raw_revenue_per_campaign = $campaign_revenue_revenue;

                                                            $rpc = $raw_revenue_per_campaign/$total_tracked_conversions;
                                                            $campaign_revenue_revenue = $rpc * $campaign_clickout;
                                                            $campaign_revenue_revenue = round($campaign_revenue_revenue, 2);

                                                            $rpc = round($rpc, 2);
                                                        }
                                                    }

                                                    /*echo $total_tracked_conversions;
                                                    echo "<br />";
                                                    echo $raw_revenue_per_campaign;
                                                    echo "<br />";*/

                                                    $grand_rpc_total += $rpc;
                                                    echo $rpc;
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                    $grand_rev_total += $campaign_revenue_revenue;
                                                    echo $campaign_revenue_revenue;
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                        $gross_margin = $campaign_revenue_revenue - $keyword->SPEND;
                                                        $grand_gm_total += $gross_margin;
                                                        
                                                        $gross_margin = round($gross_margin, 2);
                                                        echo $gross_margin;
                                                    ?>
                                            </td>
                                            <td>
                                                <?php
                                                        $net_roi = "0";
                                                        if($keyword->SPEND != 0) {
                                                            $net_roi = ($campaign_revenue_revenue - $keyword->SPEND) / $keyword->SPEND;
                                                            $net_roi = round($net_roi, 2);
                                                        }

                                                        $grand_net_roi += $net_roi;
                                                        echo $net_roi;
                                                    ?>
                                            </td>
                                            <td>
                                                <?php
                                                    $cpc = "0";
                                                    if($keyword->CLICKS > 0 && $campaign_clickout > 0) {
                                                        $cpc = round(($campaign_revenue_revenue / $campaign_clickout) * ($campaign_clickout / $keyword->CLICKS), 2);
                                                    }

                                                    $grand_cpc_be += $cpc; 
                                                    echo $cpc;
                                                ?>
                                            </td>
                                            <td>
                                                {{ $keyword->USER }}
                                            </td>
                                        </tr>

                                        <?php
                                            $campaign_label = str_replace(",", "|", $campaign_label);
                                            $match_type = str_replace(";", "|", $keyword->MATCH_TYPE);

                                            $forced_keyword = htmlspecialchars($keyword->forced_keyword, ENT_QUOTES);
                                            $keyword->KEYWORD = htmlspecialchars($keyword->KEYWORD, ENT_QUOTES);

                                            $channel_country = "XX";
                                            if(isset($data['channel_tracer'][$channel_index_key])) {
                                                $channel_country = $data['channel_tracer'][$channel_index_key]['country'];
                                            }

                                            $aggregated_keyword_csv_string_ing .= "\"".$keyword->CAMPAIGN."\",\"".$keyword->STATUS."\",\"".$budget."\",\" ".$keyword->AD_GROUP." \",\"".$keyword->LABELS."\",\"".$campaign_label."\",\"".$keyword->KEYWORD."\",\"".$forced_keyword."\",\"".$match_type."\",\"".$keyword->DELIVERY."\",\"".$keyword->BID."\",\"".$cost."\",\"".$impr."\",\"".$clicks."\",\"".$ctr."\",\"".$avg_cpc."\",\"".$campaign_clickout."\",\"".$click_rate."\",\"".$rpc."\",\"".$campaign_revenue_revenue."\",\"".$gross_margin."\",\"".$net_roi."\",\"".$cpc."\",\"".$data['formatted_bid_users'][$keyword->USER]."\",\"".$channel_country."\""."\\n";
                                            $aggregated_keyword_csv_string_ita .= "\"".$keyword->CAMPAIGN."\",\"".$keyword->STATUS."\",\"".$budget."\",\" ".$keyword->AD_GROUP." \",\"".$keyword->LABELS."\",\"".$campaign_label."\",\"".$keyword->KEYWORD."\",\"".$forced_keyword."\",\"".$match_type."\",\"".$keyword->DELIVERY."\",\"".to_ita_values($keyword->BID)."\",\"".to_ita_values($cost)."\",\"".to_ita_values($impr)."\",\"".to_ita_values($clicks)."\",\"".to_ita_values($ctr)."\",\"".to_ita_values($avg_cpc)."\",\"".to_ita_values($campaign_clickout)."\",\"".to_ita_values($click_rate)."\",\"".to_ita_values($rpc)."\",\"".to_ita_values($campaign_revenue_revenue)."\",\"".to_ita_values($gross_margin)."\",\"".to_ita_values($net_roi)."\",\"".to_ita_values($cpc)."\",\"".$data['formatted_bid_users'][$keyword->USER]."\",\"".$channel_country."\""."\\n";
                                        ?>

                                    <?php } else {
                                        error_log($campaign_trimmed);
                                        error_log($keyword->CAMPAIGN);
                                    } ?>

                                    <?php 
                                    }
                                ?>

                            </tbody>
                        </table>

                        <hr />

                        <h3>Summation of figures</h3>
                        <table class="table table-striped table-bordered">
                            <tr>
                                <th>Budget</th>
                                <th>Cost(&euro;)</th>
                                <th>Impressions</th>
                                <th>Clicks</th>
                                <th>CTR</th>
                                <th>Avg. CPC</th>
                                <th>Clickout</th>
                                <th>CR</th>
                                <th>RPC</th>
                                <th>Revenue(&euro;)</th>
                                <th>GM</th>
                                <th>Net ROI</th>
                            </tr>

                            <tr>
                                <td>{{ $grand_budget_total }}</td>
                                <td>{{ round($grand_cost_total,2) }}</td>
                                <td>{{ $grand_impression_total }}</td>
                                <td>{{ $grand_click_total }}</td>
                                <td>
                                    <?php
                                        if($grand_impression_total > 0) {
                                            echo round(($grand_click_total / $grand_impression_total) * 100, 2);
                                        } else {
                                            echo 0;
                                        }
                                    ?>
                                </td>
                                <td>
                                    <?php
                                        $grand_avg_cpc = 0;
                                        if($grand_click_total > 0) {
                                            $grand_avg_cpc = round($grand_cost_total / $grand_click_total,2);
                                        }

                                        echo $grand_avg_cpc;
                                    ?>
                                </td>
                                <td>{{ $grand_clickout_total }}</td>
                                <td>
                                    <?php
                                        $grand_cr_total = 0;
                                        
                                        if($grand_click_total > 0) {
                                            $grand_cr_total = round($grand_clickout_total / $grand_click_total, 2);
                                        }

                                        echo $grand_cr_total;
                                    ?>
                                </td>
                                <td>
                                    <?php
                                        $grand_rpc_total = 0;
                                        if($grand_clickout_total > 0) {
                                            $grand_rpc_total = round($grand_rev_total / $grand_clickout_total, 2);
                                        }

                                        echo $grand_rpc_total;
                                    ?>
                                </td>
                                <td>{{ $grand_rev_total }}</td>
                                <td>{{ round($grand_rev_total - $grand_cost_total,2) }}</td>
                                <td>
                                    <?php
                                        $grand_net_roi = 0;
                                        if($grand_cost_total > 0) {
                                            $grand_net_roi = round(($grand_rev_total - $grand_cost_total) / $grand_cost_total, 2);
                                        }
                                        echo $grand_net_roi;
                                    ?>
                                </td>
                            </tr>
                        </table>


                        <p>&nbsp;</p>

                        <hr />

                        <h3>Raw Data Report</h3>
                        <table class="table table-striped table-bordered">
                            <tr>
                                <th>Date</th>
                                <th>Clickout</th>
                                <th>Revenue(&euro;)</th>
                                <th>Clicks</th>
                                <th>Cost(&euro;)</th>
                            </tr>
                            
                            <?php
                                $raw_total_clicks = 0;
                                $raw_total_earnings = 0;
                                $raw_campaign_clicks = 0;
                                $raw_campaign_cost = 0;
                            ?>

                            <?php foreach($data['report_data']['revenue_reports'] as $report_instance) { ?>
                                <tr>
                                    <td>{{ $report_instance->DATE }}</td>
                                    <td>
                                        <?php
                                            $clicks = $report_instance->clicks;
                                            $raw_total_clicks += $clicks;
                                            echo $clicks;
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                            $computed_revenue = ($report_instance->earnings * env('REVENUE_MULTIPLIER')) * $dollar_to_eur_value;
                                            $earnings = round($computed_revenue, 2);
                                            $raw_total_earnings += $earnings;
                                            echo $earnings;
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                            if(isset($data['report_data']['keyword_reports'][$report_instance->DATE])) {
                                                $campaign_clicks = $data['report_data']['keyword_reports'][$report_instance->DATE]->clicks;
                                                $raw_campaign_clicks += $campaign_clicks;
                                                echo $campaign_clicks;
                                            }
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                            if(isset($data['report_data']['keyword_reports'][$report_instance->DATE])) {
                                                $cost = round($data['report_data']['keyword_reports'][$report_instance->DATE]->spend  * $dollar_to_eur_value, 2);
                                                $raw_campaign_cost += $cost;
                                                echo $cost;
                                            }
                                        ?>
                                    </td>
                                </tr>
                            <?php } ?>

                            <tr>
                                <th>Total</th>
                                <th>{{ $raw_total_clicks }}</th>
                                <th>{{ $raw_total_earnings }}</th>
                                <th>{{ $raw_campaign_clicks }}</th>
                                <th>{{ $raw_campaign_cost }}</th>
                            </tr>

                        </table>

                        <hr />
                        <h4>Channels with Revenues and no Associated Keyword</h4>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Channel</th>
                                    <th>Forced Keyword</th>
                                    <th>Campaign</th>
                                    <th>Keyword In</th>
                                    <th>Match Type</th>
                                    <th>Revenue(&euro;)</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                                <?php
                                    $channel_revenue = 0;
                                ?>

                                <?php foreach($data['unused_channel_ids'] as $unused_channel_id) {
                                    if(isset($data['processed_channel_tracer_by_id'][$unused_channel_id]['revenue']->EARNINGS) && $data['processed_channel_tracer_by_id'][$unused_channel_id]['revenue']->EARNINGS > 0) { ?>
                                        
                                        <?php
                                            $channel_revenue += $data['processed_channel_tracer_by_id'][$unused_channel_id]['revenue']->EARNINGS;
                                        ?>

                                        <tr>
                                            <td>{{ $data['processed_channel_tracer_by_id'][$unused_channel_id]['date'] }}</td>
                                            <td>{{ $data['processed_channel_tracer_by_id'][$unused_channel_id]['channel'] }}</td>
                                            <td>{{ $data['processed_channel_tracer_by_id'][$unused_channel_id]['keyword'] }}</td>
                                            <td>{{ $data['processed_channel_tracer_by_id'][$unused_channel_id]['campaign'] }}</td>
                                            <td>{{ $data['processed_channel_tracer_by_id'][$unused_channel_id]['keyword_in'] }}</td>
                                            <td>{{ $data['processed_channel_tracer_by_id'][$unused_channel_id]['match_type'] }}</td>
                                            <td>
                                                <?php
                                                    $computed_revenue = ($data['processed_channel_tracer_by_id'][$unused_channel_id]['revenue']->EARNINGS * env('REVENUE_MULTIPLIER')) * $dollar_to_eur_value;
                                                    echo round($computed_revenue, 2);
                                                ?>
                                            </td>
                                        </tr>
                                    <?php }
                                } ?>
                            </tbody>
                        </table>

                        <hr />

                        <center>
                            <div class="alert alert-info" role="alert">
                                <h5>
                                    <?php
                                        $computed_revenue = ($channel_revenue * env('REVENUE_MULTIPLIER')) * $dollar_to_eur_value;
                                        $other_revenues = round($channel_revenue, 2)
                                    ?>
                                    Other revenues from channels: {{ $other_revenues }}
                                </h5>
                            </div>
                        </center>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php


        /* ####################################### CLICKOUT RELATED ####################################### */

        $isTotalClickoutMatched = true;

        $grand_clickout_total = (int)$grand_clickout_total;
        $raw_total_clicks = (int)$raw_total_clicks;
                        
        if($grand_clickout_total > $raw_total_clicks) {

            $percentage_of = ($raw_total_clicks / $grand_clickout_total) * 100;

            // means tolerance is 5% or less
            if($percentage_of < 95) {
                $isTotalClickoutMatched = false;
            }

        } else {
            if($grand_clickout_total < $raw_total_clicks) {
                $percentage_of = ($grand_clickout_total / $raw_total_clicks) * 100;

                // means tolerance is 5% or less
                if($percentage_of < 95) {
                    $isTotalClickoutMatched = false;
                }
            }
        }

        /* ####################################### CLICKOUT RELATED ####################################### */





        /* ####################################### CLICKOUT RELATED ####################################### */

        $isTotalRevenueMatched = true;

        $grand_rev_total = (int)$grand_rev_total;
        $raw_total_earnings = (int)$raw_total_earnings;
                        
        if($grand_rev_total > $raw_total_earnings) {

            $revenue_difference = $grand_rev_total - $raw_total_earnings;
            $revenue_threshold = $raw_total_earnings * 0.05;

            if($revenue_difference > $revenue_threshold) {
                $isTotalRevenueMatched = false;
            }

        } else {
            if($grand_rev_total < $raw_total_earnings) {
                
                $revenue_difference = $raw_total_earnings - $grand_rev_total;
                $revenue_threshold = $grand_rev_total * 0.05;

                if($revenue_difference > $revenue_threshold) {
                    $isTotalRevenueMatched = false;
                }
            }
        }

        /* ####################################### CLICKOUT RELATED ####################################### */





        /* ####################################### CLICKOUT RELATED ####################################### */

        $isTotalClickMatched = true;

        $grand_click_total = (int)$grand_click_total;
        $raw_campaign_clicks = (int)$raw_campaign_clicks;
                        
        if($grand_click_total > $raw_campaign_clicks) {

            $clicks_difference = $grand_click_total - $raw_campaign_clicks;
            $clicks_threshold = $raw_campaign_clicks * 0.05;

            if($clicks_difference > $clicks_threshold) {
                $isTotalClickMatched = false;
            }

        } else {
            if($grand_click_total < $raw_campaign_clicks) {
                
                $clicks_difference = $raw_campaign_clicks - $grand_click_total;
                $clicks_threshold = $grand_click_total * 0.05;

                if($clicks_difference > $clicks_threshold) {
                    $isTotalClickMatched = false;
                }
            }
        }

        /* ####################################### CLICKOUT RELATED ####################################### */

        



        /* ####################################### CLICKOUT RELATED ####################################### */

        $isTotalCostMatched = true;

        $grand_cost_total = (int)$grand_cost_total;
        $raw_campaign_cost = (int)$raw_campaign_cost;
                        
        if($grand_cost_total > $raw_campaign_cost) {

            $cost_difference = $grand_cost_total - $raw_campaign_cost;
            $cost_threshold = $raw_campaign_cost * 0.05;

            if($cost_difference > $cost_threshold) {
                $isTotalCostMatched = false;
            }

        } else {

            if($grand_cost_total < $raw_campaign_cost) {
                
                $cost_difference = $raw_campaign_cost - $grand_cost_total;
                $cost_threshold = $grand_cost_total * 0.05;

                if($cost_difference > $cost_threshold) {
                    $isTotalCostMatched = false;
                }
            }

        }

        /* ####################################### CLICKOUT RELATED ####################################### */
        

        if(!$isTotalClickoutMatched || !$isTotalRevenueMatched || !$isTotalClickMatched || !$isTotalCostMatched) {

            ?>

                <div class="bottom-sticky-wrapper">
                    <div class="alert alert-danger">The raw data is not matching with the presented report</div>
                </div>

            <?php

        } else if($start == $end) {
                         
            ?>
                
                <div style="background: #fff;width: 100%;padding: 10px;box-shadow: 0px 0px 10px #ccc;left: 0;" class="bottom-sticky-wrapper text-right col-12">
                    <h5>Raw Report and Calculated Report Matches</h5><button id="sync_to_report_server" style="margin-bottom: 20px;" class="btn btn-primary">Sync To Report Database</button>
                </div>

            <?php

        }
    ?>


    <div style="display: none;">
        <form action="/csv_exporter" method="POST">
            @csrf
            <textarea name="csv_string"><?php echo $aggregated_keyword_csv_string_ing; ?></textarea>
            <input type="text" name="export_name" value="<?php echo "aggregated_".time()."_ing_$source.csv" ?>" />
            <input id="target_ing_aggregated_csv_download" type="submit" />
        </form>

        <form action="/csv_exporter" method="POST">
            @csrf
            <textarea name="csv_string"><?php echo $aggregated_keyword_csv_string_ita; ?></textarea>
            <input type="text" name="export_name" value="<?php echo "aggregated_".time()."_ita_$source.csv" ?>" />
            <input id="target_ita_aggregated_csv_download" type="submit" />
        </form>
    </div>

@stop

@section('adminlte_js')
    @parent

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
    
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">

    <script type="text/javascript">
        $(document).ready( function () {

            $(".dropdown-menu").on('click', 'a', function(){
                var base_url = "<?php echo url()->current(); ?>";
                var option = $(this).attr("href");
                var get_data = '<?php echo $query_string; ?>';
                
                var target = "";
                if(get_data != "") {
                    target = "?"+get_data+"&client="+option;
                } else {
                    target = "?client="+option;
                }

                target = base_url+target;
                window.location.href = target;

                return false;
           });
            
            $('.datepicker').datepicker();
            var current_time = <?php echo time(); ?>;
            var source = "<?php echo $source; ?>";

            var current_time = <?php echo time(); ?>;

            $('#ing_aggregated_csv_download').click (function () {
                $('#target_ing_aggregated_csv_download').click();
            });

            $('#ita_aggregated_csv_download').click (function () {
                $('#target_ita_aggregated_csv_download').click();
            });

            $(document).ready( function () {
                $('#myTableSplit').DataTable();
            } );


            $('#sync_to_report_server').click( function () {
                var csvContent = '<?php echo $aggregated_keyword_csv_string_ing; ?>'; //here we load our csv data 

                var post_data = {
                    extraction_date : "<?php echo $end; ?>",
                    source : source,
                    csvContent : csvContent
                };

                $.ajax({
                    url: "http://reports-semberry.cloud/keyword_upload",
                    type: "POST",
                    crossDomain: true,
                    data: post_data,
                    dataType: "json",
                    success: function (response) {
                        
                        var string = "Sync was a "+response.message+" with "+response.keyword_synced+" keyword created";
                        alert(string);
                        
                    },

                    error: function (xhr, status) {
                        // alert("error");
                    }
                });
            });

        });
    </script>
@stop