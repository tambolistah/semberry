@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')

    <?php
        $rss = simplexml_load_file('https://trackingberry.com/valuta.xml');
        $euro_value = (float)$rss->eur;
        $dollar_to_eur_value = 1 / $euro_value;

        $start = date('Y-m-d',(strtotime ( '-1 day' , time() ) ));
        $end = $start;

        $clients = array("yahoo", "sedo");

        $query_string = $_SERVER['QUERY_STRING'];
        if($query_string != "") {
            $exploded_query_string = explode("&", $query_string);
            
            if(isset($_GET['client'])) {
                unset($exploded_query_string[count($exploded_query_string) - 1]);
            }

            $query_string = implode("&", $exploded_query_string);
        }

        $client = "yahoo";
        if(isset($_GET['client'])) {
            $client = $_GET['client'];
        }

        $source = "bing";
        if(isset($_GET['source'])) {
            $source = $_GET['source'];
        }

        if(isset($_GET['start']) && isset($_GET['end'])) {
            $start = $_GET['start'];
            $end = $_GET['end'];           
        }

        function to_ita_values($string) {
            $string = str_replace('.', ',', $string);
            return $string;
        }

        function clean($string) {
            $string = str_replace("Â", "", $string);
            $string = preg_replace('/\xc2\xa0/', '', $string);
            $string = urlencode(str_replace(" ", "", trim(strtolower($string))));
            return $string;
        }

        function special_character_parse($string) {
            $string = str_replace("Â", "", $string);
            $string = preg_replace('/\xc2\xa0/', '', $string);
            return $string;
        }
    ?>

    <h1 class="m-0 text-dark">Group Performance</h1>

    <hr />

    <div class="row">
        <div class="col-md-4 col-12">
            <form method="get">
                <div class="row">

                    <?php if(isset($_GET['source'])) { ?>
                        <input name="source" type="hidden" value="<?php echo $_GET['source']; ?>" />
                    <?php } ?>

                    <div class="col-md-5 col-12">
                        <div class="form-group">
                            <input name="start" value="<?php echo $start; ?>" placeholder="Start Date" class="datepicker form-control" data-date-format="yyyy-mm-dd">
                        </div>
                    </div>
                    <div class="col-md-5 col-12">
                        <div class="form-group">
                            <input name="end" value="<?php echo $end; ?>" placeholder="End Date" class="datepicker form-control" data-date-format="yyyy-mm-dd">
                        </div>
                    </div>
                    <div class="col-md-2 col-12">
                        <input type="submit" class="btn btn-success" value="Filter" />
                    </div>
                </div>
            </form>

        </div>
    </div>
@stop

@section('content')
    
    <?php
        $aggregated_first_line = "Campaign,Status,Ad Group,Budget,Labels,Bid,Cost,Impressions,Clicks,CTR,Avg_CPC,Clickout,CR,RPC,REV,GM,Net_ROI,CPC BE,User,Country\\n";
        $aggregated_campaign_csv_string_ita = $aggregated_first_line;
        $aggregated_campaign_csv_string_ing = $aggregated_first_line;


        $grand_budget_total = 0;
        $grand_bid_total = 0;
        $grand_cost_total = 0;
        $grand_impression_total = 0;
        $grand_click_total = 0;
        $grand_ctr_total = 0;
        $grand_avg_cpc_total = 0;
        $grand_clickout_total = 0;
        $grand_cr_total = 0;
        $grand_rpc_total = 0;
        $grand_rev_total = 0;
        $grand_gm_total = 0;
        $grand_net_roi = 0;
        $grand_cpc_be = 0;

        
        $split_first_line = "Campaign,Status,Ad Group,Budget,Ad group,AD Delivery,AD Labels,Bid Strategy Type,Bid,Cost,Impressions,Clicks,CTR,Avg_CPC,Clickout,CR,RPC,REV,GM,Net_ROI,CPC BE,User,Country\\n";
        $split_campaign_csv_string_ita = $split_first_line;
        $split_campaign_csv_string_eng = $split_first_line;


        /*$split_grand_budget_total = 0;
        $split_grand_bid_total = 0;
        $split_grand_cost_total = 0;
        $split_grand_impression_total = 0;
        $split_grand_click_total = 0;
        $split_grand_ctr_total = 0;
        $split_grand_avg_cpc_total = 0;
        $split_grand_clickout_total = 0;
        $split_grand_cr_total = 0;
        $split_grand_rpc_total = 0;
        $split_grand_rev_total = 0;
        $split_grand_gm_total = 0;
        $split_grand_net_roi = 0;
        $split_grand_cpc_be = 0;*/

        $raw_total_revenue = 0;
    ?>

    
    <ul class="nav nav-tabs" id="myTab" role="tablist">
      <li class="nav-item">
        <a class="nav-link <?php if(!isset($_GET['source']) || $_GET['source'] == "bing") { echo "active"; } ?>" id="home-tab" data-toggle="external-link" href="?source=bing" role="tab" aria-controls="home" aria-selected="true">Bing</a>
      </li>
      <li class="nav-item">
        <a class="nav-link <?php if(isset($_GET['source']) && $_GET['source'] == "google") { echo "active"; } ?>" id="profile-tab" data-toggle="external-link" href="?source=google" role="tab" aria-controls="profile" aria-selected="false">Google</a>
      </li>
    </ul>

    <div id="group_performance">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        
                        <div class="row">
                            <div class="col-md-6">
                                <h4>
                                    Aggregated
                                    <a id="ing_aggregated_csv_download" class="btn inline-block btn-success">Export to CSV (ENG)</a>
                                    <a id="ita_aggregated_csv_download" class="btn inline-block btn-info">Export to CSV (ITA)</a>
                                </h4>
                            </div>
                        </div>

                        <hr />

                        <div class="relative-wrapper">
                            <table id="myTable">
                                <thead>
                                    <tr>
                                        <th>Campaign</th>
                                        <th>Status</th>
                                        <th>Ad Group</th>
                                        <th>Budget</th>
                                        <th>Label</th>
                                        <th>Bid</th>
                                        <th>COST(&euro;)</th>
                                        <th>Impressions</th>
                                        <th>Clicks</th>
                                        <th>CTR </th>
                                        <th>Avg. CPC</th>
                                        <th>Clickout</th>
                                        <th>CR</th>
                                        <th>RPC</th>
                                        <th>REV(&euro;)</th>
                                        <th>GM</th>
                                        <th>Net ROI</th>
                                        <th>CPC BE</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php foreach($data['cost_report'] as $cost_report_instance) { ?>

                                        <tr>
                                            <td>{{ $cost_report_instance->CAMPAIGN }}</td>
                                            <td>{{ $cost_report_instance->STATUS }}</td>
                                            <td>
                                                <?php
                                                    echo str_replace("Â", "", $cost_report_instance->LABELS);
                                                ?>
                                            </td>
                                            <td>{{ $cost_report_instance->BUDGET }}</td>
                                            <td>{{ $cost_report_instance->BID_STRATEGY_TYPE }}</td>
                                            <td>
                                                
                                                <?php
                                                    $bid = 0;
                                                    $campaign_index_key = clean($cost_report_instance->CAMPAIGN);
                                                    if(isset($data['processed_keyword_data']['bid_cost_by_campaign'][$campaign_index_key])) {
                                                        $bid = $data['processed_keyword_data']['bid_cost_by_campaign'][$campaign_index_key];
                                                    }

                                                    echo $bid;
                                                ?>

                                            </td>
                                            <td>
                                                <?php
                                                    $cost_report_instance->SPEND = round(($cost_report_instance->SPEND * $dollar_to_eur_value),2);
                                                    echo $cost_report_instance->SPEND;
                                                ?>
                                            </td>
                                            <td>{{ $cost_report_instance->IMPR }}</td>
                                            <td>{{ $cost_report_instance->CLICKS }}</td>
                                            <td>
                                                <?php
                                                    $ctr = "0";
                                                    if((int)$cost_report_instance->CLICKS > 0) {
                                                        $ctr = round(((int)$cost_report_instance->CLICKS / (int)$cost_report_instance->IMPR) * 100, 2);
                                                    }
                                                    echo $ctr;
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                    $avg_cpc = "0";
                                                    if($cost_report_instance->CLICKS > 0) {
                                                        $avg_cpc = round($cost_report_instance->SPEND / $cost_report_instance->CLICKS, 2);
                                                    }

                                                    echo $avg_cpc;
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                    $click_out_total = 0;
                                                    $campaign_revenue = 0;
                                                    $revenue_revenue = 0;

                                                    if(isset($data['processed_report']['channel_with_campaign_data_key'][$campaign_index_key])) {

                                                        foreach($data['processed_report']['channel_with_campaign_data_key'][$campaign_index_key] as $channel_instance) {
                                                            if(isset($channel_instance['REVENUE'])) {
                                                                
                                                                foreach($channel_instance['REVENUE'] as $revenue_instance) {
                                                                    $click_out_total += $revenue_instance->CLICKS;
                                                                    $revenue_revenue  += $revenue_instance->EARNINGS;
                                                                    $raw_total_revenue += $revenue_instance->EARNINGS;
                                                                }

                                                            }
                                                        }

                                                        $computed_revenue = ($revenue_revenue * env('REVENUE_MULTIPLIER')) * $dollar_to_eur_value;
                                                        $revenue_revenue = round($computed_revenue, 2);
                                                    }

                                                    echo $click_out_total;
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                    $click_rate = "0";
                                                    if(isset($data['processed_report']['channel_with_campaign_data_key'][$campaign_index_key]) && ($click_out_total > 0 && $cost_report_instance->CLICKS > 0)) { 
                                                        if($cost_report_instance->CLICKS > 0) {
                                                            $click_rate = round($click_out_total / $cost_report_instance->CLICKS, 2);
                                                        }
                                                    }
                                                    
                                                    echo $click_rate;
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                    $rate_per_click = "0";
                                                    if(isset($data['processed_report']['channel_with_campaign_data_key'][$campaign_index_key]) && $click_out_total > 0) {
                                                        if($click_out_total > 0) {
                                                            $rate_per_click = round($revenue_revenue / $click_out_total, 2);
                                                        }
                                                    }
                                                    
                                                    echo $rate_per_click;
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                    echo $revenue_revenue; 
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                    $gross_margin = $revenue_revenue - $cost_report_instance->SPEND;
                                                    $gross_margin = round($gross_margin, 2);
                                                    echo $gross_margin;
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                    $net_roi = "0";
                                                    if($cost_report_instance->SPEND > 0) {
                                                        $net_roi = round(($revenue_revenue - $cost_report_instance->SPEND) / $cost_report_instance->SPEND, 2);
                                                    }

                                                    echo $net_roi;
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                    $cpc = "0";
                                                    if(isset($data['processed_report']['channel_with_campaign_data_key'][$campaign_index_key]) && $click_out_total > 0) {
                                                        if(isset($data['processed_report']['channel_with_campaign_data_key'][$campaign_index_key]) && ($click_out_total > 0 && $cost_report_instance->CLICKS > 0)) {
                                                            
                                                            if($click_out_total > 0 && $cost_report_instance->CLICKS > 0) {
                                                                $cpc = round((($revenue_revenue / $click_out_total) * ($click_out_total / $cost_report_instance->CLICKS)), 2);
                                                            }

                                                        }
                                                    }

                                                    echo $cpc;

                                                ?>
                                            </td>
                                        </tr>
                                    <?php
                                            $campaign_labels = str_replace(";", "|", $cost_report_instance->LABELS);
                                            $bid_strategy_type = str_replace(";", "|", $cost_report_instance->BID_STRATEGY_TYPE);

                                            $channel_country = "XX";
                                            if(isset($data['processed_report']['channel_with_campaign_data_key'][$campaign_index_key])) {
                                                $channel_country = $data['processed_report']['channel_with_campaign_data_key'][$campaign_index_key][0]['COUNTRY'];
                                            }

                                            $aggregated_campaign_csv_string_ing .= "\"".$cost_report_instance->CAMPAIGN."\",\"".$cost_report_instance->STATUS."\",\"".$campaign_labels."\",\"".$cost_report_instance->BUDGET."\",\"".$bid_strategy_type."\",\"".$bid."\",\"".$cost_report_instance->SPEND."\",\"".$cost_report_instance->IMPR."\",\"".$cost_report_instance->CLICKS."\",\"".$ctr."\",\"".$avg_cpc."\",\"".$click_out_total."\",\"".$click_rate."\",\"".$rate_per_click."\",\"".$revenue_revenue."\",\"".$gross_margin."\",\"".$net_roi."\",\"".$cpc."\",\"".$data['formatted_bid_users'][$cost_report_instance->USER]."\",\"".$channel_country."\""."\\n";
                                            $aggregated_campaign_csv_string_ita .= "\"".$cost_report_instance->CAMPAIGN."\",\"".$cost_report_instance->STATUS."\",\"".$campaign_labels."\",\"".to_ita_values($cost_report_instance->BUDGET)."\",\"".$bid_strategy_type."\",\"".to_ita_values($bid)."\",\"".to_ita_values($cost_report_instance->SPEND)."\",\"".to_ita_values($cost_report_instance->IMPR)."\",\"".to_ita_values($cost_report_instance->CLICKS)."\",\"".to_ita_values($ctr)."\",\"".to_ita_values($avg_cpc)."\",\"".to_ita_values($click_out_total)."\",\"".to_ita_values($click_rate)."\",\"".to_ita_values($rate_per_click)."\",\"".to_ita_values($revenue_revenue)."\",\"".to_ita_values($gross_margin)."\",\"".to_ita_values($net_roi)."\",\"".to_ita_values($cpc)."\",\"".$data['formatted_bid_users'][$cost_report_instance->USER]."\",\"".$channel_country."\""."\\n";

                                            $grand_budget_total         += (float)$cost_report_instance->BUDGET;
                                            $grand_bid_total            += (float)$bid;
                                            $grand_cost_total           += (float)$cost_report_instance->SPEND;
                                            $grand_impression_total     += (float)$cost_report_instance->IMPR;
                                            $grand_click_total          += (float)$cost_report_instance->CLICKS;

                                            $grand_ctr_total            += (float)$ctr;
                                            $grand_avg_cpc_total        += (float)$avg_cpc;

                                            $grand_clickout_total       += (float)$click_out_total;
                                            $grand_cr_total             += (float)$click_rate;
                                            $grand_rpc_total            += (float)$rate_per_click;
                                            $grand_rev_total            += (float)$revenue_revenue;
                                            $grand_gm_total             += (float)$gross_margin;
                                            $grand_net_roi              += (float)$net_roi;
                                            $grand_cpc_be               += (float)$cpc;
                                        }

                                        $grand_ctr_total_computed = 0;
                                        $grand_total_cpc_computed = 0;
                                        $grand_cr_total_computed = 0;
                                        $grand_rpc_total_computed = 0;

                                        if($grand_impression_total > 0) {
                                            $grand_ctr_total_computed = round(($grand_click_total / $grand_impression_total) * 100, 2);
                                        }

                                        if($grand_click_total > 0) {
                                            $grand_total_cpc_computed = round($grand_cost_total / $grand_click_total,2);
                                            $grand_cr_total_computed = round($grand_clickout_total / $grand_click_total, 2);
                                        }

                                        if($grand_clickout_total > 0) {
                                            $grand_rpc_total_computed = round($grand_rev_total / $grand_clickout_total, 2);
                                        }

                                        // $aggregated_campaign_csv_string_ing .= "\"Total\",\"Total\",\"Total\",\"".$grand_budget_total."\",\"Total\",\"".$grand_bid_total."\",\"".$grand_cost_total."\",\"".$grand_impression_total."\",\"".$grand_click_total."\",\"". $grand_ctr_total_computed ."\",\"". $grand_total_cpc_computed ."\",\"".$grand_clickout_total."\",\"". $grand_cr_total_computed ."\",\"". $grand_rpc_total_computed ."\",\"". $grand_rev_total."\",\"".$grand_gm_total."\",\"".$grand_net_roi."\",\"".$grand_cpc_be."\""."\\n";
                                        // $aggregated_campaign_csv_string_ita = str_replace(".", ",", $aggregated_campaign_csv_string_ing);

                                    ?>
                                </tbody>
                            </table>

                            <hr />

                            <h3>Summation of figures</h3>
                            <table class="table table-striped table-bordered">
                                <tr>
                                    <th>Budget</th>
                                    <th>Cost(&euro;)</th>
                                    <th>Impressions</th>
                                    <th>Clicks</th>
                                    <th>CTR</th>
                                    <th>Avg. CPC</th>
                                    <th>Clickout</th>
                                    <th>CR</th>
                                    <th>RPC</th>
                                    <th>Revenue(&euro;)</th>
                                    <th>GM</th>
                                    <th>Net ROI</th>
                                </tr>

                                <tr>
                                    <td>{{ $grand_budget_total }}</td>
                                    <td>{{ round($grand_cost_total,2) }}</td>
                                    <td>{{ $grand_impression_total }}</td>
                                    <td>{{ $grand_click_total }}</td>
                                    <td>{{ $grand_ctr_total_computed }}</td>
                                    <td>{{ $grand_total_cpc_computed }}</td>
                                    <td>{{ $grand_clickout_total }}</td>
                                    <td>{{ $grand_cr_total_computed }}</td>
                                    <td>{{ $grand_rpc_total_computed }}</td>
                                    <td>{{ $grand_rev_total }}</td>
                                    <td>
                                        {{ round(($grand_rev_total - $grand_cost_total),2) }}
                                    </td>
                                    <td>
                                        <?php
                                            $computed_cpc_be = 0;
                                            if($grand_cost_total > 0) {
                                                $computed_cpc_be = round(($grand_rev_total - $grand_cost_total) / $grand_cost_total, 2);
                                            }

                                            echo $computed_cpc_be;
                                        ?>
                                    </td>
                                </tr>
                            </table>

                            <p>&nbsp;</p>

                            <hr />

                            <h3>Raw Data Report</h3>
                            <table class="table table-striped table-bordered">
                                <tr>
                                    <th>Date</th>
                                    <th>Clickout</th>
                                    <th>Revenue(&euro;)</th>
                                    <th>Budget</th>
                                    <th>Clicks</th>
                                    <th>Cost(&euro;)</th>
                                </tr>
                                
                                <?php
                                    $raw_total_clicks = 0;
                                    $raw_total_earnings = 0;
                                    $raw_total_budget = 0;
                                    $raw_campaign_clicks = 0;
                                    $raw_campaign_cost = 0;
                                ?>

                                <?php foreach($data['report_data']['revenue_reports'] as $report_instance) { ?>
                                    <tr>
                                        <td>{{ $report_instance->DATE }}</td>
                                        <td>
                                            <?php
                                                $clicks = $report_instance->clicks;
                                                $raw_total_clicks += $clicks;
                                                echo $clicks;
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                                $computed_revenue = ($report_instance->earnings * env('REVENUE_MULTIPLIER')) * $dollar_to_eur_value;
                                                $earnings = round($computed_revenue, 2);
                                                $raw_total_earnings += $earnings;
                                                echo $earnings;
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                                if(isset($data['report_data']['campaign_reports'][$report_instance->DATE])) {
                                                    $budget = $data['report_data']['campaign_reports'][$report_instance->DATE]->budget;
                                                    $raw_total_budget += $budget;
                                                    echo $budget;
                                                }
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                                if(isset($data['report_data']['campaign_reports'][$report_instance->DATE])) {
                                                    $campaign_clicks = $data['report_data']['campaign_reports'][$report_instance->DATE]->campaign_clicks;
                                                    $raw_campaign_clicks += $campaign_clicks;
                                                    echo $campaign_clicks;
                                                }
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                                if(isset($data['report_data']['campaign_reports'][$report_instance->DATE])) {
                                                    $cost = round(($data['report_data']['campaign_reports'][$report_instance->DATE]->cost * $dollar_to_eur_value),2);
                                                    $raw_campaign_cost += $cost;
                                                    echo $cost;
                                                }
                                            ?>
                                        </td>
                                    </tr>
                                <?php } ?>

                                <tr>
                                    <th>Total</th>
                                    <th>{{ $raw_total_clicks }}</th>
                                    <th>{{ $raw_total_earnings }}</th>
                                    <th>{{ $raw_total_budget }}</th>
                                    <th>{{ $raw_campaign_clicks }}</th>
                                    <th>{{ $raw_campaign_cost }}</th>
                                </tr>

                            </table>
                        </div>
                    </div>
                </div>
                <div class="card">
                   <div class="card-body">
                        <h4>
                            Split
                            <a id="ing_split_csv_download" class="btn inline-block btn-success">Export to CSV (ENG)</a>
                            <a id="ita_split_csv_download" class="btn inline-block btn-info">Export to CSV (ITA)</a>
                        </h4>

                        <br />

                        <table id="myTableSplit" class="table bordered striped">
                                <thead>
                                    <tr>
                                        <th>Campaign</th>
                                        <th>Status</th>
                                        <th>Ad Group</th>
                                        <th>Budget</th>
                                        <th>Ad group</th>
                                        <th>Delivery</th>
                                        <th>Label</th>
                                        <th>Bid</th>
                                        <th>COST(&euro;)</th>
                                        <th>Impressions</th>
                                        <th>Clicks</th>
                                        <th>CTR</th>
                                        <th>Avg. CPC</th>
                                        <th>Clickout</th>
                                        <th>CR</th>
                                        <th>RPC</th>
                                        <th>REV(&euro;)</th>
                                        <th>GM</th>
                                        <th>Net ROI</th>
                                        <th>CPC BE</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php foreach($data['processed_report']['campaigns'] as $campaign_instance) { ?>

                                            <?php
                                                $campaign_index_key = clean($campaign_instance->CAMPAIGN);
                                            ?>    

                                            <tr>
                                                <td>
                                                    <?php
                                                        $campaign_instance->CAMPAIGN = str_replace("Â", "", $campaign_instance->CAMPAIGN);
                                                        $campaign_to_date = $campaign_instance->CAMPAIGN ."<br /><strong>(". $campaign_instance->EXTRACTION_DATE . ")</strong>";
                                                        echo $campaign_to_date;
                                                    ?>    
                                                </td>
                                                <td>{{ $campaign_instance->STATUS }}</td>
                                                <td>
                                                    <?php
                                                        echo str_replace("Â", "", $campaign_instance->LABELS);
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                        // $split_grand_budget_total += (float)$campaign_instance->BUDGET;
                                                        echo $campaign_instance->BUDGET;
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                        $split_adgroup = "";
                                                        if(isset($data['keyword_adgroup_by_campaign_date'][$campaign_index_key . "-" . $campaign_instance->EXTRACTION_DATE])) {
                                                            $split_adgroup = $data['keyword_adgroup_by_campaign_date'][$campaign_index_key . "-" . $campaign_instance->EXTRACTION_DATE];
                                                            $split_adgroup = str_replace("Â", "", $split_adgroup);
                                                            echo $split_adgroup;
                                                        }
                                                    ?>
                                                </td>
                                                <td>{{ $campaign_instance->DELIVERY }}</td>
                                                <td>{{ $campaign_instance->BID_STRATEGY_TYPE }}</td>
                                                <td>
                                                    <?php
                                                        $bid = "0";
                                                        if(isset($data['processed_keyword_data']['bid_cost_by_campaign_date'][$campaign_index_key . "-" . $campaign_instance->EXTRACTION_DATE])) {
                                                            $bid = $data['processed_keyword_data']['bid_cost_by_campaign_date'][$campaign_index_key . "-" . $campaign_instance->EXTRACTION_DATE];
                                                            
                                                            /*if((float)$bid > 0) {
                                                                $split_grand_bid_total += (float)$bid;
                                                            }*/
                                                        }
                                                        echo $bid;
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                        $campaign_instance->SPEND = $campaign_instance->SPEND * $dollar_to_eur_value;
                                                        $campaign_instance->SPEND = round($campaign_instance->SPEND,2);
                                                        echo $campaign_instance->SPEND;
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                        echo $campaign_instance->IMPR;
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                        // $split_grand_click_total += (float)$campaign_instance->CLICKS;
                                                        echo $campaign_instance->CLICKS;
                                                    ?>
                                                </td>

                                                <td>
                                                    <?php
                                                        $ctr = "0";
                                                        if((int)$campaign_instance->CLICKS > 0) {
                                                            $ctr = round(((int)$campaign_instance->CLICKS / (int)$campaign_instance->IMPR) * 100, 2);
                                                        }
                                                        echo $ctr;
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                        $avg_cpc = "0";
                                                        if($campaign_instance->CLICKS > 0) {
                                                            $avg_cpc = round($campaign_instance->SPEND / $campaign_instance->CLICKS, 2);
                                                        }

                                                        // $avg_cpc += $split_grand_avg_cpc_total;
                                                        echo $avg_cpc;
                                                    ?>
                                                </td>


                                                <td>
                                                    <?php
                                                        $campaign_clickout = 0;
                                                        $campaign_revenue_revenue = 0;

                                                        if(isset($data['processed_report']['channel_with_campaign_and_date_key'][$campaign_index_key."-".$campaign_instance->EXTRACTION_DATE])) {
                                                            
                                                            foreach($data['processed_report']['channel_with_campaign_and_date_key'][$campaign_index_key."-".$campaign_instance->EXTRACTION_DATE] as $channel_with_campaign_and_date_instance) {
                                                                if(isset($channel_with_campaign_and_date_instance['REVENUE'])) {

                                                                    foreach($channel_with_campaign_and_date_instance['REVENUE'] as $revenue_instance) {

                                                                        $campaign_clickout += $revenue_instance->CLICKS;
                                                                        $campaign_revenue_revenue += $revenue_instance->EARNINGS;
                                                                    }

                                                                }
                                                            }

                                                            $computed_revenue = ($campaign_revenue_revenue * env('REVENUE_MULTIPLIER')) * $dollar_to_eur_value;
                                                            $campaign_revenue_revenue = round($computed_revenue, 2);

                                                        }

                                                        echo $campaign_clickout;

                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                        $click_rate = "0";
                                                        if($campaign_instance->CLICKS > 0 && $campaign_clickout > 0) {
                                                            $click_rate = round($campaign_clickout / $campaign_instance->CLICKS, 2);   
                                                        }

                                                        echo $click_rate;
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                        $rpc = "0";
                                                        if($campaign_clickout > 0) {
                                                            $rpc = round($campaign_revenue_revenue / $campaign_clickout, 2);
                                                        }

                                                        echo $rpc;
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                        echo $campaign_revenue_revenue;
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                        $gross_margin = $campaign_revenue_revenue - $campaign_instance->SPEND;
                                                        echo round($gross_margin, 2);
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                        $net_roi = "0";
                                                        if($campaign_instance->SPEND != 0) {
                                                            $net_roi = ($campaign_revenue_revenue - $campaign_instance->SPEND) / $campaign_instance->SPEND;
                                                            $net_roi = round($net_roi, 2);
                                                        }

                                                        echo $net_roi;
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                        $cpc = "0";

                                                        $computer_rpc = 0;
                                                        if($campaign_clickout > 0) {
                                                            $computer_rpc = $campaign_revenue_revenue / $campaign_clickout;
                                                        }
                                                        
                                                        $conversion_rate = 0;
                                                        if($campaign_instance->CLICKS > 0) {
                                                            $conversion_rate = $campaign_clickout / $campaign_instance->CLICKS;
                                                        }

                                                        if($conversion_rate > 0) {
                                                            $cpc = round(($computer_rpc * $conversion_rate), 2);
                                                        }

                                                        echo $cpc;
                                                    ?>
                                                </td>
                                            </tr>
                                            <?php

                                            $channel_country = "XX";
                                            if(isset($data['processed_report']['channel_with_campaign_and_date_key'][$campaign_index_key."-".$campaign_instance->EXTRACTION_DATE])) {
                                                $channel_country = $data['processed_report']['channel_with_campaign_and_date_key'][$campaign_index_key."-".$campaign_instance->EXTRACTION_DATE][0]['COUNTRY'];
                                            }
                                            
                                            $campaign_labels = str_replace(";", "|", $campaign_instance->LABELS);
                                            $bid_strategy_type = str_replace(";", "|", $campaign_instance->BID_STRATEGY_TYPE);
                                            
                                            $split_campaign_csv_string_eng .= "\"".$campaign_to_date."\",\"".$campaign_instance->STATUS."\",\"".$campaign_labels."\",\"".$campaign_instance->BUDGET."\",\"".$split_adgroup."\",\"".$campaign_instance->DELIVERY."\",\"".$campaign_labels."\",\"".$bid_strategy_type."\",\"".$bid."\",\"".$campaign_instance->SPEND."\",\"".$campaign_instance->IMPR."\",\"".$campaign_instance->CLICKS."\",\"".$ctr."\",\"".$avg_cpc."\",\"".$campaign_clickout."\",\"".$click_rate."\",\"".$rpc."\",\"".$campaign_revenue_revenue."\",\"".$gross_margin."\",\"".$net_roi."\",\"".$cpc."\""."\",\"".$data['formatted_bid_users'][$campaign_instance->USER]."\",\"".$channel_country."\""."\\n";
                                            $split_campaign_csv_string_ita .= "\"".$campaign_to_date."\",\"".$campaign_instance->STATUS."\",\"".$campaign_labels."\",\"".to_ita_values($campaign_instance->BUDGET)."\",\"".$split_adgroup."\",\"".$campaign_instance->DELIVERY."\",\"".$campaign_labels."\",\"".$bid_strategy_type."\",\"".to_ita_values($bid)."\",\"".to_ita_values($campaign_instance->SPEND)."\",\"".to_ita_values($campaign_instance->IMPR)."\",\"".to_ita_values($campaign_instance->CLICKS)."\",\"".to_ita_values($ctr)."\",\"".to_ita_values($avg_cpc)."\",\"".to_ita_values($campaign_clickout)."\",\"".to_ita_values($click_rate)."\",\"".to_ita_values($rpc)."\",\"".to_ita_values($campaign_revenue_revenue)."\",\"".to_ita_values($gross_margin)."\",\"".to_ita_values($net_roi)."\",\"".to_ita_values($cpc)."\",\"".$data['formatted_bid_users'][$campaign_instance->USER]."\",\"".$channel_country."\""."\\n";
                                        }
                                    ?>
                                </tbody>
                            </table>                     
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php


        /* ####################################### CLICKOUT RELATED ####################################### */

        $isTotalClickoutMatched = true;

        $grand_clickout_total = (int)$grand_clickout_total;
        $raw_total_clicks = (int)$raw_total_clicks;
                        
        if($grand_clickout_total > $raw_total_clicks) {

            $percentage_of = ($raw_total_clicks / $grand_clickout_total) * 100;

            // means tolerance is 5% or less
            if($percentage_of < 95) {
                $isTotalClickoutMatched = false;
            }

        } else {
            if($grand_clickout_total < $raw_total_clicks) {
                $percentage_of = ($grand_clickout_total / $raw_total_clicks) * 100;

                // means tolerance is 5% or less
                if($percentage_of < 95) {
                    $isTotalClickoutMatched = false;
                }
            }
        }

        /* ####################################### CLICKOUT RELATED ####################################### */





        /* ####################################### CLICKOUT RELATED ####################################### */

        $isTotalRevenueMatched = true;

        $grand_rev_total = (int)$grand_rev_total;
        $raw_total_earnings = (int)$raw_total_earnings;
                        
        if($grand_rev_total > $raw_total_earnings) {

            $revenue_difference = $grand_rev_total - $raw_total_earnings;
            $revenue_threshold = $raw_total_earnings * 0.05;

            if($revenue_difference > $revenue_threshold) {
                $isTotalRevenueMatched = false;
            }

        } else {
            if($grand_rev_total < $raw_total_earnings) {
                
                $revenue_difference = $raw_total_earnings - $grand_rev_total;
                $revenue_threshold = $grand_rev_total * 0.05;

                if($revenue_difference > $revenue_threshold) {
                    $isTotalRevenueMatched = false;
                }
            }
        }

        /* ####################################### CLICKOUT RELATED ####################################### */





        /* ####################################### CLICKOUT RELATED ####################################### */

        $isTotalClickMatched = true;

        $grand_click_total = (int)$grand_click_total;
        $raw_campaign_clicks = (int)$raw_campaign_clicks;
                        
        if($grand_click_total > $raw_campaign_clicks) {

            $clicks_difference = $grand_click_total - $raw_campaign_clicks;
            $clicks_threshold = $raw_campaign_clicks * 0.05;

            if($clicks_difference > $clicks_threshold) {
                $isTotalClickMatched = false;
            }

        } else {
            if($grand_click_total < $raw_campaign_clicks) {
                
                $clicks_difference = $raw_campaign_clicks - $grand_click_total;
                $clicks_threshold = $grand_click_total * 0.05;

                if($clicks_difference > $clicks_threshold) {
                    $isTotalClickMatched = false;
                }
            }
        }

        /* ####################################### CLICKOUT RELATED ####################################### */

        



        /* ####################################### CLICKOUT RELATED ####################################### */

        $isTotalCostMatched = true;

        $grand_cost_total = (int)$grand_cost_total;
        $raw_campaign_cost = (int)$raw_campaign_cost;
                        
        if($grand_cost_total > $raw_campaign_cost) {

            $cost_difference = $grand_cost_total - $raw_campaign_cost;
            $cost_threshold = $raw_campaign_cost * 0.05;

            if($cost_difference > $cost_threshold) {
                $isTotalCostMatched = false;
            }

        } else {

            if($grand_cost_total < $raw_campaign_cost) {
                
                $cost_difference = $raw_campaign_cost - $grand_cost_total;
                $cost_threshold = $grand_cost_total * 0.05;

                if($cost_difference > $cost_threshold) {
                    $isTotalCostMatched = false;
                }
            }

        }

        /* ####################################### CLICKOUT RELATED ####################################### */

        // echo $isTotalClickoutMatched." - ".$isTotalRevenueMatched." - ".$isTotalClickMatched." - ".$isTotalCostMatched;
        // echo "<br />";

        if(!$isTotalClickoutMatched || !$isTotalRevenueMatched || !$isTotalClickMatched || !$isTotalCostMatched) {

            ?>

                <div class="bottom-sticky-wrapper">
                    <div class="alert alert-danger">The raw data is not matching with the presented report</div>
                </div>

            <?php

        } else if($start == $end) {
                         
            ?>
                
                <div style="background: #fff;width: 100%;padding: 10px;box-shadow: 0px 0px 10px #ccc;left: 0;" class="bottom-sticky-wrapper text-right col-12">
                    <h5>Raw Report and Calculated Report Matches</h5><button id="sync_to_report_server" style="margin-bottom: 20px;" class="btn btn-primary">Sync To Report Database</button>
                </div>

            <?php

        }
    ?>

@stop

@section('adminlte_js')
    @parent

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
    
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">


    <script type="text/javascript">
        $(document).ready( function () {

            var current_time = <?php echo time(); ?>;
            var source = "<?php echo $source; ?>";

            $(".dropdown-menu").on('click', 'a', function(){
                var base_url = "<?php echo url()->current(); ?>";
                var option = $(this).attr("href");
                var get_data = '<?php echo $query_string; ?>';
                
                var target = "";
                if(get_data != "") {
                    target = "?"+get_data+"&client="+option;
                } else {
                    target = "?client="+option;
                }

                target = base_url+target;
                window.location.href = target;

                return false;
           });

            $('#ing_aggregated_csv_download').click (function () {
                var csvContent = '<?php echo $aggregated_campaign_csv_string_ing; ?>'; //here we load our csv data 
                window.open("data:text/csv;charset=utf-8," + escape(csvContent))

                var hiddenElement = document.createElement('a');
                hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csvContent);
                hiddenElement.target = '_blank';
                hiddenElement.download = 'aggregated_ing_'+ current_time +'.csv';
                hiddenElement.click();
            });

            $('#ita_aggregated_csv_download').click (function () {
                var csvContent='<?php echo $aggregated_campaign_csv_string_ita; ?>'; //here we load our csv data 
                window.open("data:text/csv;charset=utf-8," + escape(csvContent))

                var hiddenElement = document.createElement('a');
                hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csvContent);
                hiddenElement.target = '_blank';
                hiddenElement.download = 'aggregated_ita_'+ current_time +'.csv';
                hiddenElement.click();
            });


            $('#ing_split_csv_download').click (function () {
                var csvContent='<?php echo $split_campaign_csv_string_eng; ?>'; //here we load our csv data 
                window.open("data:text/csv;charset=utf-8," + escape(csvContent))

                var hiddenElement = document.createElement('a');
                hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csvContent);
                hiddenElement.target = '_blank';
                hiddenElement.download = 'split_ing_'+ current_time +'.csv';
                hiddenElement.click();
            });

            $('#ita_split_csv_download').click (function () {
                var csvContent='<?php echo $split_campaign_csv_string_ita; ?>'; //here we load our csv data 
                window.open("data:text/csv;charset=utf-8," + escape(csvContent))

                var hiddenElement = document.createElement('a');
                hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csvContent);
                hiddenElement.target = '_blank';
                hiddenElement.download = 'split_ita_'+ current_time +'.csv';
                hiddenElement.click();
            });


            $('#sync_to_report_server').click( function () {
                var csvContent = '<?php echo $aggregated_campaign_csv_string_ing; ?>'; //here we load our csv data 
                var splitCsvContent = '<?php echo $split_campaign_csv_string_eng; ?>'; //here we load our csv data 

                var post_data = {
                    extraction_date : "<?php echo $end; ?>",
                    source : source,
                    csvContent : csvContent,
                    splitCsvContent : splitCsvContent
                };

                $.ajax({
                    url: "http://reports-semberry.cloud/campaign_upload",
                    type: "POST",
                    crossDomain: true,
                    data: post_data,
                    dataType: "json",
                    success: function (response) {
                        
                        var string = "Sync was "+response.message+" with "+response.campaign_synced+" campaign and "+response.split_campaign_synced+" split campaign created";
                        alert(string);
                        
                    },

                    error: function (xhr, status) {
                        alert("error");
                    }
                });
            });


            $('#myTable').DataTable();
            $('#myTableSplit').DataTable();
            $('.datepicker').datepicker();
        });

    </script>
@stop