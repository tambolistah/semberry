@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')

    <?php
        function clean($string) {
            $string = preg_replace('/\xc2\xa0/', '', $string);
            $string = urlencode(str_replace(" ", "", trim(strtolower($string))));
            return $string;
        }

        function to_ita_values($string) {
            $string = str_replace('.', ',', $string);
            return $string;
        }

        $start = date('Y-m-d',(strtotime ( '-2 day' , time() ) ));
        $end = date("Y-m-d");

        if(isset($_GET['start']) && isset($_GET['end'])) {
            $start = $_GET['start'];
            $end = $_GET['end'];           
        }

        $search = "";
        if(isset($_GET['search'])) {
            $search = $_GET['search'];
        }

        $source = "bing";
        if(isset($_GET['source'])) {
            $source = $_GET['source'];
        }
    ?>

    <h1 class="m-0 text-dark">
        Keyword Performance
        <a id="ing_aggregated_csv_download" class="btn inline-block btn-success">Export to CSV (ENG)</a>
        <a id="ita_aggregated_csv_download" class="btn inline-block btn-info">Export to CSV (ITA)</a>
    </h1>

    <hr />

    <form method="get">
        <div class="row">
            <div class="col-md-6 col-12">
                <div class="row">

                    <?php if(isset($_GET['source'])) { ?>
                        <input name="source" type="hidden" value="<?php echo $_GET['source']; ?>" />
                    <?php } ?>
                    
                    <div class="col-md-5 col-12">
                        <div class="form-group">
                            <input name="start" value="<?php echo $start; ?>" placeholder="Start Date" class="datepicker form-control" data-date-format="yyyy-mm-dd">
                        </div>
                    </div>
                    <div class="col-md-5 col-12">
                        <div class="form-group">
                            <input name="end" value="<?php echo $end; ?>" placeholder="End Date" class="datepicker form-control" data-date-format="yyyy-mm-dd">
                        </div>
                    </div>
                    <div class="col-md-2 col-12">
                        <input type="submit" class="btn btn-success" value="Filter" />
                    </div>
                </div>
            </div>
        </div>
    </form>
@stop

@section('content')
    
    <?php
        $aggregated_first_line = "Campaign,Status,Budget,Ad Group,Keyword Label,Campaign Label,Keyword In,Forced KWD,Match Type,KWD Delivery,Bid,COST($),Impressions,Clicks,CTR,Avg_Cpc,Clickout,CR,RPC,REV($),GM($),Net ROI,CPC BE,User\\n";
        $aggregated_keyword_csv_string_ita = $aggregated_first_line;
        $aggregated_keyword_csv_string_ing = $aggregated_first_line;


        $grand_budget_total = 0;
        $grand_bid_total = 0;
        $grand_cost_total = 0;
        $grand_impression_total = 0;
        $grand_click_total = 0;
        $grand_ctr_total = 0;
        $grand_avg_cpc_total = 0;
        $grand_clickout_total = 0;
        $grand_cr_total = 0;
        $grand_rpc_total = 0;
        $grand_rev_total = 0;
        $grand_gm_total = 0;
        $grand_net_roi = 0;
        $grand_cpc_be = 0;
    ?>

    <ul class="nav nav-tabs" id="myTab" role="tablist">
      <li class="nav-item">
        <a class="nav-link <?php if(!isset($_GET['source']) || $_GET['source'] == "bing") { echo "active"; } ?>" id="home-tab" data-toggle="external-link" href="?source=bing" role="tab" aria-controls="home" aria-selected="true">Bing</a>
      </li>
      <li class="nav-item">
        <a class="nav-link <?php if(isset($_GET['source']) && $_GET['source'] == "google") { echo "active"; } ?>" id="profile-tab" data-toggle="external-link" href="?source=google" role="tab" aria-controls="profile" aria-selected="false">Google</a>
      </li>
    </ul>
    
    <div id="group_performance">
        <div class="row">
            <div class="col-12">
                <div class="card">
                   <div class="card-body">

                        <br />

                        <table id="myTableSplit" class="table bordered striped">
                            <thead>
                                <tr>
                                    <th>Campaign</th>
                                    <th>Status</th>
                                    <th>Budget</th>
                                    <th>Ad group</th>
                                    <th>Keyword Labels</th>
                                    <th>Campaign Label</th>
                                    <th>Keyword in</th>
                                    <th>Forced KWD</th>
                                    <th>Match Type</th>
                                    <th>KWD Delivery</th>
                                    <th>Bid</th>
                                    <th>COST($)</th>
                                    <th>Impressions</th>
                                    <th>Clicks</th>
                                    <th>CTR</th>
                                    <th>Avg. Cpc</th>
                                    <th>Clickout</th>
                                    <th>CR</th>
                                    <th>RPC</th>
                                    <th>REV($)</th>
                                    <th>GM($)</th>
                                    <th>Net ROI</th>
                                    <th>CPC BE</th>
                                    <th>User</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                                <?php foreach($data['keywords'] as $keyword) { ?>

                                    <?php
                                        $keyword_in_trimmed = clean($keyword->KEYWORD);
                                        $campaign_trimmed = clean($keyword->CAMPAIGN);
                                        $channel_index_key = $campaign_trimmed."-".$keyword->EXTRACTION_DATE."-".$keyword->MATCH_TYPE."-".$keyword_in_trimmed;
                                    ?>

                                    <tr>
                                        <td>{{ $keyword->CAMPAIGN }}</th>
                                        <td>{{ $keyword->STATUS }}</td>
                                        <td>
                                            <?php
                                                $budget = $data['campaigns'][$campaign_trimmed]->BUDGET;
                                                $grand_budget_total += $budget;
                                                echo $budget;
                                            ?>
                                        </td>
                                        <td>{{ $keyword->AD_GROUP }}</td>
                                        <td>{{ $keyword->LABELS }}</td>
                                        <td>
                                            <?php
                                                $campaign_label = $data['campaigns'][$campaign_trimmed]->LABELS;
                                                echo $campaign_label;
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                                echo $keyword->KEYWORD;
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                                $forced_keyword = "";
                                                if(isset($data['channel_tracer'][$channel_index_key]) && isset($data['channel_tracer'][$channel_index_key]['keyword'])) {
                                                    $forced_keyword = $data['channel_tracer'][$channel_index_key]['keyword'];
                                                    $data['channel_tracer'][$channel_index_key]['used'] = $keyword->id;
                                                    echo $forced_keyword;
                                                }
                                            ?>
                                        </td>
                                        <td>{{ $keyword->MATCH_TYPE }}</td>
                                        <td>{{ $keyword->DELIVERY }}</td>
                                        <td>{{ $keyword->BID }}</td>
                                        <td>
                                            <?php
                                                $cost = $keyword->SPEND;
                                                $grand_cost_total += $cost;   
                                                echo $cost;
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                                $impr = (float)$keyword->IMPR;
                                                $grand_impression_total += $impr;
                                                echo $impr;
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                                $clicks = $keyword->CLICKS;
                                                $grand_click_total +=  $clicks;
                                                echo $clicks;
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                                $ctr = "0";
                                                if((int)$keyword->CLICKS > 0) {
                                                    $ctr = round((int)$keyword->CLICKS / (float)$keyword->IMPR, 2);
                                                }
                                                
                                                $grand_ctr_total += $ctr;
                                                echo $ctr;
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                                $avg_cpc = "0";
                                                if($keyword->CLICKS > 0) {
                                                    $avg_cpc = round($keyword->SPEND / $keyword->CLICKS, 2);
                                                }

                                                $grand_avg_cpc_total += $avg_cpc;
                                                echo $avg_cpc;
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                                $campaign_clickout = 0;
                                                $campaign_revenue_revenue = 0;
                                                
                                                if(isset($keyword->revenue)) {
                                                    if(count($keyword->revenue) > 0) {
                                                        foreach($keyword->revenue as $revenue_instance) {
                                                            $campaign_clickout += $revenue_instance->CLICKS;
                                                            $campaign_revenue_revenue += round($revenue_instance->EARNINGS * env('REVENUE_MULTIPLIER'), 2);
                                                        }                                                        
                                                    }
                                                }

                                                $grand_clickout_total += $campaign_clickout;
                                                echo $campaign_clickout;
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                                $click_rate = "0";
                                                if($keyword->CLICKS > 0) {
                                                    $click_rate = round($campaign_clickout / $keyword->CLICKS, 2);   
                                                }

                                                $grand_cr_total += $click_rate;
                                                echo $click_rate;
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                                $rpc = "0";
                                                if($campaign_clickout > 0) {
                                                    $rpc = round($campaign_revenue_revenue / $campaign_clickout, 2);
                                                }

                                                $grand_rpc_total += $rpc;
                                                echo $rpc;
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                                $grand_rev_total += $campaign_revenue_revenue;
                                                echo $campaign_revenue_revenue;
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                                    $gross_margin = $campaign_revenue_revenue - $keyword->SPEND;
                                                    $grand_gm_total += $gross_margin;
                                                    
                                                    echo $gross_margin;
                                                ?>
                                        </td>
                                        <td>
                                            <?php
                                                    $net_roi = "0";
                                                    if($keyword->SPEND != 0) {
                                                        $net_roi = ($campaign_revenue_revenue - $keyword->SPEND) / $keyword->SPEND;
                                                        $net_roi = round($net_roi, 2);
                                                    }

                                                    $grand_net_roi += $net_roi;
                                                    echo $net_roi;
                                                ?>
                                        </td>
                                        <td>
                                            <?php
                                                $cpc = "0";
                                                if($keyword->CLICKS > 0 && $campaign_clickout > 0) {
                                                    $cpc = round(($campaign_revenue_revenue / $campaign_clickout) * ($campaign_clickout / $keyword->CLICKS), 2);
                                                }

                                                $grand_cpc_be += $cpc; 
                                                echo $cpc;
                                            ?>
                                        </td>
                                        <td>
                                            {{ $keyword->USER }}
                                        </td>
                                    </tr>

                                    <?php
                                        $campaign_label = str_replace(",", "|", $campaign_label);
                                        $campaign_label = str_replace(";", "|", $campaign_label);

                                        $forced_keyword = htmlspecialchars($forced_keyword, ENT_QUOTES);
                                        $keyword->KEYWORD = htmlspecialchars($keyword->KEYWORD, ENT_QUOTES);

                                        $aggregated_keyword_csv_string_ing .= "\"".$keyword->CAMPAIGN."\",\"".$keyword->STATUS."\",\"".$budget."\",\" ".$keyword->AD_GROUP." \",\"".$keyword->LABELS."\",\"".$campaign_label."\",\"".$keyword->KEYWORD."\",\"".$forced_keyword."\",\"".$keyword->MATCH_TYPE."\",\"".$keyword->DELIVERY."\",\"".$keyword->BID."\",\"".$cost."\",\"".$impr."\",\"".$clicks."\",\"".$ctr."\",\"".$avg_cpc."\",\"".$campaign_clickout."\",\"".$click_rate."\",\"".$rpc."\",\"".$campaign_revenue_revenue."\",\"".$gross_margin."\",\"".$net_roi."\",\"".$cpc."\",\"".$keyword->USER."\""."\\n";
                                        $aggregated_keyword_csv_string_ita .= "\"".$keyword->CAMPAIGN."\",\"".$keyword->STATUS."\",\"".$budget."\",\" ".$keyword->AD_GROUP." \",\"".$keyword->LABELS."\",\"".$campaign_label."\",\"".$keyword->KEYWORD."\",\"".$forced_keyword."\",\"".$keyword->MATCH_TYPE."\",\"".$keyword->DELIVERY."\",\"".to_ita_values($keyword->BID)."\",\"".to_ita_values($cost)."\",\"".to_ita_values($impr)."\",\"".to_ita_values($clicks)."\",\"".to_ita_values($ctr)."\",\"".to_ita_values($avg_cpc)."\",\"".to_ita_values($campaign_clickout)."\",\"".to_ita_values($click_rate)."\",\"".to_ita_values($rpc)."\",\"".to_ita_values($campaign_revenue_revenue)."\",\"".to_ita_values($gross_margin)."\",\"".to_ita_values($net_roi)."\",\"".to_ita_values($cpc)."\",\"".$keyword->USER."\""."\\n";
                                    ?>

                                    <?php 
                                    }

                                ?>

                            </tbody>
                        </table>

                        <hr />

                        <h3>Summation of figures</h3>
                        <table class="table table-striped table-bordered">
                            <tr>
                                <th>Budget</th>
                                <th>Cost</th>
                                <th>Impressions</th>
                                <th>Clicks</th>
                                <th>CTR</th>
                                <th>Avg. CPC</th>
                                <th>Clickout</th>
                                <th>CR</th>
                                <th>RPC</th>
                                <th>Revenue</th>
                                <th>GM</th>
                                <th>Net ROI</th>
                            </tr>

                            <tr>
                                <td>{{ $grand_budget_total }}</td>
                                <td>{{ $grand_cost_total }}</td>
                                <td>{{ $grand_impression_total }}</td>
                                <td>{{ $grand_click_total }}</td>
                                <td>
                                    <?php
                                        if($grand_impression_total > 0) {
                                            echo round(($grand_click_total / $grand_impression_total) * 100, 2);
                                        } else {
                                            echo 0;
                                        }
                                    ?>
                                </td>
                                <td>
                                    <?php
                                        $grand_avg_cpc = 0;
                                        if($grand_click_total > 0) {
                                            $grand_avg_cpc = round($grand_cost_total / $grand_click_total,2);
                                        }

                                        echo $grand_avg_cpc;
                                    ?>
                                </td>
                                <td>{{ $grand_clickout_total }}</td>
                                <td>
                                    <?php
                                        $grand_cr_total = 0;
                                        
                                        if($grand_click_total > 0) {
                                            $grand_cr_total = round($grand_clickout_total / $grand_click_total, 2);
                                        }

                                        echo $grand_cr_total;
                                    ?>
                                </td>
                                <td>
                                    <?php
                                        $grand_rpc_total = 0;
                                        if($grand_clickout_total > 0) {
                                            $grand_rpc_total = round($grand_rev_total / $grand_clickout_total, 2);
                                        }

                                        echo $grand_rpc_total;
                                    ?>
                                </td>
                                <td>{{ $grand_rev_total }}</td>
                                <td>{{ $grand_rev_total - $grand_cost_total }}</td>
                                <td>
                                    <?php
                                        $grand_net_roi = 0;
                                        if($grand_cost_total > 0) {
                                            $grand_net_roi = round(($grand_rev_total - $grand_cost_total) / $grand_cost_total, 2);
                                        }
                                        echo $grand_net_roi;
                                    ?>
                                </td>
                            </tr>
                        </table>
                        <hr />
                        <h4>Channels with Revenues and no Associated Keyword</h4>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Channel</th>
                                    <th>Forced Keyword</th>
                                    <th>Campaign</th>
                                    <th>Keyword In</th>
                                    <th>Match Type</th>
                                    <th>Revenue</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                                <?php
                                    $channel_revenue = 0;
                                ?>

                                <?php foreach($data['unused_channel_ids'] as $unused_channel_id) {
                                    if(isset($data['processed_channel_tracer_by_id'][$unused_channel_id])) { ?>
                                        
                                        <?php if(count($data['processed_channel_tracer_by_id'][$unused_channel_id]['revenue']) > 0) { ?>

                                            <?php $channel_revenue_instance = 0; ?>

                                            <?php foreach($data['processed_channel_tracer_by_id'][$unused_channel_id]['revenue'] as $revenue_instance) { ?>

                                                <?php
                                                    $channel_revenue += $revenue_instance->EARNINGS;
                                                    $channel_revenue_instance += $revenue_instance->EARNINGS;
                                                ?>

                                            <?php } ?>

                                            <?php if($channel_revenue_instance > 0) { ?>

                                                <tr>
                                                    <td>{{ $data['processed_channel_tracer_by_id'][$unused_channel_id]['date'] }}</td>
                                                    <td>{{ $data['processed_channel_tracer_by_id'][$unused_channel_id]['channel'] }}</td>
                                                    <td>{{ $data['processed_channel_tracer_by_id'][$unused_channel_id]['keyword'] }}</td>
                                                    <td>{{ $data['processed_channel_tracer_by_id'][$unused_channel_id]['campaign'] }}</td>
                                                    <td>{{ $data['processed_channel_tracer_by_id'][$unused_channel_id]['keyword_in'] }}</td>
                                                    <td>{{ $data['processed_channel_tracer_by_id'][$unused_channel_id]['match_type'] }}</td>
                                                    <td>
                                                        <?php 
                                                            echo round($channel_revenue_instance * env('REVENUE_MULTIPLIER'), 2);
                                                        ?>
                                                    </td>
                                                </tr>

                                            <?php } ?>

                                        <?php } ?>

                                    <?php }
                                } ?>
                            </tbody>
                        </table>

                        <hr />

                        <center>
                            <div class="alert alert-info" role="alert">
                                <h5>Other revenues from channels: {{ round($channel_revenue * env('REVENUE_MULTIPLIER'), 2) }}</h5>
                            </div>
                        </center>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div style="display: none;">
        <form action="/csv_exporter" method="POST">
            @csrf
            <textarea name="csv_string"><?php echo $aggregated_keyword_csv_string_ing; ?></textarea>
            <input type="text" name="export_name" value="<?php echo "aggregated_".time()."_ing_$source.csv" ?>" />
            <input id="target_ing_aggregated_csv_download" type="submit" />
        </form>

        <form action="/csv_exporter" method="POST">
            @csrf
            <textarea name="csv_string"><?php echo $aggregated_keyword_csv_string_ita; ?></textarea>
            <input type="text" name="export_name" value="<?php echo "aggregated_".time()."_ita_$source.csv" ?>" />
            <input id="target_ita_aggregated_csv_download" type="submit" />
        </form>
    </div>
@stop

@section('adminlte_js')
    @parent

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
    
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">

    <script type="text/javascript">
        $(document).ready( function () {
            $('.datepicker').datepicker();

            var current_time = <?php echo time(); ?>;

            $('#ing_aggregated_csv_download').click (function () {
                $('#target_ing_aggregated_csv_download').click();
            });

            $('#ita_aggregated_csv_download').click (function () {
                $('#target_ita_aggregated_csv_download').click();
            });

            $(document).ready( function () {
                $('#myTableSplit').DataTable();
            } );
        });
    </script>
@stop