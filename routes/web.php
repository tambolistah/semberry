<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::get('/bingcampaign','App\Http\Controllers\CampaignController@bingcampaign');

Route::get('/home', function() {
    return view('home');
})->name('home')->middleware('auth');

Auth::routes();
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::group(['middleware' => ['auth']], function() {
	
    Route::get('/keyword_out', [App\Http\Controllers\HomeController::class, 'keyword_out'])->name('keyword_out');

    Route::get('/group_performance','App\Http\Controllers\CampaignController@group_performance');
    Route::get('/kwd_performance','App\Http\Controllers\CampaignController@kwd_performance');

    Route::get('/kwd_performance','App\Http\Controllers\CampaignController@kwd_performance');


    Route::get('revenue_report/parse','App\Http\Controllers\RevenueReportController@parse');
    Route::resource('revenue_report', 'App\Http\Controllers\RevenueReportController');

    Route::post('keyword/parse','App\Http\Controllers\KeywordController@parse');
    Route::resource('keyword', 'App\Http\Controllers\KeywordController');
    Route::get('aggregated_keyword', 'App\Http\Controllers\KeywordController@aggregated');

    Route::post('campaign/parse','App\Http\Controllers\CampaignController@parse');
    Route::resource('campaign', 'App\Http\Controllers\CampaignController');
    Route::resource('channel_tracer', 'App\Http\Controllers\ChannelTracerController');

    Route::post('/csv_exporter','App\Http\Controllers\HomeController@csv_exporter');

    Route::get('/url_maker', function () {
	    return view('url_maker');
	});

    Route::get('/keyword_upload', function () {
        return view('keyword_upload');
    });

    Route::get('/campaign_upload', function () {
        return view('campaign_upload');
    });


    /* ---------------------------------------------- KEYWORD REPORT DOWNLOAD ---------------------------------------------- */

    Route::get('/keyword_download', function () {
        return view('keyword_download');
    });

    Route::post('/keyword_download', 'App\Http\Controllers\HomeController@keyword_download');

    /* ---------------------------------------------- KEYWORD REPORT DOWNLOAD ---------------------------------------------- */




    /* ---------------------------------------------- CAMPAIGN REPORT DOWNLOAD ---------------------------------------------- */

    Route::get('/campaign_download', function () {
        return view('campaign_download');
    });

    Route::post('/campaign_download', 'App\Http\Controllers\HomeController@campaign_download');

    /* ---------------------------------------------- CAMPAIGN REPORT DOWNLOAD ---------------------------------------------- */



    /* ---------------------------------------------- REVENUE REPORT DOWNLOAD ---------------------------------------------- */

    Route::get('/revenue_download', function () {
        return view('revenue_download');
    });

    Route::post('/revenue_download', 'App\Http\Controllers\HomeController@revenue_download');

    /* ---------------------------------------------- REVENUE REPORT DOWNLOAD ---------------------------------------------- */



    /* ---------------------------------------------- CONVERSION REPORT DOWNLOAD ---------------------------------------------- */

    Route::get('/conversion_download', function () {
        return view('conversion_download');
    });

    Route::post('/conversion_download', 'App\Http\Controllers\HomeController@conversion_download');

    /* ---------------------------------------------- CONVERSION REPORT DOWNLOAD ---------------------------------------------- */

});